package mstn.servlets;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class email1
 */
@WebServlet("/email")
public class email extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

    public email() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Content-Type", "text/plain");
		PrintWriter out = response.getWriter();
		try {
			emailSender sender = new emailSender();
			sender.sendMessage(request);
			out.print("E-Mail Enviado!");
		} catch (Exception me) {
			me.printStackTrace(out);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private class emailSender{
		
		private Session mailSession;
		
		public void sendMessage(HttpServletRequest req) throws MessagingException, NamingException, MalformedURLException, JSONException, UnsupportedEncodingException {
			req.setCharacterEncoding("UTF-8");
			String plan = req.getParameter("plan");
			if (plan.equals("CL")){
				plan = "Classic";
			} else if (plan.equals("PR")){
				plan = "Premium";
			} else if (plan.equals("EX")){
				plan = "Executive";
			} else if (plan.equals("MA")){
				plan = "Master";
			} else if (plan.equals("LA")){
				plan = "Latitud";
			} else if (plan.equals("PL")){
				plan = "Platinum";
			}
			
			String data = req.getParameter("data");
			JSONObject json = new JSONObject();
			if(!plan.equals("Platinum"))
				json = new JSONObject(data);


			String to = req.getParameter("email");
			String bcc = req.getParameter("bcc");
		  String subject = "Humano - Cotizaci\u00f3n Plan Internacional";
			String body = ""; 
			body += "Estimado Cliente: \n";
			body += "\n";
			body += "Conforme a su solicitud, a continuaci\u00f3n le anexamos la cotizaci\u00f3n elaborada para: " + req.getParameter("cliente") +"\n";
			body += "\n";
			body += "Plan: " + plan + "\n";
			// body += "Deducible Internacional US$: " + req.getParameter("international") + " / Deducible Local US$: " + req.getParameter("local") + "\n";
			if(!plan.equals("Latitud") && !plan.equals("Platinum")){
				body += "Deducible Local US$: " + req.getParameter("local") + "\n";
			}
			body += "\n";
			if(plan.equals("Platinum")){
				for (int i = 1; i <= 6; i++){
					if (req.getParameter("edad" + i) != null && !req.getParameter("edad" + i).equals("")){
						body += "Nombre: " + req.getParameter("nombre" + i);
						body += " " + req.getParameter("parentesco" + i);
						body += " / " + req.getParameter("edad" + i) + " a\u00f1os ";
						body += " / Sexo: " + req.getParameter("sexo" + i);
						body += " / Tarifa US$: " + req.getParameter("tarifa" + i);
						body += "\n";
					}
				}
				body += "\n";
				body += "Tarifa Anual: " + req.getParameter("total") + "\n";
				body += "Tarifa Semestral: " + req.getParameter("semestral") + "\n";
				body += "Tarifa Trimestral: " + req.getParameter("trimestral") + "\n";

			} else {
				JSONArray planes = json.getJSONArray("planes");
				for (int i = 0; i < planes.length(); i++) {
					JSONObject planO = planes.getJSONObject(i);
					body += "Nombre: " + planO.getString("nombre");
					body += " / Parentesco: " + planO.getString("parentesco");
					body += " / Sexo: " + planO.getString("sexo");

					if(plan.equals("Latitud")){
						body += " / Tarifa US$: " + planO.getString("D100");
					}
					else {
						body += " / Tarifas por Deducibles: US$1000: " + planO.getString("D1000");
						body += " | US$2500: " + planO.getString("D2500");
						body += " | US$5000: " + planO.getString("D5000");
						body += " | US$10000: " + planO.getString("D10000");
					}
					body += "\n";
				}
				body += "\n";
				body += "\n";
				if(plan.equals("Latitud")){
					body += "Tarifa Anual: " + req.getParameter("anual100") + "\n";
					body += "Tarifa Semestral: " + req.getParameter("semestral100") + "\n";
					body += "Tarifa Trimestral: " + req.getParameter("trimestral100") + "\n";
				}
				else {
					body += "Tarifa Anual por Deducibles:  US$1000: " + req.getParameter("anual1000");
					body += " | US$2500: " + req.getParameter("anual2500");
					body += " | US$5000: " + req.getParameter("anual5000");
					body += " | US$10000: " + req.getParameter("anual10000");
					
					body += "\nTarifa Semestral por Deducibles:  US$1000: " + req.getParameter("semestral1000");
					body += " | US$2500: " + req.getParameter("semestral2500");
					body += " | US$5000: " + req.getParameter("semestral5000");
					body += " | US$10000: " + req.getParameter("semestral10000");
					
					body += "\nTarifa Trimestral por Deducibles:  US$1000: " + req.getParameter("trimestral1000");
					body += " | US$2500: " + req.getParameter("trimestral2500");
					body += " | US$5000: " + req.getParameter("trimestral5000");
					body += " | US$10000: " + req.getParameter("trimestral10000");
				}
			}
			body += "\n";
			body += "Tarifas y deducibles expresados en D\u00f3lares Americanos (US$)\n";
			body += "\n";
			body += "NOTA: Esta Cotizaci\u00f3n tiene una validez de 30 (Treinta) d\u00edas a partir de la fecha indicada en esta cotizaci\u00f3n y la aceptaci\u00f3n est\u00e1 sujeta a la revisi\u00f3n de la declaraci\u00f3n de salud.\n";
			body += "\n";
			
			body += "Informaci\u00f3n detallada de este plan podr\u00e1 ser encontrada en el brochure de Salud Internacional adjunto.\n";
			body += "Quedamos a su disposici\u00f3n para aclarar cualquier duda.\n";
			body += "\n";
			body += "Sena J. Espinosa\n";
			body += "Directora de Negocios Internacional y Vida | Oficina Principal\n";
			body += "sespinosa@humano.com.do\n";
			body += "Tel: (809) 338-1521\n";
			body += "www.humano.com.do\n";
			  
			String scheme = req.getScheme();             // http
			String serverName = req.getServerName();     // hostname.com
			int serverPort = req.getServerPort();        // 80

			InitialContext context = new InitialContext();
			mailSession = (Session)context.lookup("mail/internacional");

			// Define message
			MimeMessage message = new MimeMessage(mailSession);
		
			// Email to...
			String toEmails[] = to.split(",");
			for(int i=0; i<toEmails.length; i++)
			{
				message.addRecipient(Message.RecipientType.TO,
														new InternetAddress(toEmails[i]));
			}
			
			// Email bcc to...
			if (!bcc.equals("")){
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
			}
			// Email subject
			message.setSubject(subject);
		
			// Create the multi-part
			Multipart multipart = new MimeMultipart();
			// Create text mimebody
			BodyPart messageBodyPart = new MimeBodyPart();
			// Fill the message
			messageBodyPart.setText(body);
			//messageBodyPart.setContent(body);
			// Add the first part (text mimebody)
			multipart.addBodyPart(messageBodyPart);
		
			// this is if you would need to attach a file from local drive, not from URL

			//messageBodyPart = new MimeBodyPart();
			//DataSource source = new FileDataSource("");
			//messageBodyPart.setDataHandler(new DataHandler(source));
			//messageBodyPart.setFileName(filename);

			String queryString = req.getQueryString();
			String urlStr = scheme + "://" + serverName + ":" + serverPort + "/birt/frameset?" + queryString;
			if(!plan.equals("Platinum"))
				urlStr += "&data=" + java.net.URLEncoder.encode(data, "UTF-8");	
			// Create file attached mimebody
			// messageBodyPart = new MimeBodyPart(); 
			// URL url = new URL(urlStr);
			// DataSource source = new URLDataSource(url);
			// messageBodyPart.setDataHandler(new DataHandler(source));
			// messageBodyPart.setFileName("cotizacion.pdf");
			// multipart.addBodyPart(messageBodyPart);

			messageBodyPart = addAttachment(multipart, urlStr, "cotizacion.pdf");
		    
			String fileName = "brochure_" + plan + ".pdf";
			urlStr = scheme + "://" + serverName + ":" + serverPort + "/" + fileName;
			// messageBodyPart = new MimeBodyPart(); 
			// url = new URL(urlStr);
			// source = new URLDataSource(url);
			// messageBodyPart.setDataHandler(new DataHandler(source));
			// messageBodyPart.setFileName(fileName);
			// multipart.addBodyPart(messageBodyPart);
			messageBodyPart = addAttachment(multipart, urlStr, fileName.replace("_", " ").toUpperCase());

			// Put parts in message
			message.setContent(multipart);
		
			Transport.send(message);
		}
	}
	BodyPart addAttachment(Multipart multipart, String urlStr, String filename) throws MalformedURLException, MessagingException{
		BodyPart messageBodyPart = new MimeBodyPart();
		URL file = new URL(urlStr);
		DataSource source = new URLDataSource(file);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		multipart.addBodyPart(messageBodyPart);
		return messageBodyPart;
	}
}