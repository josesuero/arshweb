package mstn.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.URLDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;



/**
 * Servlet implementation class emaillocal
 */
@WebServlet("/emailcolect")
public class emailcolect extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public emailcolect() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			emailSender sender = new emailSender();
			sender.sendMessage(request);
			
			out.print("E-Mail Enviado!");
		} catch (Exception me) {
			me.printStackTrace(out);
		}
	}

	public String valores(String valor) {
		if (valor.equals("T")) {
			valor = "Titular";
		} else if (valor.equals("T65")) {
			valor = "Menor de 64.5 A\u00f1os";
		} else if (valor.equals("TM")) {
			valor = "Titular (Con Maternidad)";
		} else if (valor.equals("T75")) {
			valor = "Mayor de 74.5 A\u00f1os";
		} else if (valor.equals("H")) {
			valor = "Menores de 21 a\u00f1os";
		} else if (valor.equals("O")) {
			valor = "Ninguno";
		}
		return valor;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private class emailSender {

		private Session mailSession;

		public void sendMessage(HttpServletRequest req)
				throws MessagingException, NamingException,
				MalformedURLException, JSONException, UnsupportedEncodingException {
			req.setCharacterEncoding("UTF-8");
			String body = "";
			
			String data = req.getParameter("data");
			JSONObject json = new JSONObject(data);
			JSONObject cliente = json.getJSONObject("cliente");
		    String subject = "ARS Humano - Cotizaci\u00f3n Plan Internacional Colectivo";
			body += "Estimado Cliente: \n";
			body += "\n";
			
			body += "Conforme a su solicitud, a continuaci\u00f3n le anexamos la cotizaci\u00f3n elaborada para: " + cliente.getString("nombre") + "\n";
			
			body += "\n";
			body += "Prima Anual: " + json.getString("total") + "\n";
			body += "Prima Semestral: " + json.getString("semestral") + "\n";
			body += "Prima Trimestral: " + json.getString("trimestral") + "\n";
			body += "Prima Mensual: " + json.getString("mensual") + "\n";
			body += "\n";
			body += "Primas y deducibles expresados en D\u00f3lares Americanos (US$)\n";
			body += "\n";
			body += "NOTA: Esta Cotizaci\u00f3n tiene una validez de 30 (treinta) d\u00edas a partir de la fecha indicada y la aceptación est\u00e1 sujeta a la opinión de La Compa\u00f1\u00eda.\n";
			body += "\n";
			
			body += "Agradecemos haya considerado nuestra propuesta de valor de Salud Internacional, y nos reiteramos a su disposici\u00f3n para cualquier inquietud adicional al respecto.\n";
			
			body += "\n";
			body += "Federico Fernandez\n";
			body += "(809)-747-9754\n";
			body += "Director de Negocios Salud Internacional\n";
			body += "ARS Humano\n";
			body += "Ave. Lope de Vega # 36\n";
			body += "Santo Domingo, R.D.\n";
			body += "Tel: (809) 476-3659\n";
			body += "www.arshumano.com\n";			
			
			String scheme = req.getScheme(); // http
			String serverName = req.getServerName(); // hostname.com
			int serverPort = req.getServerPort(); // 80

			String to = cliente.getString("email");
			// String to = "sdelrico@arshumano.com";
			String bcc = cliente.getString("bcc");
			// String bcc = "";


			 String urlStr = scheme + "://" + serverName + ":" + serverPort +
			 "/birt26/frameset?__report=arshcolect.rptdesign&__format=pdf&data=" + URLEncoder.encode(data,"UTF-8");

			/*String urlStr = "http://localhost:" + serverPort
					+ "/birt/frameset?__report=arshlocal.rptdesign&data="
					+ data + "&__format=pdf";*/

			//String urlStr = "http://localhost:8080/birt/frameset?__report=arshlocal.rptdesign&__format=pdf&data={%22planes%22:[{%22nombre%22:%22T%22,%22Superior%22:9800,%22Royal%22:11332,%22Max%22:16732,%22Platinum%22:38800},{%22nombre%22:%22T75%22,%22Superior%22:29400,%22Royal%22:33996,%22Max%22:50196,%22Platinum%22:116400},{%22nombre%22:%22H%22,%22Superior%22:39200,%22Royal%22:45330,%22Max%22:66930,%22Platinum%22:155200}],%22opciones%22:[{%22nombre%22:%22Odontologia%22,%22plan%22:%22Platinum|Dentasa%22,%22Superior%22:29316,%22Royal%22:29316,%22Max%22:29316,%22Platinum%22:29316},{%22nombre%22:%22Ultimos%20Gastos%22,%22plan%22:%22ILIMITADO%22,%22Superior%22:5880,%22Royal%22:5880,%22Max%22:5880,%22Platinum%22:5880},{%22nombre%22:%22Medicina%20Ambulatoria%22,%22plan%22:%225000|30%22,%22Superior%22:13090,%22Royal%22:13090,%22Max%22:13090,%22Platinum%22:13090}],%22cliente%22:{%22txtNombre%22:%22jose%22,%22txtTelefonoC%22:%22809-%22,%22txtFax%22:%22809-%22,%22txtDireccion%22:%22suero%22,%22txtSector%22:%22Sector%22,%22txtMailC%22:%22dguzman@mstn.com%22,%22txtVia%22:%22Indicar%20Nombre%22,%22textTelefonoR%22:%22809-%22,%22txtAgencia%22:%22Indicar%20Agencia%22,%22txtCelular%22:%22809-%22,%22txtMailR%22:%22email%22},%22totalAfiliados%22:7,%22totalPlanes%22:{%22Superior%22:78400,%22Royal%22:90658,%22Max%22:133858,%22Platinum%22:310400},%22subTotal%22:{%22Superior%22:126686,%22Royal%22:138944,%22Max%22:182144,%22Platinum%22:358686},%22descuento%22:{%22Superior%22:6334,%22Royal%22:6947,%22Max%22:9107,%22Platinum%22:17934},%22total%22:{%22Superior%22:120352,%22Royal%22:131997,%22Max%22:173037,%22Platinum%22:340752},%22cuota%22:{%22Superior%22:60176,%22Royal%22:65998,%22Max%22:86518,%22Platinum%22:170376},%22formaPago%22:%22Semestral%22,%22bcc%22:%22%22,%22subject%22:%22%22}";

			InitialContext context = new InitialContext();
			mailSession = (Session) context.lookup("mail/colectivo");

			// Define message
			MimeMessage message = new MimeMessage(mailSession);

			// Email to...
			String toEmails[] = to.split(",");
			for (int i = 0; i < toEmails.length; i++) {
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(toEmails[i]));
			}

			// Email bcc to...
			if (!bcc.equals("")) {
				message.addRecipient(Message.RecipientType.BCC,
						new InternetAddress(bcc));
			}
			// Email subject
			message.setSubject(subject);
			// Create the multi-part
			Multipart multipart = new MimeMultipart();
			// Create text mimebody
			BodyPart messageBodyPart = new MimeBodyPart();
			// Fill the message
			messageBodyPart.setText(body);
			// Add the first part (text mimebody)
			multipart.addBodyPart(messageBodyPart);

			// this is if you would need to attach a file from local drive, not
			// from URL

			// messageBodyPart = new MimeBodyPart();
			/*
			 * DataSource source = new FileDataSource(filename);
			 * messageBodyPart.setDataHandler(new DataHandler(source));
			 * messageBodyPart.setFileName(filename);
			 */

			// Create file attached mimebody
			messageBodyPart = new MimeBodyPart();
			URL url = new URL(urlStr);
			DataSource source = new URLDataSource(url);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName("cotizacion.pdf");

			// Add the second part (attached mimebody)
			// multipart.addBodyPart(messageBodyPart);

			/*
			 * urlStr = scheme + "://" + serverName + ":" + serverPort +
			 * "/ARSH-Internacional.pdf";
			 * 
			 * // Create file attached mimebody messageBodyPart = new
			 * MimeBodyPart(); url = new URL(urlStr); source = new
			 * URLDataSource(url); messageBodyPart.setDataHandler(new
			 * DataHandler(source));
			 * messageBodyPart.setFileName("ARSH-Internacional.pdf");
			 */
			// Add the second part (attached mimebody)
			multipart.addBodyPart(messageBodyPart);

			// Put parts in message
			message.setContent(multipart);

			Transport.send(message);
		}
	}
}
