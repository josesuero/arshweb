package mstn.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class store
 */
@WebServlet("/store")
public class store extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public store() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Connection connection=null;
		req.setCharacterEncoding("UTF-8");
		ResultSet rs = null;
		String sqlstr = "";
		try {
			connection = getJNDIConnection();
			connection.setAutoCommit(false);
			Statement statement = connection.createStatement();
			String data = req.getParameter("data");
			if (data != null && !data.trim().isEmpty()){
				JSONObject json = new JSONObject(data);
				String cotizador = json.getString("cotizador");
				if (cotizador != null && !cotizador.trim().isEmpty()) {
					out.print("Guardando registro para el cotizador: " + cotizador + "\n");
					JSONArray deduciblesInt = json.getJSONArray("deducibles");
					
					for (int dedInt = 0; dedInt < deduciblesInt.length(); dedInt++) {
						String deducibleInt = deduciblesInt.getString(dedInt);
						sqlstr = "insert into cotizacionINT(fecha,cliente,telefono,gerente,intermediario,email,plan,deducibleLocal,deducibleInt,tarifa) values(curdate(), '" + req.getParameter("cliente") + "','" + req.getParameter("telefono") + "','" + req.getParameter("gerente") + "','" + req.getParameter("intermediario") + "','" + req.getParameter("email") + "','" + req.getParameter("plan") + "','" 
							+ req.getParameter("local") + "','" 
							// + req.getParameter("international") + "'," 
							+ deducibleInt + "'," 
							// + req.getParameter("total").replaceAll(",", "") 
							+ json.getJSONObject("total").getString(deducibleInt) 
							+ ");";
						statement.executeUpdate(sqlstr,Statement.RETURN_GENERATED_KEYS);
						connection.commit();
						rs = statement.getGeneratedKeys();
						if(rs.next()){
							int id = rs.getInt(1);
							JSONArray planes = json.getJSONArray("planes");
							for (int idx = 0; idx < planes.length(); idx++) {
								JSONObject prospecto = planes.getJSONObject(idx);
								sqlstr = "insert into cotizacionINT_detail(id_cotizacion,parentesco,edad,sexo,tarifa) values(" 
								+ id + ",'" 
								+ prospecto.getString("nombre") + "','" //+ req.getParameter("nombre" + i) + "','" 
								+ prospecto.getString("edad") + "','" //+ req.getParameter("edad" + i) + "','" 
								+ prospecto.getString("sexo") + "'," //+ req.getParameter("sexo" + i) + "'," 
								+ prospecto.getString("D" + deducibleInt ).replaceAll(",", "") //+ req.getParameter("tarifa" + i).replaceAll(",", "") 
								+ ")";
								statement.executeUpdate(sqlstr);
							}
							// for (int i = 1; i <= 6; i++){
							// 	if (req.getParameter("edad" + i) != null && !req.getParameter("edad" + i).equals("")){
							// 		//body += "Nombre: " + req.getParameter("nombre" + i);
							// 	}
							// }
							connection.commit();
						}
						rs.close();
					}
					return;
				}
			}
			sqlstr = "insert into cotizacionINT(fecha,cliente,telefono,gerente,intermediario,email,plan,deducibleLocal,deducibleInt,tarifa) values(curdate(), '" + req.getParameter("cliente") + "','" + req.getParameter("telefono") + "','" + req.getParameter("gerente") + "','" + req.getParameter("intermediario") + "','" + req.getParameter("email") + "','" + req.getParameter("plan") + "','" + req.getParameter("local") + "','" + req.getParameter("international") + "'," + req.getParameter("total").replaceAll(",", "") +");";
			statement.executeUpdate(sqlstr,Statement.RETURN_GENERATED_KEYS);
			connection.commit();
			rs = statement.getGeneratedKeys();
			//sqlstr = "select last_insert_id();";
			//rs = statement.executeQuery(sqlstr);
			if(rs.next()){
				int id = rs.getInt(1);
				for (int i = 1; i <= 6; i++){
					if (req.getParameter("edad" + i) != null && !req.getParameter("edad" + i).equals("")){
						sqlstr = "insert into cotizacionINT_detail(id_cotizacion,parentesco,edad,sexo,tarifa) values(" + id + ",'" + req.getParameter("nombre" + i) + "','" + req.getParameter("edad" + i) + "','" + req.getParameter("sexo" + i) + "'," + req.getParameter("tarifa" + i).replaceAll(",", "") + ")";
						statement.executeUpdate(sqlstr);
						//body += "Nombre: " + req.getParameter("nombre" + i);
					}
				}
				connection.commit();
			}
			rs.close();
		} catch (SQLException e) {
			out.print(sqlstr);
			e.printStackTrace(out);
		} catch (JSONException e) {
			e.printStackTrace(out);
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	public Connection getJNDIConnection(){
		String dataSourceContext = "jdbc/ARSH";
		Connection result = null;
		try {
			Context initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup(dataSourceContext);
			if(dataSource != null){
				result = dataSource.getConnection();
			}
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
