package mstn.servlets;
import java.sql.*;
import java.util.Enumeration;

import javax.naming.*;
import javax.sql.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class dbserver
 */
@WebServlet("/dbserver")
public class dbserver extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public dbserver() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String table = request.getParameter("table");
		//String field = request.getParameter("field");
		String filter = "";
		Enumeration<String> params = request.getParameterNames();
		while (params.hasMoreElements()){
			String param = params.nextElement();
			if (!param.equals("table") && !param.equals("field")){
				if (!filter.isEmpty()){
					filter += " and " ;
				} else {
					filter += " where ";
				}
				filter += param + " like '" + request.getParameter(param) + "%'";
			}
		}
		Connection connection=null;
		ResultSet rs = null;
		String[] fields = request.getParameter("field").split(",");
		connection = getJNDIConnection();
		try {
			Statement statement = connection.createStatement();
			String sqlstr = "Select * from " + table + filter;
			rs = statement.executeQuery(sqlstr);
			String result = "[";
			if(!rs.isBeforeFirst()){
				out.print(0);
			} else {
				while(rs.next()){
					if(!rs.isFirst()){
						result +=",";
					}
					result += "{";
					for (int i=0;i < fields.length;i++) {
						if (i > 0){
							result +=",";
						}
						String[] parts = fields[i].split(":");
						result += parts[0] + ":'" + rs.getString(parts[1]) + "'";
					}
					result += "}";
				}
				result += "]";
				out.print(result);
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace(out);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public Connection getJNDIConnection(){
		String dataSourceContext = "jdbc/ARSH";
		Connection result = null;
		try {
			Context initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup(dataSourceContext);
			if(dataSource != null){
				result = dataSource.getConnection();
			}
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
