package mstn.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class ajaxserver
 */
@WebServlet("/ajaxserver")
public class ajaxserver extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ajaxserver() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();		
		
		String resp = "";
		try {
			String data = request.getParameter("data");
			if (data.equals("")){
				data = "{}";
			}
			JSONObject json = new JSONObject(data);
			if (request.getParameter("requesttype").equals("planesGetAll")){
				resp = planesGetAll(json);				
			} else if (request.getParameter("requesttype").equals("planesSave")){
			    planesSave(json);				
			} else if (request.getParameter("requesttype").equals("opcionesGetAll")){
				resp = opcionesGetAll(json);
			} else if (request.getParameter("requesttype").equals("opcionesSave")){
				opcionesSave(json);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(out);
		} catch (SQLException e) {
			e.printStackTrace(out);	
		}
		
		out.print(resp);
	}
	
	public Connection getJNDIConnection(){
		String dataSourceContext = "jdbc/ARSH";
		Connection result = null;
		try {
			Context initialContext = new InitialContext();
			DataSource dataSource = (DataSource)initialContext.lookup(dataSourceContext);
			if(dataSource != null){
				result = dataSource.getConnection();
			}
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public String planesGetAll(org.json.JSONObject data) throws SQLException, JSONException
    {		
		Connection connection=null;
		ResultSet rs = null;
		org.json.JSONArray records = new org.json.JSONArray();
		
		try {
			connection = getJNDIConnection();	
			Statement statement = connection.createStatement();
			String sqlstr = "Select * from planes order by name" ;
			rs = statement.executeQuery(sqlstr);
			while(rs.next()){
				JSONObject rec = new JSONObject();
				ResultSetMetaData meta =rs.getMetaData();
				for (int i = 1; i <= meta.getColumnCount();i++){
					rec.put(meta.getColumnName(i), rs.getString(i));
				}
				records.put(rec);
			}
		} finally {
				connection.close();
		}
		
		return records.toString();
    }
	
	public void planesSave(JSONObject data) throws SQLException, JSONException 
	{		
		Connection connection=null;
		String sqlstr;
		try {			
			connection = getJNDIConnection();	
			Statement statement = connection.createStatement();
			if (data.getString("id").equals("0"))   //INSERT
			{
				sqlstr = "insert into planes(name,tarifa,mensual) values ('"+data.getString("name")+"','"+data.getString("tarifa")+"','"+data.getString("mensual")+"')";
			    statement.executeUpdate(sqlstr);
			} else   //UPDATE
			{
				if (data.getString("DELETE").toString() == "false")
                {                
					sqlstr = "update planes set name = '"+data.getString("name")+"', tarifa='"+data.getString("tarifa")+"', mensual='"+data.getString("mensual")+"' where id="+data.getString("id");
					statement.executeUpdate(sqlstr);
                } else if (data.getString("DELETE").toString() == "true"){
                	sqlstr = "delete from planes where id = "+data.getString("id");
                }
			}              
		} finally {
			connection.close();			
		}		
	}
	
	public String opcionesGetAll(org.json.JSONObject data) throws SQLException, JSONException
    {	
		Connection connection=null;
		ResultSet rs = null;
		JSONArray records = new JSONArray();
		
		try {
			connection = getJNDIConnection();	
			Statement statement = connection.createStatement();
			String sqlstr = "Select * from opciones order by name" ;
			rs = statement.executeQuery(sqlstr);
			
			while(rs.next()){
				JSONObject rec = new JSONObject();
				ResultSetMetaData meta =rs.getMetaData();
				for (int i = 1; i <= meta.getColumnCount();i++){
					rec.put(meta.getColumnName(i), rs.getString(i));
				}
				
                sqlstr = "select * from opciones_planes where id_option =" + rec.getString("id");
                Statement statementopt = connection.createStatement();
                ResultSet rsopt = statementopt.executeQuery(sqlstr);
                
                ResultSetMetaData metaopt =rsopt.getMetaData();
                JSONArray recordsopt = new JSONArray();
                while(rsopt.next()){
                	JSONObject recopt = new JSONObject();
                	for (int x = 1; x <= metaopt.getColumnCount();x++){
                		recopt.put(metaopt.getColumnName(x), rsopt.getString(x));	
                	}
                	recordsopt.put(recopt);
                }
                
                rec.put("opc_planes", recordsopt);
				
				
				records.put(rec);
				
                
				
			}
					
			
		} finally {
				connection.close();
		}		
		return records.toString();
    }    
	
	public void opcionesSave(JSONObject data) throws SQLException, JSONException 
	{		
		Connection connection=null;
		ResultSet rs = null;
		String sqlstr;
		int id = 0;
		org.json.JSONArray opc_Planes = data.getJSONArray("opc_planes");
		org.json.JSONObject opc;
		
		try {			
			connection = getJNDIConnection();	
			Statement statement = connection.createStatement();
			if (data.getString("id").equals("0"))   //INSERT
			{
				sqlstr = "insert into opciones(name) values ('"+data.getString("name")+"'";
				statement.executeUpdate(sqlstr);
				
				//Statement statement = connection.createStatement();
				sqlstr = "Select max(id) from opciones" ;
				rs = statement.executeQuery(sqlstr);
				id = rs.getInt("ID");
												
				for (int i = 0; i < opc_Planes.length(); i++){
					opc = opc_Planes.getJSONObject(i); 
					sqlstr = "insert into opciones_planes(id_option, name, tarifa) values ("+id+",'"+opc.getString("name")+"','"+opc.getString("tarifa")+"'";
					statement.executeUpdate(sqlstr);					
				}
				
			} else //UPDATE
			{
				sqlstr = "update opciones set name = '"+data.getString("name")+"' where id="+data.getString("id");
				statement.executeUpdate(sqlstr);
				
				for (int i = 0; i < opc_Planes.length(); i++){
					opc = opc_Planes.getJSONObject(i);
					
					if (opc.getString("name").toString() != "-001")
                    {
                        
                        if (opc.getString("id").toString() != "0")
                        {
                        	sqlstr = "update opciones_planes set id_option="+data.getString("id")+", name="+opc.getString("name")+", tarifa="+opc.getString("tarifa")+" where id="+opc.getString("id");
                        }
                        else
                        {
                        	sqlstr = "insert into opciones_planes(id_option, name, tarifa) values("+data.getString("id")+",'"+opc.getString("name")+"','"+opc.getString("tarifa")+"'))";
                        }
                    }                       
                    else
                    {
                        sqlstr = "delete from opciones_planes where id = " + opc.getString("id");
                    }					
					statement.executeUpdate(sqlstr);					
				}
			}              
		} finally {
				connection.close();			
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
