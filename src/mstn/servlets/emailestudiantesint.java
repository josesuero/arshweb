package mstn.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.URLDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class email1
 */
@WebServlet("/emailestudiantesint")
public class emailestudiantesint extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

    public emailestudiantesint() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Content-Type", "text/plain");
		PrintWriter out = response.getWriter();
		try {
			emailSender sender = new emailSender();
			sender.sendMessage(request, response);
			out.print("E-Mail Enviado!");
		} catch (Exception me) {
			me.printStackTrace(out);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private class emailSender{
		
		private Session mailSession;
		
		public void sendMessage(HttpServletRequest req, HttpServletResponse response) throws MessagingException, NamingException, JSONException, IOException {
			req.setCharacterEncoding("UTF-8");
			String plan = req.getParameter("plan");
			if (plan.equals("EI")){
				plan = "Estudiantes Internacionales";
			}
			String data = req.getParameter("data");
			JSONObject json = new JSONObject(data);
			// PrintWriter out = response.getWriter();
			// out.print("\n" + data);
			
			String to = req.getParameter("email");
			String bcc = req.getParameter("bcc");
			String subject = "Humano - Cotizaci\u00f3n Estudiantes Internacionales";
			String body = ""; 
			body += "Estimado Cliente: \n";
			body += "\n";
			body += "Conforme a su solicitud, a continuaci\u00f3n le anexamos la cotizaci\u00f3n elaborada para: " + req.getParameter("cliente") +"\n";
			body += "\n";
			body += "Plan: " + plan + "\n";
			body += json.getString("modalidadText") + "\n";
			body += json.getString("daterange") + "\n\n";

			JSONArray planes = json.getJSONArray("planes");
				for (int i = 0; i < planes.length(); i++) {
					JSONObject planO = planes.getJSONObject(i);
					body += "Nombre: " + planO.getString("nombre");
					body += " / Parentesco: " + planO.getString("parentesco");
					body += " / Sexo: " + planO.getString("sexo");
					body += " / Tarifa: " + planO.getString("D0");
					body += "\n";
				}
				body += "\n";
				body += "\n";

				body += "Tarifa \u00danica: " + req.getParameter("unico0");
				if(json.getBoolean("showAnual")) {	
					body += "\nTarifa Anual: " + req.getParameter("anual0");
				}
				if(json.getBoolean("showSemestral")) {	
					body += "\nTarifa Semestral: " + req.getParameter("semestral0");
				}
				if(json.getBoolean("showTrimestral")) {	
					body += "\nTarifa Trimestral: " + req.getParameter("trimestral0");
				}
			
			body += "\n";
			body += "Tarifas expresadas en D\u00f3lares Americanos (US$)\n";
			body += "\n";
			body += "NOTA: Esta Cotizaci\u00f3n tiene una validez de 30 (Treinta) d\u00edas a partir de la fecha indicada en esta cotizaci\u00f3n y la aceptaci\u00f3n est\u00e1 sujeta a la revisi\u00f3n de la declaraci\u00f3n de salud.\n";
			body += "\n";
			
			body += "Informaci\u00f3n detallada de este plan podr\u00e1 ser encontrada en el brochure de Salud Internacional adjunto.\n";
			body += "Quedamos a su disposici\u00f3n para aclarar cualquier duda.\n";
			body += "\n";
			body += "Sena J. Espinosa\n";
			body += "Directora de Negocios Internacional y Vida | Oficina Principal\n";
			body += "sespinosa@humano.com.do\n";
			body += "Tel: (809) 338-1521\n";
			body += "www.humano.com.do\n";
			  
			String scheme = req.getScheme();             // http
			String serverName = req.getServerName();     // hostname.com
			int serverPort = req.getServerPort();        // 80

			InitialContext context = new InitialContext();
			mailSession = (Session)context.lookup("mail/internacional");

			// Define message
			MimeMessage message = new MimeMessage(mailSession);
		
			// Email to...
			String toEmails[] = to.split(",");
			for(int i=0; i<toEmails.length; i++)
			{
				message.addRecipient(Message.RecipientType.TO,
														new InternetAddress(toEmails[i]));
			}
			
			// Email bcc to...
			if (!bcc.equals("")){
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
			}
			// Email subject
			message.setSubject(subject, "UTF-8");
		
			// Create the multi-part
			Multipart multipart = new MimeMultipart();
			// Create text mimebody
			BodyPart messageBodyPart = new MimeBodyPart();
			// Fill the message
			// messageBodyPart.setText(body);
			body = replaceAccents(body);
			messageBodyPart.setContent(body, "text/plain;charset=utf-8");
			message.setHeader("Content-Type", "text/plain;charset=utf-8");
			
			// Add the first part (text mimebody)
			multipart.addBodyPart(messageBodyPart);

			String queryString = req.getQueryString();
			String urlStr = scheme + "://" + serverName + ":" + serverPort + "/birt/frameset?" + queryString;
			urlStr += "&data=" + java.net.URLEncoder.encode(data, "UTF-8");	

			messageBodyPart = addAttachment(multipart, urlStr, "cotizacion.pdf");
		    
			String fileName = "brochure_estudiantes_internacionales.pdf";
			urlStr = scheme + "://" + serverName + ":" + serverPort + "/" + fileName;
			
			// multipart.addBodyPart(messageBodyPart);
			messageBodyPart = addAttachment(multipart, urlStr, fileName.replace("_", " ").toUpperCase());

			// Put parts in message
			message.setContent(multipart);
		
			Transport.send(message);
		}
	}
	BodyPart addAttachment(Multipart multipart, String urlStr, String filename) throws MalformedURLException, MessagingException{
		BodyPart messageBodyPart = new MimeBodyPart();
		URL file = new URL(urlStr);
		DataSource source = new URLDataSource(file);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		multipart.addBodyPart(messageBodyPart);
		return messageBodyPart;
	}
	String replaceAccents(String text) {
		return text
	        // .replace("\u00e1", "&aacute;").replace("\u00e9", "&eacute;").replace("\u00ed", "&iacute;").replace("\u00f3", "&oacute;").replace("\u00fa", "&uacute;")
	        // .replace("\u00c1", "&Aacute;").replace("\u00c9", "&Eacute;").replace("\u00cd", "&Iacute;").replace("\u00d3", "&Oacute;").replace("\u00da", "&Uacute;")
	        // .replace("\u00f1", "&ntilde;").replace("\u00d1", "&Ntilde;")
			/// Las letras mayúsculas no se pudieron reemplazar
	        .replaceAll("Ã¡", "&aacute;") //.replaceAll("Ã█", "&Aacute;") 
	        .replaceAll("Ã©", "&eacute;") //.replaceAll("Ã‰", "&Eacute;")
	        .replaceAll("Ã­", "&iacute;") //.replaceAll("Ã█", "&Iacute;")
	        .replaceAll("Ã³", "&oacute;") //.replaceAll("Ã“", "&Oacute;")
	        .replaceAll("Ãº", "&uacute;") //.replaceAll("Ãš", "&Uacute;")
	        .replaceAll("Ã±", "&ntilde;")
	        .replaceAll("Ã‘", "&Ntilde;");
	}
}