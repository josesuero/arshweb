var hostserver = location.hostname;
if (hostserver == "localhost")
	var proxy = false;
else
	var proxy = true;
var servidor = "http://" + hostserver + ":8080";
var cotizacion = {};
function ajaxrequest(table, fields, filter) {
	var url = "dbserver?table=" + table + "&field=" + fields + filter;

	var urlproxy;
	if (proxy === true) {
		url = encodeURIComponent(url);
		urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
	} else {
		urlproxy = url;
	}
	var value = false;
	$.ajax({
		type: "GET",
		url: urlproxy,
		async: false,
		success: function (result, funcname, resultobj) {
			value = eval(resultobj.responseText);
		},
		error: function (error) {
			alert(error);
			value = false;
		}
	});
	return value;
}
function sendemail(Data) {

	var url = "emaillocal?" + "data=" + encodeURIComponent(Data);
	if (proxy === true) {
		url = encodeURIComponent(url);
		urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
	} else {
		urlproxy = url;
	}
    /*var urlproxy;
    if (proxy === true) {
        url = encodeURIComponent(url);
        urlproxy = "proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
    } else {
        urlproxy = url;
    }*/
	var value = false;
	$.ajax({
		method: "POST",
		url: urlproxy,
		//        data: {data: Data},
		dataType: "jsonp",
		async: false
	})
		.success(function (result, funcname, resultobj) {
			value = eval(resultobj.responseText);
		})
		.error(function (error) {
			value = error.responseText;
		});
	return value;
}
var planes;
var opciones;
var opciones_planes;
var pagos;
var maintable;
var edades = [
	{ value: "0", text: "0-49 a\xf1os", },
	{ value: "1", text: "50-54 a\xf1os", },
	{ value: "2", text: "55-59 a\xf1os", },
	{ value: "3", text: "60-64 a\xf1os", },
	{ value: "4", text: "65-69 a\xf1os", },
	{ value: "5", text: "70-74 a\xf1os", },
	{ value: "6", text: "75-79 a\xf1os", },
	{ value: "7", text: "80 \xf3 m\xe1s a\xf1os", },
];
$(document).ready(function () {
	// planes = ajaxrequest("planes", 'id:id,name:name,tarifa:tarifa,mensual:mensual,opciones:opciones,opcionesDep:opcionesDep,sumMatern:sumMatern,descHijo:descHijo', "");
	planes = [
		{ id: "1", name: "Superior", mensual: "1395", conyuge: "1365", hijo: "1095", opciones: "360" },
		{ id: "1", name: "Royal", mensual: "1715", conyuge: "1685", hijo: "1350", opciones: "360" },
		{ id: "1", name: "Max", mensual: "2360", conyuge: "2330", hijo: "1865", opciones: "360" },
		{
			id: "1", name: "Platinum", mensual: "6340", conyuge: "6310", hijo: "5050", opciones: "360", porEdad: {
				0: { mensual: "6340", conyuge: "6310" },
				1: { mensual: "7600", conyuge: "7570" },
				2: { mensual: "9115", conyuge: "9085" },
				3: { mensual: "10930", conyuge: "10900" },
				4: { mensual: "13110", conyuge: "13080" },
				5: { mensual: "15725", conyuge: "15695" },
				6: { mensual: "18865", conyuge: "18835" },
				7: { mensual: "22630", conyuge: "22600" },
			}
		},
	];

	opciones = ajaxrequest("opciones", 'id:id,name:name,required:required', "");
	opciones_planes = ajaxrequest("opciones_planes", 'id:id,id_option:id_option,name:name,tarifa:tarifa,tarifa_dep:tarifa_dep', "");
	pagos = ajaxrequest("pagos", 'id:id,name:name,descuento:descuento,recargo:recargo,divisor:divisor', "");

	edades.forEach(function (edad) {
		$("#titular").append("<option value='" + edad.value + "'>" + edad.text + "</option>");
		$("#conyugue").append("<option value='" + edad.value + "'>" + edad.text + "</option>");
	});

	opciones = opciones
		// .sort(function(a, b){ return (a.orden>b.orden)? 1 : (a.orden<b.orden)? -1 : a.name > b.name})
		.map(function (opcion) {
			opcion.name = opcion.name
				.replace("Ultimos", "\xdaltimos");
			return opcion;
		});

	const opExcludeIDs = [];
	var opOdontologia = opciones_planes.filter(o => o.id_option == 1)
		.map(o => {
			o.id = o.id * 1;
			opExcludeIDs.push(o.id);
			return o;
		});;
	// var opOdontologiaHumano = opOdontologia.filter(o => o.name.startsWith('Dental Humano'));
	opOdontologia.sort(sortObjList('id'));
	var opGMM = opciones_planes.filter(o => o.id_option == 8)
		.map(o => {
			o.id = o.id * 1;
			opExcludeIDs.push(o.id);
			return o;
		});
	opGMM.sort(sortObjList('id'));

	opciones_planes = opciones_planes.filter(o => opExcludeIDs.indexOf(o.id) == -1);
	// opOdontologiaHumano.forEach((o, i) => opciones_planes.splice(i, 0, o));
	opOdontologia.forEach(o => opciones_planes.push(o));
	opGMM.forEach(o => opciones_planes.push(o));

	$("#titular").change(calcular);
	$("#conyugue").change(calcular);
	$("#H").keyup(calcular);

	$("#showAll").click(function () {
		for (var i = 0; i < planes.length; i++) {
			planes[i].visible = true;
			$("." + planes[i].name).show();
		}
	});

	maintable = $("#maintable");
	var thead = maintable.children("thead").children("#planes");
	for (var i = 0; i < planes.length; i++) {
		var plan = planes[i];
		planes[i].visible = true;
		var button = $("<button id='" + plan.name + "'></button>").text(plan.name.toUpperCase()).click(function () {
			for (var i = 0; i < planes.length; i++) {
				if (planes[i].name == this.id) {
					planes[i].visible = false;
					break;
				}
			}
			$("." + this.id).hide();
		});

		thead.append($("<td class='titulo2 " + plan.name + "' style='width:150px'></td>").append(button));
	}


	var tbody = $("tbody.opciones");
	for (var x = 0; x < opciones.length; x++) {
		var row = $("<tr></tr>");
		var cell = $("<td></td>");
		if (opciones[x].required == '1') {
			cell.append($("<input type='checkbox' id='" + opciones[x].name + "' checked disabled />").change(calcular));
		} else {
			cell.append($("<input type='checkbox' id='" + opciones[x].name + "' />").change(calcular));
		}
		cell.append($("<label for='" + opciones[x].name + "'></label>").html(opciones[x].name));


		row.append(cell);

		cell = $("<td></td>");

		var planesObj = $("<select class='option' style='width:200px'></select>").change(function () {
			var planesObj = $(this);
			var x = $(this).attr("planID");
			$(this).parent().find(".subopcion").children().remove();
			for (var i = 0; i < opciones_planes.length; i++) {
				if (opciones_planes[i].id_option === opciones[x].id) {
					if (opciones_planes[i].name.indexOf("|") === -1) {
						// planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
					} else {
						if (opciones_planes[i].name.split("|")[0] === $($(this).attr("options")[$(this).attr("selectedIndex")]).text()) {
							var name = opciones_planes[i].name.split("|");
							if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
								planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
							}

							var subopcion = $(this).parent().find(".subopcion");
							if (subopcion.length === 0) {
								subopcion = $("<select class='subopcion'></select>").change(calcular);
								$(this).parent().append(subopcion);
							}
							if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
								subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
							}
						}
					}
				}
			}
			var subopcion = $(this).parent().find(".subopcion");
			if (subopcion[0] && !subopcion[0].options.length) {
				$(subopcion[0]).hide();
			} else {
				$(subopcion[0]).show();
			}
			calcular();
		});
		planesObj.attr("planID", x);
		cell.append(planesObj);
		for (var i = 0; i < opciones_planes.length; i++) {
			if (opciones_planes[i].id_option === opciones[x].id) {
				if (opciones_planes[i].name.indexOf("|") === -1) {
					planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
				} else {
					var name = opciones_planes[i].name.split("|");
					if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
						planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
					}

					var subopcion = cell.find(".subopcion");
					if (subopcion.length === 0) {
						subopcion = $("<select class='subopcion'></select>").change(calcular);
						cell.append(subopcion);
					}
					if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
						subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
					}
				}
			}
		}

		row.append(cell);
		tbody.append(row);
		$('[planid=' + x + ']').change();
	}

	tbody = $(".calculable").children("tr");
	for (var i = 0; i < planes.length; i++) {
		var plan = planes[i];
		tbody.append($("<td class='tarifa " + plan.name + "' align='right'></td>").append($("<span type='text' class='calcular' id='" + plan.name + i + "'></span>").html(0)));
	}

	for (var i = 0; i < pagos.length; i++) {
		$("#opcionPagos").change(calcular).append("<option value='" + pagos[i].id + "'>" + pagos[i].name + "</option>");
	}
	calcular();
});
function round5(value) {
	var intValue = parseInt(value);
	var decimals = value - intValue;
	if (decimals > 0.5) {
		return intValue + 1;
	} else {
		return intValue;
	}
}
function getDescuento(value, cantidad) {
	var opcionPag = pagos[$("#opcionPagos").attr("selectedIndex")];
	var f1 = ((parseFloat(value) / cantidad) / opcionPag.divisor) || 0;
	var f2 = parseFloat(opcionPag.descuento) / 100;
	var descIndividual = f1 * f2;
	var result = round5(descIndividual) * opcionPag.divisor * cantidad;
	return result;
}
function acentosToHTML(string) {
	return string
		.replace("\xe1", "&aacute;").replace("\xe9", "&eacute;").replace("\xed", "&iacute;").replace("\xf3", "&oacute;").replace("\xfa", "&uacute;")
		.replace("\xc1", "&Aacute;").replace("\xc9", "&Eacute;").replace("\xcd", "&Iacute;").replace("\xd3", "&Oacute;").replace("\xda", "&Uacute;")
		.replace("\xf1", "&ntilde;").replace("\xd1", "&Ntilde;");
}
function calcular(e) {
	cotizacion = {
		planes: [], opciones: [], cliente: {}, totalAfiliados: 0, totalPlanes: {}, subTotal: {},
		descuento: {}, total: {}, cuota: {}, formaPago: "", bcc: "",
		subject: "Cotizaci\xf3n para Planes Locales - Humano", planDef: [], odontologia: "",
	};
	if ($("#titular").val() === "TM" && $("#conyugue").val() !== "O") {
		alert("No aplica c\u00f3nyuge con maternidad ya que el plan incluye maternidad");
		$(e.target).attr("selectedIndex", 0);
	}
	if ($("#titular").val() === "T" && $("#conyugue").val() === "TM") {
		alert("No Aplica titular y c\u00f3nyuge con maternidad");
		$(e.target).attr("selectedIndex", 0);
	}

	var totalafiliados = 0;
	planes.map((plan, i) => {
		return plan.total = 0;
	});
	totales = planes.map(function () { return 0; });
	totalesDesc = planes.map(function () { return 0; });

	var tbody = maintable.children(".control").children("tr");
	tbody.each(function () {
		var objectPlanes = {};
		var row = $(this);
		var control = row.find(".control");
		var controlID = control.attr("id");
		var controlText = control.text();
		tipotarifa = "H";
		var controlText = tipotarifa;
		cantidad = 1;
		if (controlID !== "H") {
			tipotarifa = control.val();
			
		} else {
			cantidad = control.val();
			if (cantidad === "") {
				cantidad = 0;
			}
			cantidad = parseInt(cantidad);
			controlText = tipotarifa;
		}
		if (tipotarifa === "O") {
			cantidad = 0;
			controlText = '';
		}
		objectPlanes.nombre = acentosToHTML(controlText);
		row.find(".calcular").each(function (i) {
			var plan = planes[i];
			var cargo = plan.mensual;

			if (controlID == 'H') {
				cargo = plan.hijo;
			} else if (plan.name == 'Platinum' && tipotarifa != 'O') {
				cargo = plan.porEdad[tipotarifa].mensual;
				if (controlID == 'conyugue') {
					cargo = plan.porEdad[tipotarifa].conyuge;
				}
			} else if (controlID == 'conyugue') {
				cargo = plan.conyuge;
			}

			var tarifa = Math.round(cargo * 12 * cantidad);

			totales[i] += tarifa;
			totalesDesc[i] += getDescuento(tarifa, cantidad);
			plan.total += tarifa;
			$(this).html(tarifa);
			if (tarifa == 0) tarifa = "";
			objectPlanes[plan.name] = tarifa;
		});
		cotizacion.planes[cotizacion.planes.length] = objectPlanes;
		totalafiliados += cantidad;
	});


	tbody = $("tbody.totales").children("tr");
	tbody.each(function () {
		var row = $(this);
		row.find(".calcular").each(function (i) {
			$(this).html(planes[i].total);
			cotizacion.totalPlanes[planes[i].name] = window.formatNumber.new(planes[i].total, "");
		});
	});

	var id_option = 0;
	var incluido = 0;//para en Vida incluir a titular y cobrar los demas y mostrar incluido aun
	var SeguroViajero = [];
	tbody = $("tbody.opciones").children("tr");
	tbody.each(function (x) {
		var row = $(this);
		tarifa = 0;
		var objectTarifa = {};
		objectTarifa.nombre = opciones[x].name;
		var calcularOpcion = row.find("input[type='checkbox']").attr("checked");
		var name = "";
		if (calcularOpcion === true) {
			id_option = opciones[x].id;
			name = row.find(".option").val();
			subopcion = row.find(".subopcion");
			if (subopcion.length > 0) {
				if (subopcion.val()) {
					name += "|" + subopcion.val();
					subopcion.show
				} else {

				}
			}
			objectTarifa.plan = name;
			for (var i = 0; i < opciones_planes.length; i++) {
				if (opciones_planes[i].id_option === id_option && opciones_planes[i].name === name) {
					tarifa = parseFloat(opciones_planes[i].tarifa);
					tarifa += parseFloat(opciones_planes[i].tarifa_dep * (totalafiliados - 1));
				}
			}
		}
		var temptarifa = tarifa;
		row.find(".calcular").each(function (i) {
			String.prototype.startsWith = function (str) {
				return this.indexOf(str) == 0;
			}
			String.prototype.contains = function (str) {
				return this.indexOf(str) > -1;
			}
			//////////////////
			var plan = $(this).attr("id");
			var notAvail = 0;
			tarifa = Math.ceil(temptarifa);
			if (objectTarifa.nombre == 'Habitaci\xf3n' || objectTarifa.nombre == 'Habitacion') {
				if (name && name.indexOf('Superior') == -1 && plan.indexOf("Superior") !== -1) {
					notAvail = 1;
					tarifa = 0;
				}
				if (name && name.indexOf('Royal') == -1 && plan.indexOf("Royal") !== -1) {
					notAvail = 1;
					tarifa = 0;
				}
				if (name && name.indexOf('Max') == -1 && plan.indexOf("Max") !== -1) {
					notAvail = 1;
					tarifa = 0;
				}
				if (name && name.indexOf('Platinum') == -1 && plan.indexOf("Platinum") !== -1) {
					notAvail = 1;
					tarifa = 0;
				}
			}
			if (name && objectTarifa.nombre == 'Odontologia' || objectTarifa.nombre == 'Odontolog\xeda') {
				if (name.contains('Humano')) {
					cotizacion.odontologia = "";
					incluido = 1;
					tarifa = 0;
					// if (name.contains('Servidens') && !plan.contains('Superior')) notAvail = 1;
					if (plan.contains('Platinum')) {
						notAvail = 1;
					}
				} else if (name.contains('Superior')) {
					cotizacion.odontologia = "Superior";
					if (plan.contains('Platinum')) {
						notAvail = 1;
						tarifa = 0;
					}
				} else if (name.contains('Royal')) {
					cotizacion.odontologia = "Royal";
					if (plan.contains('Platinum')) {
						notAvail = 1;
						tarifa = 0;
					}
				} else if (name.contains('Max')) {
					cotizacion.odontologia = "Max";
					if (plan.contains('Platinum')) {
						notAvail = 1;
						tarifa = 0;
					}
				} else if (name.contains('Platinum')) {
					cotizacion.odontologia = "Platinum";
					if (!plan.contains("Platinum")) {
						notAvail = 1;
						tarifa = 0;
					}
				}
			}

			if (name && objectTarifa.nombre == 'Vida') {
				tarifa /= totalafiliados;//solo cobra a titular
				if (name == "RD$100,000") {
					//if(tarifa>0)
					tarifa = 0; 		//quitado a peticion tarifa -= 360;		//solo no cobra titular //-tarifa de vida
					incluido = 1;		//aun lo muestre incluido
				}
			}
			if (objectTarifa.nombre == 'GMM') {
				if (name == "RD$600,000") {
					if (plan.contains("Superior") || plan.contains("Royal")) {
						tarifa = 0;
						incluido = 1;
					} else if (plan.contains("Max") || plan.contains("Platinum")) {
						tarifa = 0;
						notAvail = 1;
					}
				}
				if (name == "RD$750,000") {
					if (plan.contains("Max")) {
						tarifa = 0;
						incluido = 1;
					} else {
						tarifa = 0;
						notAvail = 1;
					}
				}
				if (name == "RD$1,000,000") {
					if (plan.contains("Platinum")) {
						tarifa = 0;
						notAvail = 1;
					}
				}
				if (name == "RD$2,500,000") {
					if (plan.contains("Platinum")) {
						tarifa = 0;
						incluido = 1;
					} else {
						tarifa = 0;
						notAvail = 1;
					}
				}
			}
			if (name && objectTarifa.nombre == 'Seguro Viajero') {
				if (name && name.indexOf('Platinum') == -1 && plan.indexOf("Platinum") !== -1) {
					tarifa = 0;
					incluido = 1;
				}
				SeguroViajero[i] = tarifa;
			}
			if (name == "RD$ 50,000" && /*id_option == 8*/ objectTarifa.nombre == 'Enfermedades Mayores') {
				tarifa = 0;
				incluido = 1;
			}
			if (objectTarifa.nombre == 'Ultimos Gastos' || objectTarifa.nombre == '\xdaltimos Gastos') {
				if (name == "50,000" && name.indexOf('Max') == -1 && plan.indexOf("Max") !== -1) {
					tarifa = 0;
					incluido = 1;
				}
				if (name == "50,000" && name.indexOf('Platinum') == -1 && plan.indexOf("Platinum") !== -1) {
					notAvail = 1;
					tarifa = 0;
					incluido = 1;
				}
				if (name == "100,000" && name.indexOf('Platinum') == -1 && plan.indexOf("Platinum") !== -1) {
					tarifa = 0;
					incluido = 1;
				}
			}

			if (name && objectTarifa.nombre == 'Medicina Ambulatoria') {
				var monto = parseInt(name.replace(/,/g, ''));
				if (monto == 10000 && name.indexOf('Platinum') == -1 && plan.indexOf("Platinum") !== -1) {
					tarifa = 0;
					incluido = 1;
				}
                /*else if (monto<10000 && name.indexOf('Platinum') == -1 && plan.indexOf("Platinum") == 0){
                   notAvail = 1;
                   tarifa = 0;
                }*/
			}
			if (name == "Alert Plus" && objectTarifa.nombre == 'Aero-Ambulancia'
				&& (name.indexOf('Platinum') == -1 || name.indexOf('Max') == -1)
				&& (plan.indexOf("Platinum") !== -1 || plan.indexOf("Max") !== -1)
			) {
				tarifa = 0;
				incluido = 1;
			}
			/////////////////

			if (($("#titular").val() == "T75" || $("#titular").val() == "T65") && id_option == 6) {
				notAvail = 1;
				tarifa = 0;
				incluido = 1;
				objectTarifa.plan = "N/A";
			}

			if (isNaN(parseInt(tarifa)))
				totales[i] += 0;
			else {
				totales[i] += parseInt(tarifa);
				if (name && objectTarifa.nombre != 'Seguro Viajero')
					totalesDesc[i] += getDescuento(parseInt(tarifa), 1);

			}
			if (incluido === 1 && calcularOpcion === true) {
				$(this).html("incluido");
				tarifa = "incluido";
			} else {
				$(this).html(tarifa);
				if (tarifa == 0) tarifa = "";
			}
			if (notAvail == 1) {
				$(this).html("N/A");
				tarifa = "N/A";
			}
			objectTarifa[planes[i].name] = tarifa;
			incluido = 0;
		});
		objectTarifa.nombre = acentosToHTML(objectTarifa.nombre);
		if (objectTarifa.plan) {
			objectTarifa.plan = acentosToHTML(objectTarifa.plan);
		}
		cotizacion.opciones[cotizacion.opciones.length] = objectTarifa;
	});

	$("#totalAfiliados").html(totalafiliados);
	cotizacion.totalAfiliados = totalafiliados;

	tbody = $("tbody.totalOpciones").children("tr");
	tbody.each(function () {
		var row = $(this);
		row.find(".calcular").each(function (i) {
			$(this).html(totales[i] - planes[i].total);
		});
	});
	tbody = $("tbody.totalGeneral").children("tr");
	tbody.each(function () {
		var row = $(this);
		row.find(".calcular").each(function (i) {
			$(this).html(totales[i]);
			cotizacion.subTotal[planes[i].name] = totales[i];
		});
	});

	var descuento = [];
	tbody = $("tbody.Descuento").children("tr");
	tbody.each(function () {
		var row = $(this);
		row.find(".calcular").each(function (i) {
			var opcionPag = $("#opcionPagos").attr("selectedIndex");
			descuento[i] = totalesDesc[i];

			$("#descuento").html(Math.round(parseFloat(pagos[opcionPag].descuento), 0));
			$(this).html(descuento[i]);
			cotizacion.descuento[planes[i].name] = descuento[i];
			var opcionPagoletra = "";
			switch (opcionPag) {
				case 0:
					opcionPagoletra = "Anual";
					break;
				case 1:
					opcionPagoletra = "Semestral";
					break;
				case 2:
					opcionPagoletra = "Trimestral";
					break;
				case 3:
					opcionPagoletra = "Mensual";
					break;
			}
			cotizacion.formaPago = opcionPagoletra;
		});
	});

	tbody = $("tbody.total").children("tr");
	tbody.each(function () {
		var row = $(this);
		row.find(".calcular").each(function (i) {
			var total = (totales[i]) - descuento[i];
			$(this).html(total);
			cotizacion.total[planes[i].name] = total;
		});
	});

	tbody = $("tbody.Cuota").children("tr");
	tbody.each(function () {
		var row = $(this);
		row.find(".calcular").each(function (i) {
			var divisor = pagos[$("#opcionPagos").attr("selectedIndex")].divisor;
			var cuota = Math.round((totales[i] - descuento[i]) / divisor);

			cuota = forcedSetting(cuota, i);

			$(this).html(cuota);
			cotizacion.cuota[planes[i].name] = cuota;
		});
	});

	cotizacion.planDef = planes;

	tbody = $("#tabla2").find(".cliente");
	tbody.each(function () {
		cotizacion.cliente[this.id] = acentosToHTML($(this).val());
	});

	$(".calcular").prettynumber({
		delimiter: ','
	});
}
function forcedSetting(cuota, planIndex) {
	if (planIndex == 2) {
		var divisor = pagos[$("#opcionPagos").attr("selectedIndex")].divisor;
		if ($("#conyugue").val() !== "O") {
			if (divisor == 4)
				cuota = cuota - 1;
			else if (divisor == 2)
				cuota = cuota + 1;
		}
		if (getCantidadHijos() > 0) {
			if (divisor == 4)
				cuota = cuota - 1;
			else if (divisor == 2)
				cuota = cuota + 1;
		}
	}
	return cuota;
}
function getCantidadHijos() {
	var cantidad = $('#H').val();
	if (cantidad === "") {
		cantidad = 0;
	}
	return parseInt(cantidad);
}

function sendMail() {
	calcular();
	//var url = "emaillocal";
	//var urlproxy;
	//var data;
    /*
	if (proxy == true){
		//url = encodeURIComponent(url + "?data=" + JSON.stringify(cotizacion));
		url = url + "?data=" + JSON.stringify(cotizacion);
		urlproxy = "proxy.php";
		data = {url:"http://172.24.206.227:8080/" + url, mode:'native', REQUEST_METHOD:"POST"};
		//mode=native&url=http://172.24.206.227:8080/"+url
	} else {
		urlproxy = url;
		data = {data:encodeURIComponent(JSON.stringify(cotizacion))};
	}

	urlproxy = "http://cotizador.arshumano.com.do:8080/emaillocal";
	data = {data:JSON.stringify(cotizacion)};
	$.ajax({type: "POST",
		url:urlproxy,
		async:false,
		data:data,
		success:function(result,funcname, resultobj){
			alert(resultobj.responseText);
		},
		error:function(error){
			alert(error.responseText);
		}
	});		
	*/
	//$("#printForm").attr("action","http://cotizador.arshumano.com:8080/emaillocal");
	//$("#printForm").attr("action","http://localhost:8080/emaillocal");
	//$("#printData").val(JSON.stringify(cotizacion));
	$("#printForm").attr("target", "emailFrame");
	//$("#printForm").submit();
	//setTimeout('alert($("#emailFrame").contents())',500)
	///Modificacion hecha por CManzu..
	cotizacion.bcc = $('#txtMailR').val();
	if ($('#txtMailC').val() !== '') {
		//$.post(servidor + "/emaillocal",{data: JSON.stringify(cotizacion)},function(e){
		var e = sendemail(JSON.stringify(cotizacion));
        /*if(e=="E-Mail Enviado!")
    	   alert(e);
    	else
    		alert("No se envi\xf3 el correo!.\n Ha ocurrido un problema. Compruebe que los e-mails indicados son v\xe1lidos");
    		$("#txtMailC").focus();*/
		setTimeout('alert("Email enviado")', 500);
	}
	else
		alert("El campo Mail para cliente es requerido para el env\xedo.");
}
function print() {
	calcular();
    /*var url = "";
	url += "/birt/frameset";
	url += "?__report=arshlocal.rptdesign";//&data="+JSON.stringify(cotizacion); 
	url += "&__format=pdf";
	//window.open("http://cotizador.arshumano.com:8080" + url);*/

	//$("#printForm").attr("action","http://cotizador.arshumano.com:8080/birt/frameset");
	$("#printForm").attr("action", servidor + "/birt/frameset");
	$("#printForm").attr("target", "_new");
	$("#printData").val((JSON.stringify(cotizacion)));
	$("#printForm").submit();
}
window.formatNumber = {
	/*Example use: formatNumber.new(123456779.18, "$")*/
	separador: ",", /* separador para los miles*/
	sepDecimal: '.', /* separador para los decimales*/
	formatear: function (num) {
		num += '';
		var splitStr = num.split('.');
		var splitLeft = splitStr[0];
		var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		var regx = /(\d+)(\d{3})/;
		while (regx.test(splitLeft)) {
			splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		}
		return this.simbol + splitLeft + splitRight;
	},
	new: function (num, simbol) {
		this.simbol = simbol || '';
		return this.formatear(num);
	}
}
function sortObjList(prop, desc) {
	return (a, b) => {
		const reverse = desc ? -1 : 1;
		if (a[prop] < b[prop]) {
			return -1 * reverse;
		}
		if (a[prop] > b[prop]) {
			return 1 * reverse;
		}
		return 0;
	};
}
