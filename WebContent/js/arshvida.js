var proxy = true;
var dec = 0;

var cantPlanes = {};

var current;
var cotizacion = {};

var primas = [];
	
$(document).ready(function(){
	addRow("detalles");
	addRowSueldo("detallesSueldo");
	addRow("detallesSuma");
	$("#tabs").tabs();
	
	var url = "dbserver?table=pricelist_details&field=key:plan_key,value:plan_value&id_list=14";
	//proxy change
	var urlproxy;
	if (proxy === true){
		url = encodeURIComponent(url);
		urlproxy = "proxy.php?mode=native&url=http://172.24.206.227:8080/"+url;
	}  else {
		urlproxy = url;
	}
	
	$.ajax({type: "GET",
		url:urlproxy,
		async:false,
		success:function(result,funcname, resultobj){
			primas = eval(resultobj.responseText);
		},
		error:function(error){
			alert(error);
		}
	});
	
});
function addCommas(nStr){
	nStr += "";
	nStr = parseFloat(nStr.replace(/,/g,'')).toFixed(dec);
	var x = nStr.split(".");
	var x1 = x[0];
	var x2 = x.length > 1 ? "." + x[1]: "";
	var rgx= /(\d+)(\d{3})/;
	while(rgx.test(x1)){
		x1 = x1.replace(rgx, "$1" + "," + "$2");
	}
	return x1+x2;
}

function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

function sendMail(){
	calcular();
	var url = "emailvida";
	var urlproxy;
	var data;
	if (proxy == true){
		//url = encodeURIComponent(url + "?data=" + JSON.stringify(cotizacion));
		url = url + "?data=" + JSON.stringify(cotizacion);
		urlproxy = "proxy.php";
		data = {url:"http://172.24.206.227:8080/" + url, mode:'native', REQUEST_METHOD:"POST"};
		//mode=native&url=http://172.24.206.227:8080/"+url
	} else {
		urlproxy = url;
		data = {data:encodeURIComponent(JSON.stringify(cotizacion))};
	}
	$.ajax({type: "POST",
		url:urlproxy,
		async:false,
		data:data,
		success:function(result,funcname, resultobj){
			alert(resultobj.responseText);
		},
		error:function(error){
			alert(error.responseText);
		}
	});		
	
	
} 

function print(category){
/*	
	var url = "http://172.24.206.227:8080";
	url += "/birt/frameset?";
	//postdata = "__report=vivir" + category + ".rptdesign&data="+JSON.stringify(cotizacion); 
	//postdata += "&__format=pdf";
	var postdata = {__report:"vivir" + category + ".rptdesign",data:JSON.stringify(cotizacion),__format:"pdf"};
	$.post(url, postdata,function(data){
		var win=window.open("about:blank");
		with(win.document)
		{
			open();
			write(data);
			close();
		}
		
	},function(e){
		alert(e);
	});
	//window.open("http://cotizador.arshumano.com:8080" + url);
	/
	/*
	var form = $('<form></form>');
	form.attr( "method","POST"); 
	form.attr("action","http://cotizador.arshumano.com:8080/birt/frameset");
	form.attr("target","_blank");
	var field = $('<input type="hidden" name="__report" value="vivir' + category + '.rptdesign">');
	form.append(field);
	field = $('<input type="hidden" name="data" >');
	field.attr("value", JSON.stringify(cotizacion));
	form.append(field);
	field = $('<input type="hidden" name="__format" value="pdf">');
	form.append(field);
	//window.open("","reportWindow");
	*/
	$("#__report").val("vivir" + category + ".rptdesign");
	$("#data").val(JSON.stringify(cotizacion));
	
	$("#printform").submit();
}

function report(type, category){

	if ($("#cliente").val() === ""){
		alert("El campo Solicitante no puede estar vacio");
		$("#cliente").focus();
		return false;
	}
	if ($("#tiponegocio").val() === "http://172.24.206.227:8080"){
		alert("El campo naturaleza negocio no puede estar vacio");
		$("#tiponegocio").focus();
		return false;
	}

	if ($("#telefono").val() === ""){
		alert("El campo Tel�fono no puede estar vacio");
		$("#telefono").focus();
		return false;
	}

	if ($("#email").val() === ""){
		alert("El campo E-Mail no puede estar vacio");
		$("#email").focus();
		return false;
	}

	if ($("#intermediario").val() === ""){
		alert("El campo Intermediario no puede estar vacio");
		$("#intermediario").focus();
		return false;
	}
	
	if ($("#gerente").val() === ""){
		alert("El campo Gerente no puede estar vacio");
		$("#gerente").focus();
		return false;
	}
	
	if (type === "report"){
		print(category);
	} else if (type === "email"){
		sendMail(category);
	}
}
function cleanTable(){
	$(".tarifa").html(0);
	$(".campoTabla").html(0).parent().css("background-color","white");
}

function addValue(key, result){
	for(var y = 0; y < result.length;y++){
		if (result[y].key.indexOf(key) != -1){
			var fieldkey = result[y].key.replace(key,"").replace(" - ","").replace("/","");
			
			var value = $("#" + fieldkey).html().replace(/,/g,"");
			if (value == ""){
				value = 0;
			}
			value = parseFloat(value);
			$("#" + fieldkey).html(formatValue(value + Math.ceil(parseFloat(result[y].value))));
		}
	}
}
function formatValue(value){
	return addCommas(value);
}
/*function sumar(){
	var total = 0;
	$(".tarifa").each(function(i, obj){
		var value = $(obj).html();
		if(value != ""){
			total += parseFloat(value);
		};
	})
	
	//.53
	//.2750
}*/
function limpiarTarifas(){
	cleanTable();
}

function calcular(tabla){
	cotizacion = {titulares:[]};
	
	var edad = $("#edad").val();
	if (edad === ""){
		alert("Debe colocar la edad promedio del grupo");
		$("#edad").focus();
		return;
	}
	
	var MONTO100 = parseFloat($("#MONTO100").val().replace(/,/g,""));
	var MONTO250 = parseFloat($("#MONTO250").val().replace(/,/g,""));
	var MONTO500 = parseFloat($("#MONTO500").val().replace(/,/g,""));
	var MONTO1000 = parseFloat($("#MONTO1000").val().replace(/,/g,""));
	
	var rows = $("#" + tabla).children("#detailBody").children("tr");
	cleanTable();
	var total = 0;
	
	var tasa = 0;
	var mad = 0;
	var paci = 0;
	var reho = 0;
	var ulga = 0;
	
	var tasaVida = 0;
	var tasaMad = 0;
	var tasaPaci = 0;
	var tasaReho = 0;
	var tasaUlga = 0;
	
	
	var vidamin = 0;
	for (var i = 0; i < primas.length; i++){
		if (primas[i].key === edad){
			tasaVida = roundNumber(parseFloat(primas[i].value), 2);
			tasa = tasaVida/1000;
			$("#tasaVida").html("(" + tasaVida + ")");
		} else if (primas[i].key === "MAD"){
			tasaMad = roundNumber(parseFloat(primas[i].value), 2); 
			mad = tasaMad/1000;			
			$("#tasaMAD").html("(" + tasaMad + ")");
		} else if (primas[i].key === "PACI"){
			tasaPaci = roundNumber(parseFloat(primas[i].value), 2); 
			paci = tasaPaci/1000;
			$("#tasaPACI").html("(" + tasaPaci + ")");
		} else if (primas[i].key === "REHO"){
			tasaReho = roundNumber((parseFloat(primas[i].value)), 2);
			reho = (tasaReho  * 90) /1000;
			$("#tasaREHO").html("(" + tasaReho + ")");
		} else if (primas[i].key === "ULGA"){
			tasaUlga = roundNumber(parseFloat(primas[i].value), 2);
			ulga = tasaUlga/1000;
			$("#tasaULGA").html("(" + tasaUlga + ")");
		} else if (primas[i].key === "VIDAMIN"){
			vidamin = roundNumber(parseFloat(primas[i].value), 2); 
		}
	}
	total100 = 0;
	total250 = 0;
	total500 = 0;
	total1000 = 0;
	totalcant = 0;
	
	
	for(var x = 0; x < rows.length; x++){
		var suma = 0;
		row =  $(rows[x]);
		//var row = $(this).parent().parent();
		var vida = parseFloat(row.find("#vida").val().replace(/,/g, ""));
		
		if(!isNaN(vida) && vida !== 0){
			
			if (vida < vidamin){
				alert("La cobertura de vida no puede ser menor a " + formatValue(vidamin));
				row.find("#vida").val();
				return;
			}
			
			var cantidad = row.find("#cantidad").val();
			if (cantidad === ""){
				cantidad = 1;
				row.find("#cantidad").val(formatValue(1));
			}  else {
				cantidad = parseFloat(cantidad);
			}
			
			totalcant += cantidad;
			suma += vida;
			row.find("#vida").val(formatValue(vida));
			vidaVAL = vida * tasa;
			
			total100 += MONTO100 * tasa;
			total250 += MONTO250 * tasa;
			total500 += MONTO500 * tasa;
			total1000 += MONTO1000 * tasa;
			
			var MADVal = parseFloat(row.find("#mad").val().replace(/,/g, ""));
			if (isNaN(MADVal)){
				MADVal = vida;
			}
			if (vida < MADVal){
				alert("La cobertura de MAD no puede ser mayor a la de vida");
				row.find("#mad").focus();
				return;
			}
			suma += MADVal;
			row.find("#mad").val(formatValue(MADVal));
			var MADsa = MADVal;
			MADVal = MADVal * mad;
			
			total100 += MONTO100 * mad;
			total250 += MONTO250 * mad;
			total500 += MONTO500 * mad;
			total1000 += MONTO1000 * mad;
			
			var PACIVal = parseFloat(row.find("#paci").val().replace(/,/g, ""));
			if (isNaN(PACIVal)){
				PACIVal = vida;
			}
			if (vida < PACIVal){
				alert("La cobertura de PACI no puede ser mayor a la de vida");
				row.find("#paci").focus();
				return;
			}

			suma += PACIVal;
			row.find("#paci").val(formatValue(PACIVal));
			
			var PACIsa = PACIVal;
			PACIVal = PACIVal * paci;
			
			total100 += MONTO100 * paci;
			total250 += MONTO250 * paci;
			total500 += MONTO500 * paci;
			total1000 += MONTO1000 * paci;			
			
			var REHOVal = parseFloat(row.find("#reho").val().replace(/,/g, ""));
			if (isNaN(REHOVal)){
				REHOVal = 0;
			}
			if (vida < REHOVal){
				alert("La cobertura de REHO no puede ser mayor a la de vida");
				row.find("#reho").focus();
				return;
			}
			suma += REHOVal;
			var REHOsa = REHOVal;
			REHOVal = REHOVal * reho;
			/*
			total100 += MONTO100 * reho;
			total250 += MONTO250 * reho;
			total500 += MONTO500 * reho;
			total1000 += MONTO1000 * reho;
			*/
			total100 += REHOVal;
			total250 += REHOVal;
			total500 += REHOVal;
			total1000 += REHOVal;
			
			var ULGAVal = parseFloat(row.find("#ulga").val().replace(/,/g, ""));
			if (isNaN(ULGAVal)){
				ULGAVal = 0;
			}
			if (ULGAVal != 0 && ULGAVal < 20000){
				alert("El valor de �ltimos gastos no puede ser menor a RD$20,000");
				row.find("#ulga").val(formatValue(20000));
				row.find("#ulga").focus();
				return;
			}
			if (vida < ULGAVal){
				alert("La cobertura de ULGA no puede ser mayor a la de vida");
				row.find("#ulga").focus();
				return;
			}
			suma += ULGAVal;
			var ULGAsa = ULGAVal;
			ULGAVal = ULGAVal * ulga;
			/*
			total100 += MONTO100 * ulga;
			total250 += MONTO250 * ulga;
			total500 += MONTO500 * ulga;
			total1000 += MONTO1000 * ulga;
			*/
			
			total100 += ULGAVal;
			total250 += ULGAVal;
			total500 += ULGAVal;
			total1000 += ULGAVal;
			
			total100 = Math.ceil(total100 * 1.16);
			total250 = Math.ceil(total250 * 1.16);
			total500 = Math.ceil(total500 * 1.16);
			total1000 = Math.ceil(total1000 * 1.16);
			
			var tarifaPP = Math.ceil((vidaVAL + MADVal + PACIVal + REHOVal + ULGAVal)  * 1.16);
			var tarifa = tarifaPP  * cantidad;
			//row.find("#suma").html(formatValue(suma));
			row.find("#tarifa").html(formatValue(tarifa));
			row.find("#tarifaPP").html(formatValue(tarifaPP));
			

			total += tarifa;

			cotizacion.titulares[x] = {};
			cotizacion.titulares[x].cantidad = cantidad;
			cotizacion.titulares[x].categoria = row.find("#nombre").val();
			cotizacion.titulares[x].vida = vida;
			cotizacion.titulares[x].mad = MADsa;
			cotizacion.titulares[x].paci = PACIsa;
			cotizacion.titulares[x].reho = REHOsa;
			cotizacion.titulares[x].ulga = ULGAsa;
			cotizacion.titulares[x].tarifaPP = tarifaPP;
			cotizacion.titulares[x].tarifa = tarifa;

		}
		
		$("#" + tabla).find("#anual").html(formatValue(total * 12));
		$("#" + tabla).find("#semestral").html(formatValue(total * 6));
		$("#" + tabla).find("#trimestral").html(formatValue(total * 3));
		$("#" + tabla).find("#mensual").html(formatValue(total));
		if (total < 1000){
			$("#" + tabla).find("#mensual").html(formatValue(0));
		}
		
		
		cotizacion.total = formatValue(total);
		cotizacion.anual = formatValue(total * 12);
		cotizacion.semestral = formatValue(total * 6);
		cotizacion.trimestral = formatValue(total * 3);
		cotizacion.mensual = formatValue(total);
		if (total < 1000){
			cotizacion.mensual = formatValue(0);
		}
		
		cotizacion.MONTO100 = MONTO100;
		cotizacion.totalPP100 = total100;
		cotizacion.totalPGA100 = total100 * totalcant * 12;
		cotizacion.totalPGS100 = total100 * totalcant * 6;
		cotizacion.totalPGT100 = total100 * totalcant * 3;
		cotizacion.totalPGM100 = total100 * totalcant;
		
		cotizacion.MONTO250 = MONTO250;
		cotizacion.totalPP250 = total250;
		cotizacion.totalPGA250 = total250 * totalcant * 12;
		cotizacion.totalPGS250 = total250 * totalcant * 6;
		cotizacion.totalPGT250 = total250 * totalcant * 3;
		cotizacion.totalPGM250 = total250 * totalcant;
		
		cotizacion.MONTO500 = MONTO500;
		cotizacion.totalPP500 = total500;
		cotizacion.totalPGA500 = total500 * totalcant * 12;
		cotizacion.totalPGS500 = total500 * totalcant * 6;
		cotizacion.totalPGT500 = total500 * totalcant * 3;
		cotizacion.totalPGM500 = total500 * totalcant ;
		
		cotizacion.MONTO1000 = MONTO1000;
		cotizacion.totalPP1000 = total1000;
		cotizacion.totalPGA1000 = total1000 * totalcant * 12;
		cotizacion.totalPGS1000 = total1000 * totalcant * 6;
		cotizacion.totalPGT1000 = total1000 * totalcant * 3;
		cotizacion.totalPGM1000 = total1000 * totalcant ;		
		
		cotizacion.cliente = new Object();
		cotizacion.cliente.nombre = $("#cliente").val();
		cotizacion.cliente.telefono = $("#telefono").val();
		cotizacion.cliente.tipo = $("#tiponegocio").val();
		cotizacion.cliente.gerente = $("#gerente").val();
		cotizacion.cliente.intermediario = $("#intermediario").val();
		cotizacion.cliente.email = $("#email").val();
		cotizacion.cliente.bcc = "";
		cotizacion.cliente.edad = $("#edad").val();
		cotizacion.cliente.moneda = $("#moneda").val();
		
		cotizacion.tasaVida = tasaVida;
		cotizacion.tasaMad = tasaMad;
		cotizacion.tasaPaci = tasaPaci;
		cotizacion.tasaReho = tasaReho;
		cotizacion.tasaUlga = tasaUlga;

		
		cotizacion.tabla = {};
	}
	
	if (totalcant < 5){
		alert("No puede cotizar menos de 5 personas y la cobertura de vida debe ser mayor de " + addCommas(vidamin));
		return;
	}
	
	return true;
}

function calcularSueldo(){
	cotizacion = {titulares:[]};
	
	var edad = $("#edad").val();
	if (edad === ""){
		alert("Debe colocar la edad promedio del grupo");
		$("#edad").focus();
		return false;
	}
	
	var rows = $("#detallesSueldo").children("#detailBody").children("tr");
	cleanTable();
	var totalM1 = 0;
	var totalM2 = 0; 
	var totalM3 = 0;
	
	totalcant = 0;
	
	var tasa = 0;
	var mad = 0;
	var paci = 0;
	var reho = 0;
	var ulga = 0;
	var vidamin = 0;
	
	var tasaVida = 0;
	var tasaMad = 0;
	var tasaPaci = 0;
	var tasaReho = 0;
	var tasaUlga = 0;
	
	var vidamax = parseFloat($("#sumaMaxima").val().replace(/,/g,"")); 
	for (var i = 0; i < primas.length; i++){
		if (primas[i].key === edad){
			tasaVida = roundNumber(parseFloat(primas[i].value), 2);
			tasa = tasaVida/1000;
			$("#tasaVida").html("(" + tasaVida + ")");
		} else if (primas[i].key === "MAD"){
			tasaMad = roundNumber(parseFloat(primas[i].value), 2); 
			mad = tasaMad/1000;
			
			$("#tasaMAD").html("(" + tasaMad + ")");
		} else if (primas[i].key === "PACI"){
			tasaPaci = roundNumber(parseFloat(primas[i].value), 2); 
			paci = tasaPaci/1000;
			$("#tasaPACI").html("(" + tasaPaci + ")");
		} else if (primas[i].key === "REHO"){
			tasaReho = roundNumber((parseFloat(primas[i].value) * 90), 2);;
			reho = tasaReho/1000;
			$("#tasaREHO").html("(" + tasaReho + ")");
		} else if (primas[i].key === "ULGA"){
			tasaUlga = roundNumber(parseFloat(primas[i].value), 2);
			ulga = tasaUlga/1000;
			$("#tasaULGA").html("(" + tasaUlga + ")");
		} else if (primas[i].key === "VIDAMIN"){
			vidaminDef = roundNumber(parseFloat(primas[i].value), 2); 
		}
	}
	//$("#sumaMinima").val(formatValue(vidamin));
	vidamin = parseFloat($("#sumaMinima").val().replace(/,/g, ""));
	if (vidamin < vidaminDef){
		alert("El minimo no puede ser menor a " + formatValue(vidaminDef));
		$("#sumaMinima").focus();
		return false;
	}
	for(var x = 0; x < rows.length; x++){
		var suma = 0;
		row =  $(rows[x]);
		//var row = $(this).parent().parent();
		
		var cantidad = row.find("#cantidad").val();
		if (cantidad == ""){
			cantidad = 1;
			row.find("#cantidad").val(1);
		} else {
			cantidad = parseFloat(cantidad.replace(/,/g,""));
		}
		
		totalcant += cantidad;		
		var sueldo = parseFloat(row.find("#sueldoval").val().replace(/,/g, ""));
		if (isNaN(sueldo)){
			return false;
		}
		
		var sa1 = Math.ceil((sueldo * $("#multi1").val()) / 1000) * 1000;
		if (sa1 < vidamin){
			sa1 = vidamin;
		} else if (sa1 > vidamax){
			sa1 = vidamax;
		}		
		var sa2 = Math.ceil((sueldo * $("#multi2").val()) / 1000) * 1000;
		if (sa2 < vidamin){
			sa2 = vidamin;
		} else if (sa2 > vidamax){
			sa2 = vidamax;
		}
		var sa3 = Math.ceil((sueldo * $("#multi3").val()) / 1000) * 1000;
		if (sa3 < vidamin){
			sa3 = vidamin;
		} else if (sa3 > vidamax){
			sa3 = vidamax;
		}
		
		var P1 = 0; 
		var P2 = 0;
		var P3 = 0;

		
		if ($("#vidaCHK").is(":checked")){
			suma += tasa;
			P1 += Math.ceil(sa1 * tasa) * 1.16; 
			P2 += Math.ceil(sa2 * tasa) * 1.16;
			P3 += Math.ceil(sa3 * tasa) * 1.16;
			
		}
		if ($("#madCHK").is(":checked")){
			suma += mad;
			P1 += Math.ceil(sa1 * mad * 1.16); 
			P2 += Math.ceil(sa2 * mad * 1.16);
			P3 += Math.ceil(sa3 * mad * 1.16);
			
		}
		if ($("#paciCHK").is(":checked")){
			suma += paci;
			P1 += Math.ceil(sa1 * paci * 1.16); 
			P2 += Math.ceil(sa2 * paci * 1.16);
			P3 += Math.ceil(sa3 * paci * 1.16);
		}
		if ($("#rehoCHK").val() !== "0"){
			suma += reho;
			var rehoVAL = parseFloat($("#rehoCHK").val());  
			P1 += Math.ceil(rehoVAL * reho * 1.16); 
			P2 += Math.ceil(rehoVAL * reho * 1.16);
			P3 += Math.ceil(rehoVAL * reho * 1.16);
		}
		if ($("#ulgaCHK").val() !== "0"){
			suma += ulga;
			var ulgaVAL = parseFloat($("#ulgaCHK").val());  
			P1 += Math.ceil(ulgaVAL * ulga * 1.16); 
			P2 += Math.ceil(ulgaVAL * ulga * 1.16);
			P3 += Math.ceil(ulgaVAL * ulga * 1.16);
		}

		
		
		row.find("#sumaMulti1").html(formatValue(sa1));
		row.find("#sumaMulti2").html(formatValue(sa2));
		row.find("#sumaMulti3").html(formatValue(sa3));
		
		row.find("#primaMulti1").html(formatValue(P1));
		row.find("#primaMulti2").html(formatValue(P2));
		row.find("#primaMulti3").html(formatValue(P3));

		$("#anual").html(formatValue(total * 12));
		$("#semestral").html(formatValue(total * 6));
		$("#trimestral").html(formatValue(total * 3));
		$("#mensual").html(formatValue(total));
		if (total < 1000){
			$("#mensual").html(formatValue(0));
		}
		
		totalM1 += P1;
		totalM2 += P2;
		totalM3 += P3;
		
		cotizacion.titulares[x] = {};
		
		cotizacion.titulares[x].cantidad = cantidad;
		cotizacion.titulares[x].sueldo = formatValue(sueldo);
		
		cotizacion.titulares[x].M1SA = sa1;
		cotizacion.titulares[x].M2SA = sa2;
		cotizacion.titulares[x].M3SA = sa3;
		
		cotizacion.titulares[x].M1PRIMA = P1;
		cotizacion.titulares[x].M2PRIMA = P2;
		cotizacion.titulares[x].M3PRIMA = P3;
		
		cotizacion.M1 = $("#multi1").val();
		cotizacion.M2 = $("#multi2").val();
		cotizacion.M3 = $("#multi3").val();
		
		cotizacion.total = formatValue(total);
		cotizacion.anual = formatValue(total);
		cotizacion.semestral = formatValue(semestral);
		cotizacion.trimestral = formatValue(trimestral);
		cotizacion.mensual = formatValue(mensual);
		if (total < 1000){
			cotizacion.mensual = formatValue(0);
		}		
		
		cotizacion.cliente = new Object();
		cotizacion.cliente.nombre = $("#cliente").val();
		cotizacion.cliente.telefono = $("#telefono").val();
		cotizacion.cliente.gerente = $("#gerente").val();
		cotizacion.cliente.tipo = $("#tiponegocio").val();
		cotizacion.cliente.intermediario = $("#intermediario").val();
		cotizacion.cliente.email = $("#email").val();
		cotizacion.cliente.bcc = "";
		cotizacion.cliente.edad = $("#edad").val();
		cotizacion.cliente.moneda = $("#moneda").val();
		
		cotizacion.tabla = new Object();
		$(".campoTabla").each(function(){
			var campoTabla = $(this);
			cotizacion.tabla[campoTabla.attr("id")] = campoTabla.html();
		});
		
		cotizacion.tasaVida = tasaVida;
		cotizacion.tasaMad = tasaMad;
		cotizacion.tasaPaci = tasaPaci;
		cotizacion.tasaReho = tasaReho;
		cotizacion.tasaUlga = tasaUlga;
	}
	var totales = $("#totalesSueldo");
	totales.find("#totalMensualSueldoM1").html(formatValue(totalM1));
	totales.find("#totalMensualSueldoM2").html(formatValue(totalM2));
	totales.find("#totalMensualSueldoM3").html(formatValue(totalM3));
	
	totales.find("#totalTrimestralSueldoM1").html(formatValue(totalM1 * 3));
	totales.find("#totalTrimestralSueldoM2").html(formatValue(totalM2 * 3));
	totales.find("#totalTrimestralSueldoM3").html(formatValue(totalM3 * 3));
	
	totales.find("#totalSemestralSueldoM1").html(formatValue(totalM1 * 6));
	totales.find("#totalSemestralSueldoM2").html(formatValue(totalM2 * 6));
	totales.find("#totalSemestralSueldoM3").html(formatValue(totalM3 * 6));
	
	totales.find("#totalAnualSumaM1").html(formatValue(totalM1 * 12));
	totales.find("#totalAnualSumaM2").html(formatValue(totalM2 * 12));
	totales.find("#totalAnualSumaM3").html(formatValue(totalM3 * 12));

	if (totalcant < 5 && totalcant > 0){
		alert("No puede cotizar menos de 5 personas");
		return;
	}

	
	return true;	
}


function deleteRow(){
	var table = $("#detalles");
	table.children("#detailBody").children("tr:last").remove();
	limpiarTarifas();
}

function deleteRowSueldo(){
	var table = $("#detallesSueldo");
	table.children("#detailBody").children("tr:last").remove();
	limpiarTarifas();
}

function addRow(tabla){
	var row = $('<tr></tr>');
	columna = $('<td></td>');
	columna.append('<input type="text" id="cantidad" size="5" />');
	row.append(columna);

	if (tabla == "detalles"){
		columna = $('<td></td>');
		columna.append('<input type="text" id="nombre" size="11" />');
		row.append(columna);
	}

	columna = $('<td class="numeric"></td>');
	columna.append('<input type="text" id="vida" size="8" value=""/>');
	row.append(columna);

	
	columna = $('<td class="numeric"></td>');
	columna.append('<input type="text" id="mad" size="8" />');
	row.append(columna);
	
	columna = $('<td class="numeric"></td>');
	columna.append('<input type="text" id="paci" size="8" />');
	row.append(columna);
	
	columna = $('<td class="numeric"></td>');
	var obj = $('<select id="reho"></select>');
	obj.append('<option value="0">0</option>');

	obj.append('<option value="25"">25</option>');
	obj.append('<option value="50">50</option>');
	obj.append('<option value="75">75</option>');
	obj.append('<option value="100">100</option>');
	
	obj.append('<option value="500" selected="selected">500</option>');
	obj.append('<option value="1000">1,000</option>');
	obj.append('<option value="2500">2,500</option>');
	obj.append('<option value="5000">5,000</option>');
	columna.append(obj);
	row.append(columna);
	

	columna = $('<td class="numeric"></td>');
	var obj = $('<select id="ulga"></select>');
	obj.append('<option value="0">0</option>');

	obj.append('<option value="1000">1,000</option>');
	obj.append('<option value="2000">2,000</option>');
	obj.append('<option value="3000">3,000</option>');
	obj.append('<option value="5000">5,000</option>');
	
	obj.append('<option value="20000">20,000</option>');
	obj.append('<option value="25000">25,000</option>');
	obj.append('<option value="50000" selected="selected">50,000</option>');
	obj.append('<option value="75000">75,000</option>');
	obj.append('<option value="100000">100,000</option>');
	columna.append(obj);
	row.append(columna);
	
	/*
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="suma" class="tarifa"></span>');
	row.append(columna);
	*/
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="tarifaPP" class="tarifaPP"></span>');
	row.append(columna);	
	
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="tarifa" class="tarifa"></span>');
	row.append(columna);

	var table = $("#" + tabla).find("#detailBody");
	table.append(row);
	limpiarTarifas();
}


function addRowSueldo(tabla){
	var row = $('<tr></tr>');
	columna = $('<td></td>');
	columna.append('<input type="text" id="cantidad" size="5" />');
	row.append(columna);

	columna = $('<td></td>');
	columna.append('<input type="text" id="sueldoval" size="11" />');
	row.append(columna);

	columna = $('<td class="numeric"></td>');
	columna.append('<span id="sumaMulti1" size="5" value=""></span>');
	row.append(columna);

	
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="primaMulti1" size="5" value=""></span>');
	row.append(columna);

	
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="sumaMulti2" size="5" value=""></span>');
	row.append(columna);
	
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="primaMulti2" size="5" value=""></span>');
	row.append(columna);
	
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="sumaMulti3" size="5" value=""></span>');
	row.append(columna);

	columna = $('<td class="numeric"></td>');
	columna.append('<span id="primaMulti3" size="5" value=""></span>');
	row.append(columna);	

	var table = $("#detallesSueldo").children("#detailBody");
	table.append(row);
	limpiarTarifas();
}