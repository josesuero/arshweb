if (!String.prototype.startsWith) {
	String.prototype.startsWith = function (stringBuscada, posicion) {
		posicion = posicion || 0;
		return this.indexOf(stringBuscada, posicion) === posicion;
	};
}
if (!String.prototype.includes) {
	String.prototype.includes = function (search, start) {
		'use strict';
		if (typeof start !== 'number') start = 0;
		if (start + search.length > this.length) return false;
		else return this.indexOf(search, start) !== -1;
	};
}
/******************************************************************/
var proxy = true;
var dec = 0;
var dedInternacional = [/*"100", */"1000", "2500", "5000", "10000"];
var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var tarifas = {};//objeto string : array para almacenar los pedidos del servidor
var firstLocalData = true;
var dbRecords = [];

var planes;
var opciones;
var opciones_planes;
var pagos;
var maintable;
var data = {
	cotizador: "internacional",
	deducibles: dedInternacional,
};
$(document).ready(function () {
	addRow();
	addRow();
	for (var i = 0; i < 4; i++) {
		addRow(1);
	}

	planes = dedInternacional.map(function (i) { return { "id": "0", "name": i, "tarifa": "00.00", "mensual": "0.00", "opciones": "0.00", "opcionesDep": "0.00", "sumMatern": "0.00", "descHijo": "0.00" }; });
	opciones = ajaxrequest("opciones_int", 'id:id,name:name,required:required,orden:orden', "&type=int");
	opciones_planes = ajaxrequest("opciones_int_planes", 'id:id,id_option:id_option,name:name,tarifa:tarifa,tarifa_dep:tarifa_dep,orden:orden', "");

	opciones = opciones
		.sort(function (a, b) {
			return (a.orden > b.orden) ? 1 : (a.orden < b.orden) ? -1 :
				(a.name > b.name) ? 1 : (a.name < b.name) ? -1 : 0;
		})
		.map(function (opcion) {
			opcion.name = opcion.name
				.replace("Ultimos", "\xdaltimos");
			return opcion;
		});

	opciones_planes = opciones_planes.sort(function (a, b) { return (a.orden > b.orden) ? 1 : (a.orden < b.orden) ? -1 : a.name > b.name });

	var tbody = $("tbody.opciones");
	var showRD = false;
	for (var x = 0; x < opciones.length; x++) {
		if (opciones[x].name[0] == "*") {
			showRD = true;
		}
		var row = $("<tr></tr>").addClass("calc2");
		// var cell = $("<td colspan='3'></td>").addClass("titulos");
		var cell = $("<td></td>").addClass("titulos");
		if (opciones[x].required == '1') {
			cell.append($("<input type='checkbox' id='" + opciones[x].name + "' checked disabled />").change(calcular));
		} else {
			cell.append($("<input type='checkbox' id='" + opciones[x].name + "' />").change(calcular));
		}
		cell.append($("<label for='" + opciones[x].name + "'></label>").html(opciones[x].name));
		row.append(cell);

		row.append($("<td>&nbsp;</td>"));
		row.append($("<td>&nbsp;</td>"));
		row.append($("<td>&nbsp;</td>"));

		// cell = $("<td colspan='2'></td>");
		cell = $("<td></td>");

		var planesObj = $("<select class='option' style='width:200px'></select>").change(function () {
			var planesObj = $(this);
			var x = $(this).attr("planID");
			$(this).parent().find(".subopcion").children().remove();
			for (var i = 0; i < opciones_planes.length; i++) {
				if (opciones_planes[i].id_option === opciones[x].id) {
					if (opciones_planes[i].name.indexOf("|") === -1) {
						// planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
					} else {
						if (opciones_planes[i].name.split("|")[0] === $($(this).attr("options")[$(this).attr("selectedIndex")]).text()) {
							var name = opciones_planes[i].name.split("|");
							if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
								planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
							}
							var subopcion = $(this).parent().find(".subopcion");
							if (subopcion.length === 0) {
								subopcion = $("<select class='subopcion'></select>").change(calcular);
								$(this).parent().append(subopcion);
							}
							if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
								subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
							}
						}
					}
				}
			}
			calcular();

		});
		planesObj.attr("planID", x);
		cell.append(planesObj);
		for (var i = 0; i < opciones_planes.length; i++) {
			if (opciones_planes[i].id_option === opciones[x].id) {
				if (opciones_planes[i].name.indexOf("|") === -1) {
					planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
				} else {
					var name = opciones_planes[i].name.split("|");
					if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
						planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
					}

					var subopcion = cell.find(".subopcion");
					if (subopcion.length === 0) {
						subopcion = $("<select class='subopcion'></select>").change(calcular);
						cell.append(subopcion);
					}
					if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
						subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
					}
				}
			}
		}
		row.append(cell);
		tbody.append(row);
	}
	if (showRD) {
		$("#NOSHOWRD").hide();
	}
	else {
		$("#SHOWRD").hide();
	}
	tbody = $(".calculable").children("tr");
	for (var i = 0; i < planes.length; i++) {
		var plan = planes[i];
		planName = plan.name.replace(/ /g, "");
		tbody.append($("<td class='tarifa numeric " + planName + "' align='right'></td>")
			.append(
			// $("<span type='text' class='calcular' id='" + plan.name + i + "'></span>").html(0)
			$("<input disabled type='text' class='tarifa calcular' id='" + plan.name + i + "' />").val(0)
			)
		);
	}

	var cuotas = ["anual", "semestral", "trimestral"];
	var rowsCuotas = $("#cuotas").find("tr");
	for (var c = 0; c < cuotas.length; c++) {
		var cuota = cuotas[c];
		var tr = rowsCuotas[c];
		for (var i = 0; i < planes.length; i++) {
			var planName = planes[i].name.replace(/ /g, "");
			var td = $('<td style="width: 90px" class="numeric ' + planName + '"></td>')[0];
			var input = $('<input disabled class="tarifa" type="text" id="' + cuota + planName + '" />')[0];
			$(tr).append(td);
			$(td).append(input);
		};
	}

	$("#tipo").change(tipoChanged).change();
	calcular();
});
function tipoChanged() {
	if ($(this).attr("id") == "tipo") {
		if ($("#tipo").val() == "LA") {
			// if($("#local").attr("selectedIndex")==0){
			// 	$("#local").attr("selectedIndex", 1);
			// }
			//$("#opt0").hide();
			$("#local").parent().hide();
			$(".numeric.100").show();
			$(".numeric.1000").hide();
			$(".numeric.2500").hide();
			$(".numeric.5000").hide();
			$(".numeric.10000").hide();
		} else {
			//$("#opt0").show();
			$("#local").parent().show();
			$(".numeric.100").hide();
			$(".numeric.1000").show();
			$(".numeric.2500").show();
			$(".numeric.5000").show();
			$(".numeric.10000").show();
		}
	}
	calcular();
};
function addCommas(nStr) {
	nStr += "";
	nStr = parseFloat(nStr.replace(/,/g, '')).toFixed(dec);
	var x = nStr.split(".");
	var x1 = x[0];
	var x2 = x.length > 1 ? "." + x[1] : "";
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, "$1" + "," + "$2");
	}
	return x1 + x2;
}
function report(type) {
	if ($("#detalles tr #edad").filter(function (p) { return $(this).val() != ""; }).length == 0) {
		alert("Debe especificar la edad de un prospecto.");
		$("#edad").focus();
		return false;
	}
	if ($("#cliente").val() === "") {
		alert("El campo Cliente no puede estar vac\xedo.");
		$("#cliente").focus();
		return false;
	}

	if ($("#telefono").val() === "") {
		alert("El campo Tel\xe9fono no puede estar vac\xedo.");
		$("#telefono").focus();
		return false;
	}

	if ($("#gerente").val() === "") {
		alert("El campo Gerente no puede estar vac\xedo.");
		$("#gerente").focus();
		return false;
	}
	if ($("#intermediario").val() === "") {
		alert("El campo Intermediario no puede estar vac\xedo.");
		$("#intermediario").focus();
		return false;
	}
	if ($("#email").val() === "") {
		alert("El campo E-Mail no puede estar vac\xedo.");
		$("#email").focus();
		return false;
	} else if (!emailRegex.test($("#email").val())) {
		alert("Debe ingresar un correo v\xe1lido.");
		$("#email").focus();
		return false;
	}


	var url = "";

	url += "?__report=arshint3.rptdesign&cliente=" + encodeURIComponent($("#cliente").val())
		+ "&intermediario=" + encodeURIComponent($("#intermediario").val())
		+ "&telefono=" + encodeURIComponent($("#telefono").val())
		+ "&plan=" + encodeURIComponent($("#tipo").val())
		+ "&international=" + encodeURIComponent($("#international option:selected").text())
		+ "&local=" + encodeURIComponent($("#local option:selected").text());

	var x = 0;
	var rows = $("#detalles").children("tbody").children("tr");
	for (var y = 0; y < rows.length; y++) {
		//var fieldvalue = 0;
		row = $(rows[y]);
		if (row.find("#edad").val() != "") {
			x += 1;
			url += "&codigo" + x + "=" + encodeURIComponent(row.find("#codigo").val())
				+ "&nombre" + x + "=" + encodeURIComponent(row.find("#nombre").val())
				+ "&parentesco" + x + "=" + encodeURIComponent(row.find("#parentesco option:selected").text())
				+ "&edad" + x + "=" + encodeURIComponent(row.find("#edad option:selected").text())
				+ "&sexo" + x + "=" + encodeURIComponent(row.find("#sexo").val())
			//+ "&tarifa" + x + "=" + row.find("#tarifa").html()
			for (var i = 0; i < dedInternacional.length; i++) {
				var dedI = dedInternacional[i];
				url += "&tarifa" + x + dedI + "=" + row.find("#tarifa" + dedI).val();
			};
		}
	}

	/*
	url += "&nombre1=" + $("#nombre1").val() + "&nombre2=" + $("#nombre2").val()+ "&nombre3=" + 
				$("#nombre3").val() + "&nombre4=" + $("#nombre4").val() + "&nombre5=" + $("#nombre5").val()+ "&nombre6=" + $("#nombre6").val()
				+ "&parentesco1=" + $("#parentesco1").val() + "&parentesco2=" + $("#parentesco2").val() + "&parentesco3=" + 
				$("#parentesco3").val() + "&parentesco4=" + $("#parentesco4").val() + "&parentesco5=" + 
				$("#parentesco5").val() + "&parentesco6=" + $("#parentesco6").val() + "&edad1=" +$("#edad1").val() + "&edad2=" +$("#edad2").val()+ "&edad3=" +
				$("#edad3").val() + "&edad4=" +$("#edad4").val()+ "&edad5=" +$("#edad5").val()+ "&edad6=" +$("#edad6").val() + "&sexo1=" +$("#sexo1").val()+ "&sexo2=" +
				$("#sexo2").val()+ "&sexo3=" +$("#sexo3").val() + "&sexo4=" +$("#sexo4").val()+ "&sexo5=" +$("#sexo5").val()+ "&sexo6=" +$("#sexo6").val()+ 
				"&tarifa1=" +$("#tarifa1").html()+ "&tarifa2=" +$("#tarifa2").html()+ "&tarifa3=" +$("#tarifa3").html() + "&tarifa4=" +
				$("#tarifa4").html()+ "&tarifa5=" +$("#tarifa5").html()+"&tarifa6=" +$("#tarifa6").html()
	*/
	url += "&email=" + encodeURIComponent($("#email").val());
	url += "&bcc=";
	url += "&gerente=" + encodeURIComponent($("#gerente").val());
	//url += "&total=" + $("#total").html() 	
	//+	"&semestral=" + $("#semestral").html() + "&trimestral=" + $("#trimestral").html()
	var pagos = ["anual", "semestral", "trimestral"];
	for (var y = 0; y < pagos.length; y++) {
		var pago = pagos[y];
		for (var i = 0; i < dedInternacional.length; i++) {
			var dedI = dedInternacional[i];
			url += "&" + pago + dedI + "=" + $("#" + pago + dedI).val();
		};
	};
	// + "&010001000=" + $("#010001000").html()+
	// "&10001000=" + $("#10001000").html() + "&25001000=" + $("#25001000").html()+ "&50001000=" + $("#50001000").html()
	// + "&100001000=" + $("#100001000").html() + "&150001000=" + $("#150001000").html() + "&010002500=" + $("#010002500").html()+
	// "&10002500=" + $("#10002500").html() + "&25002500=" + $("#25002500").html()+ "&50002500=" + $("#50002500").html()
	// + "&100002500=" + $("#100002500").html() + "&150002500=" + $("#150002500").html() + "&010005000=" + $("#010005000").html()+
	// "&10005000=" + $("#10005000").html() + "&25005000=" + $("#25005000").html()+ "&50005000=" + $("#50005000").html()
	// + "&100005000=" + $("#100005000").html() + "&150005000=" + $("#150005000").html()+ "&0100010000=" + $("#0100010000").html()+
	// "&100010000=" + $("#100010000").html() + "&250010000=" + $("#250010000").html()+ "&500010000=" + $("#500010000").html()
	// + "&1000010000=" + $("#1000010000").html() + "&1500010000=" + $("#1500010000").html();
	// if(["report", "email"].indexOf(type)==-1){
	// 	url += "&data=" + encodeURIComponent(JSON.stringify(data));		
	// }
	url += "&__format=pdf";
	//store quote
	var urlproxy;
	if (proxy == true) {
		urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/store" + encodeURIComponent(url);
	} else {
		urlproxy = "store" + url;
	}
	$.ajax({
		type: "POST",
		url: urlproxy,
		async: false,
		// data: {data: data},
		data: "data=" + encodeURIComponent(JSON.stringify(data)),
		success: function (result, funcname, resultobj) {
			//alert(resultobj.responseText);
		},
		error: function (error) {
			//alert(error);
		}
	});

	if (type === "report") {
		url = "/birt/frameset" + url;
	} else if (type === "email") {
		url = "email" + url;
	}
	if (type === "report") {
		openReport("POST", "http://cotizador.humano.com.do:8080/" + url, { data: data }, "_blank");
		// window.open("http://cotizador.humano.com.do:8080" + url);	
	} else if (type === "email") {
		urlproxy = "";
		if (proxy == true) {
			url = encodeURIComponent(url);
			urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
		} else {
			urlproxy = url;
		}
		$.ajax({
			type: "POST",
			url: urlproxy,
			async: false,
			data: "data=" + JSON.stringify(data),
			success: function (result, funcname, resultobj) {
				console.log(arguments);
				alert("E-Mail enviado!.");
			},
			error: function (error) {
				console.log(arguments);
				alert(error);
			}
		});
	}

}
function cleanTable() {
	$("#010001000").parent().parent().find('td').css("background-color", "#ecfafb").find('span').html(0).css("color", "#00baad");
	$("#010005000").parent().parent().find('td').css("background-color", "#ecfafb").find('span').html(0).css("color", "#00baad");
	$("#010002500").parent().parent().find('td').css("background-color", "white").find('span').html(0).css("color", "#628bab");
	$("#0100010000").parent().parent().find('td').css("background-color", "white").find('span').html(0).css("color", "#628bab");
	/*$("#010001000").html(0).parent().css("background-color","#ecfafb");
	$("#10001000").html(0).parent().css("background-color","#ecfafb");
	$("#25001000").html(0).parent().css("background-color","#ecfafb");
	$("#50001000").html(0).parent().css("background-color","#ecfafb");
	$("#100001000").html(0).parent().css("background-color","white");
	$("#150001000").html(0).parent().css("background-color","white");*/
	/*$("#010002500").html(0).parent().css("background-color","white");
	$("#10002500").html(0).parent().css("background-color","white");
	$("#25002500").html(0).parent().css("background-color","white");
	$("#50002500").html(0).parent().css("background-color","white");
	$("#100002500").html(0).parent().css("background-color","white");
	$("#150002500").html(0).parent().css("background-color","white");*/
	/*$("#010005000").html(0).parent().css("background-color","white");
	$("#10005000").html(0).parent().css("background-color","white");
	$("#25005000").html(0).parent().css("background-color","white");
	$("#50005000").html(0).parent().css("background-color","white");
	$("#100005000").html(0).parent().css("background-color","white");
	$("#150005000").html(0).parent().css("background-color","white");*/
	/*$("#0100010000").html(0).parent().css("background-color","white");
	$("#100010000").html(0).parent().css("background-color","white");
	$("#250010000").html(0).parent().css("background-color","white");
	$("#500010000").html(0).parent().css("background-color","white");
	$("#1000010000").html(0).parent().css("background-color","white");
	$("#1500010000").html(0).parent().css("background-color","white");*/
}

function addValue(key, result) {
	for (var y = 0; y < result.length; y++) {
		var fieldkey = result[y].key.replace(key, "").replace(" - ", "").replace("/", "");
		if ($("#" + fieldkey).length) {
			var value = parseFloat($("#" + fieldkey).html().replace(/,/g, ""));
			$("#" + fieldkey).html(formatValue(value + Math.ceil(parseFloat(result[y].value))));
		}
	}
}
function formatValue(value) {
	return addCommas(value);
}
/*function sumar(){
	var total = 0;
	$(".tarifa").each(function(i, obj){
		var value = $(obj).html();
		if(value != ""){
			total += parseFloat(value);
		};
	})
	
	//.53
	//.2750
}*/

function calcular() {
	var totalafiliados = 0;
	var totalconyuge = 0;
	var totalhijos = 0;
	var rows = $("#detalles").children("tbody").children("tr");

	var total = dedInternacional.reduce(function (acc, cur, i) {
		acc[cur] = 0;
		return acc;
	}, {});
	var selectedPlan = $("#tipo").val();
	data["planes"] = [];
	for (var x = 0; x < rows.length; x++) {
		row = $(rows[x]);
		var calcularRow = false;
		var oPlan = {};
		oPlan.codigo = row.find("#codigo").val();
		oPlan.nombre = row.find("#nombre").val();
		oPlan.valparentesco = row.find("#parentesco option:selected").val()
		oPlan.parentesco = row.find("#parentesco option:selected").text()
		oPlan.valedad = row.find("#edad option:selected").val()
		oPlan.edad = row.find("#edad option:selected").text()
		oPlan.sexo = row.find("#sexo").val();

		var value = 0;
		var key = row.find("#edad").val();
		key += row.find("#sexo").val();
		key += $("#tipo").val();
		key += row.find("#parentesco").val();
		if (row.find("#edad").val() != "") {
			calcularRow = true;
			totalafiliados += 1;
			if (oPlan.parentesco == "Hijo")
				totalhijos += 1;
			if (!!firstLocalData && !!tarifas[key]) {
				value = tarifas[key];
			}
			if (!value.length) {
				value = dbRecords.filter(function (i) {
					return i.key.startsWith(key);
				});
				tarifas[key] = value;
			}
			if (!value.length) {
				var url = "dbserver?table=pricelist_details&field=key:plan_key,value:plan_value&id_list=12&plan_key=" + key;
				//proxy change
				var urlproxy;
				if (proxy === true) {
					url = encodeURIComponent(url);
					urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
				} else {
					urlproxy = url;
				}

				$.ajax({
					type: "GET",
					url: urlproxy,
					async: false,
					success: function (result, funcname, resultobj) {
						value = eval(resultobj.responseText);
					},
					error: function (error) {
						alert(error);
					}
				});
				tarifas[key] = value;
			}
		}
		if (calcularRow) {
			for (var y = 0; y < dedInternacional.length; y++) {
				var international = dedInternacional[y];
				var fieldvalue = 0;
				var code = key + $("#local").val() + " - " + international;
				for (var i = 0; i < value.length; i++) {
					if (value[i].key == code) {
						fieldvalue = Math.ceil(value[i].value);
						total[international] += fieldvalue;

						break;// Confirmar que so'lo una vez cumple la condicio'n por iteracio'n
					}
				}
				oPlan['D' + international] = !!fieldvalue ? formatValue(fieldvalue) : "";
				row.find("#tarifa" + international).val(formatValue(fieldvalue));
			}
			data.planes.push(oPlan);
		}
		else {
			row.find(".tarifa").val(formatValue(0));
		}
	}
	data["opciones"] = [];
	var id_option = 0;
	tbody = $("tbody.opciones").children("tr.calc2");
	if (totalafiliados > 0) {
		tbody.each(function (x) {
			var row = $(this);
			var tarifa = 0;
			var tarifa_dep = 0;
			var cOpcion = {};
			cOpcion.nombre = opciones[x].name;
			var calcularOpcion = row.find("input[type='checkbox']").attr("checked");
			var name = "";
			if (calcularOpcion === true) {
				id_option = opciones[x].id;
				name = row.find(".option").val();
				subopcion = row.find(".subopcion");
				if (subopcion.length > 0) {
					name += " " + subopcion.val();
				}
				cOpcion.plan = name;
				for (var i = 0; i < opciones_planes.length; i++) {
					if (opciones_planes[i].id_option === id_option && opciones_planes[i].name === name) {
						tarifa = parseFloat(opciones_planes[i].tarifa || 0);
						tarifa_dep = parseFloat(opciones_planes[i].tarifa_dep || 0);
						break;
					}
				}
			}
			var temptarifa = tarifa;
			row.find(".calcular").each(function (i) {
				var international = dedInternacional[i];
				var tarifaO = 0,
					TC = 0,
					H = 0;

				for (var p = 0; p < data.planes.length; p++) {
					var afiliado = data.planes[p];
					if (
						(cOpcion.nombre.includes("Vida") &&
							(
								afiliado.valedad > 11 ||
								(selectedPlan == "EX" && cOpcion.plan == "US$10,000.00") ||
								(selectedPlan == "MA" && cOpcion.plan == "US$10,000.00") ||
								(selectedPlan == "PR" && cOpcion.plan != "US$50,000.00")
							)
						) ||
						(cOpcion.nombre.includes("Muerte") && afiliado.valedad > 11) ||
						(cOpcion.nombre.includes("Anticipado") && afiliado.valedad > 9) ||
						(cOpcion.nombre.includes("Gastos") &&
							(
								afiliado.valedad > 11 ||
								(selectedPlan == "PR" && cOpcion.plan != "US$5,000.00")
							)
						) ||
						(cOpcion.nombre.includes("Ambulatoria") &&
							(
								afiliado.valedad > 11 ||
								selectedPlan == "PR"
							)
						) ||
						(cOpcion.nombre.includes("Odontolog") && selectedPlan == "PR")
					) {
						if (afiliado.valparentesco == "H")
							H += 0;
						else
							TC += 0;
					}
					else {
						if (afiliado.valparentesco == "H")
							H += tarifa_dep;
						else
							TC += tarifa;
					}
				}
				tarifaO = TC + H;

				total[international] += tarifaO;
				cOpcion['D' + international] = !!tarifaO ? formatValue(tarifaO) : "";
				$(this).val(formatValue(tarifaO));
			});
			if (calcularOpcion) {
				data.opciones.push(cOpcion);
			}
		});
	}
	if (data.opciones.length == 0) {
		data.opciones.push({ "nombre": "Ninguno seleccionado." });
	} else if (data.opciones.filter(function (i) { return i.nombre[0] == "*"; }).length > 0) {
		data.opciones.push({ "nombre": "*S\xf3lo aplica en Rep\xfablica Dominicana." });
	}
	data.total = total;
	data.totalafiliados = totalafiliados;
	for (var y = 0; y < dedInternacional.length; y++) {
		var international = dedInternacional[y];
		var semestral = total[international] * 0.53;
		var trimestral = total[international] * 0.2750;
		$("#anual" + international).val(formatValue(total[international]));
		$("#semestral" + international).val(formatValue(semestral));
		$("#trimestral" + international).val(formatValue(trimestral));
	}
}
function deleteRow() {
	var rows = $("#detalles").children("tbody").children("tr");
	if (rows.length > 1) {
		$(rows[rows.length - 1]).remove();
		calcular();
	}
}
function addRow(hijo) {
	//if($("#detalles").children("tbody").children("tr").length > 9) return;
	if (hijo == undefined) {
		hijo = "";
	} else {
		hijo = "selected";
	}
	var row = $('<tr></tr>');
	// columna = $('<td></td>');
	// columna.append('<input type="text" id="codigo" size="11">');
	// row.append(columna);


	columna = $('<td></td>');
	columna.append('<input type="text" class="fldnombre" id="nombre">');
	row.append(columna);

	columna = $('<td style="width:135px; background: #ecfafb;"></td>');
	var obj = $('<select id="parentesco"><option value="TC" >Titular/Conyuge</option><option value="H" ' + hijo + '>Hijo</option></select>');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);

	columna = $('<td style="width:60px; background: #ecfafb;"></td>');
	obj = $('<select id="edad"><option value=""></option><option value="1">0-18</option><option value="2">19-24</option><option value="3">25-29</option><option value="4">30-34</option><option value="5">35-39</option><option value="6">40-44</option><option value="7">45-49</option><option value="8">50-54</option><option value="9">55-59</option><option value="10">60-64</option><option value="11">65-69</option></select>');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);

	columna = $('<td style="width:60px; background: #ecfafb;"></td>');
	obj = $('<select id="sexo"><option value="F">F</option><option value="M">M</option></select></td');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);

	// columna = $('<td class="numeric"></td>');
	// columna.append('<span id="tarifa" class="tarifa"></span>');
	// row.append(columna);

	for (var i = 0; i < dedInternacional.length; i++) {
		columna = $('<td class="numeric ' + dedInternacional[i] + '" style="width: 90px"></td>');
		columna.append('<input type="text" disabled id="tarifa' + dedInternacional[i] + '" class="tarifa" />');
		row.append(columna);
	}

	var table = $("#detalles");
	table.append(row);
	calcular();
}
function ajaxrequest(table, fields, filter) {
	var url = "dbserver?table=" + table + "&field=" + fields + filter;

	var urlproxy;
	if (proxy === true) {
		url = encodeURIComponent(url);
		urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
	} else {
		urlproxy = url;
	}
	var value = false;
	$.ajax({
		type: "GET",
		url: urlproxy,
		async: false,
		success: function (result, funcname, resultobj) {
			value = eval(resultobj.responseText);
		},
		error: function (error) {
			alert(error);
			value = false;
		}
	});
	return value;
}
function openReport(verb, url, data, target) {
	var form = document.createElement("form");
	form.action = url;
	form.acceptCharset = "UTF-8";
	form.method = verb;
	form.target = target || "_self";
	if (data) {
		for (var key in data) {
			var input = document.createElement("textarea");
			input.name = key;
			input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
			form.appendChild(input);
		}
	}
	form.style.display = 'none';
	document.body.appendChild(form);
	form.submit();
};