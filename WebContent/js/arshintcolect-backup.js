var proxy = true;
var dec = 0;

var cantPlanes = {};

var current;

var cotizacion = {};
$(document).ready(function(){
	addRow();
	addRow();
	for(var i = 0; i<4;i++){
		addRow(1);
	}
	$("#dependientesDiv").dialog({width:650, height:350,modal:true, show:'fade', hide:'fade',autoOpen:false, buttons:{Guardar:saveDependiente, Cancelar:function(){current=null;$("#dependientesDiv").dialog("close");}}});
});
function addCommas(nStr){
	nStr += "";
	nStr = parseFloat(nStr.replace(/,/g,'')).toFixed(dec);
	var x = nStr.split(".");
	var x1 = x[0];
	var x2 = x.length > 1 ? "." + x[1]: "";
	var rgx= /(\d+)(\d{3})/;
	while(rgx.test(x1)){
		x1 = x1.replace(rgx, "$1" + "," + "$2");
	}
	return x1+x2;
}

function sendMail(){
	calcular();
	var url = "emailcolect";
	var urlproxy;
	var data;
	if (proxy == true){
		//url = encodeURIComponent(url + "?data=" + JSON.stringify(cotizacion));
		url = url + "?data=" + JSON.stringify(cotizacion);
		urlproxy = "proxy.php";
		data = {url:"http://172.24.206.227:8080/" + url, mode:'native', REQUEST_METHOD:"POST"};
		//mode=native&url=http://172.24.206.227:8080/"+url
	} else {
		urlproxy = url;
		data = {data:encodeURIComponent(JSON.stringify(cotizacion))};
	}
	$.ajax({type: "POST",
		url:urlproxy,
		async:false,
		data:data,
		success:function(result,funcname, resultobj){
			alert(resultobj.responseText);
		},
		error:function(error){
			alert(error.responseText);
		}
	});		
	
	
} 

function print(){
	calcular();
	var url = "";
	url += "/birt/frameset";
	url += "?__report=arshcolect.rptdesign&data="+JSON.stringify(cotizacion); 
	url += "&__format=pdf";
	window.open("http://cotizador.arshumano.com:8080" + url);
	
}

function report(type){
	if ($("#email").val() === ""){
		alert("El campo E-Mail no puede estar vacio");
		$("#email").focus();
		return false;
	}
	
	if ($("#cliente").val() === ""){
		alert("El campo Cliente no puede estar vacio");
		$("#cliente").focus();
		return false;
	}
	
	if ($("#telefono").val() === ""){
		alert("El campo Tel�fono no puede estar vacio");
		$("#telefono").focus();
		return false;
	}
	
	if ($("#gerente").val() === ""){
		alert("El campo Gerente no puede estar vacio");
		$("#gerente").focus();
		return false;
	}
	if ($("#intermediario").val() === ""){
		alert("El campo Intermediario no puede estar vacio");
		$("#intermediario").focus();
		return false;
	}
	
	if (type === "report"){
		print();
	} else if (type === "email"){
		sendMail();
	}
}
function cleanTable(){
	$(".tarifa").html(0);
	$(".campoTabla").html(0).parent().css("background-color","white");
}

function addValue(key, result){
	for(var y = 0; y < result.length;y++){
		if (result[y].key.indexOf(key) != -1){
			var fieldkey = result[y].key.replace(key,"").replace(" - ","").replace("/","");
			
			var value = $("#" + fieldkey).html().replace(/,/g,"");
			if (value == ""){
				value = 0;
			}
			value = parseFloat(value);
			$("#" + fieldkey).html(formatValue(value + Math.ceil(parseFloat(result[y].value))));
		}
	}
}
function formatValue(value){
	return addCommas(value);
}
/*function sumar(){
	var total = 0;
	$(".tarifa").each(function(i, obj){
		var value = $(obj).html();
		if(value != ""){
			total += parseFloat(value);
		};
	})
	
	//.53
	//.2750
}*/
function limpiarTarifas(){
	cleanTable();
}

function calcular(){
	cotizacion = {titulares:[]};
	
	var value = 0;
	var rows = $("#detalles").children("tbody").children("tr");
	cleanTable();
	var total = 0;
	cantPlanes = {};
	
	for(var x = 0; x < rows.length; x++){
		row =  $(rows[x]);
		var edad =row.find("#edad").val();
		if (edad != ""){
			var tipo =row.find("#tipo").val();
			var cantidad = row.find("#cantidad").val();
			if (cantidad == ""){
				cantidad = 1;
			} else {
				cantidad = parseFloat(cantidad);
			}
	
			if (cantPlanes[tipo]){
				cantPlanes[tipo] += cantidad;
			} else {
				cantPlanes[tipo] = cantidad;
			}
		}
	}
	
	for(var x = 0; x < rows.length; x++){
		var fieldvalue = 0;

		row =  $(rows[x]);
		//var row = $(this).parent().parent();
		if(row.find("#edad").val()!=""){
			var key = row.find("#edad").val();
			
			key+= row.find("#sexo").val();
			
			var tipo = row.find("#tipo").val();
			if (cantPlanes[tipo] <= 10){
				tipo += "SG";
			} else if (cantPlanes[tipo] <=24){
				tipo += "MG";
			} else {
				tipo += "LG";
			}
			
			key+= tipo;
			//key+= row.find("#parentesco").val();
			//key+= "TC";
			var url = "dbserver?table=pricelist_details&field=key:plan_key,value:plan_value&id_list=13&plan_group=" + tipo;
			//proxy change
			var urlproxy;
			if (proxy === true){
				url = encodeURIComponent(url);
				urlproxy = "proxy.php?mode=native&url=http://172.24.206.227:8080/"+url;
			}  else {
				urlproxy = url;
			}
			
			$.ajax({type: "GET",
				url:urlproxy,
				async:false,
				success:function(result,funcname, resultobj){
					value = eval(resultobj.responseText);
				},
				error:function(error){
					alert(error);
				}
			});
			//key = row.find("#edad").val() + key.replace(/%/g,'');
			
			var plankey = key;
			//TITULAR
			key+= "TC";
			addValue(key, value);
			
			key+= row.find("#local").val();
			key+= " - ";
			key+= row.find("#international").val();
			
			var cantidad = row.find("#cantidad").val();
			if (cantidad == ""){
				cantidad = 1;
			} else {
				cantidad = parseFloat(cantidad);
			}

			for (var i =0; i < value.length;i++){
				if(value[i].key == key){
					fieldvalue = Math.ceil(value[i].value) * cantidad;
				}
			}
			
			cotizacion.titulares[x] = {};
			cotizacion.titulares[x].codigo = row.find("#codigo").val();
			cotizacion.titulares[x].nombre = row.find("#nombre").val();
			cotizacion.titulares[x].edad = row.find("#edad").val();
			cotizacion.titulares[x].sexo = row.find("#sexo").val();
			cotizacion.titulares[x].plan = tipo;
			cotizacion.titulares[x].local = row.find("#local").val();
			cotizacion.titulares[x].international = row.find("#international").val();
			cotizacion.titulares[x].cantidad = row.find("#cantidad").val();
			cotizacion.titulares[x].tarifa = fieldvalue;
			
			//DEPENDIENTES
			var dependientes = row.find("#btndependientes").attr("dependientes");
			
			dependientes = eval("(" + dependientes  + ")");
			for (var i = 0; i < dependientes.length;i++){
				var depkey = dependientes[i].edad;
				depkey += dependientes[i].sexo;
				depkey += tipo;
				depkey += dependientes[i].parentesco;
				addValue(depkey, value);
				
				depkey+= row.find("#local").val();
				depkey+= " - ";
				depkey+= row.find("#international").val();
				tarifaDep = 0;
				for (var y =0; y < value.length;y++){
					if(value[y].key == depkey){
						fieldvalue += Math.ceil(value[y].value);
						tarifaDep = value[y].value;
					}
				}
				dependientes[i].tarifa = Math.ceil(tarifaDep);
			}
			total+=fieldvalue;
			
			cotizacion.titulares[x].dependientes = dependientes;
			row.find("#btndependientes").attr("dependientes", JSON.stringify(dependientes));
		}
		var semestral = total * 0.53;
		var trimestral = total * 0.2750;
		var mensual = total * 0.095;
		$("#total").html(formatValue(total));
		$("#anual").html(formatValue(total));
		$("#semestral").html(formatValue(semestral));
		$("#trimestral").html(formatValue(trimestral));
		$("#mensual").html(formatValue(mensual));
		row.find("#tarifa").html(formatValue(fieldvalue));
		$("#" + $("#local").val().replace("/","") + $("#international").val()).parent().css("background-color","lightgrey");
		
		cotizacion.total = formatValue(total);
		cotizacion.anual = formatValue(total);
		cotizacion.semestral = formatValue(semestral);
		cotizacion.trimestral = formatValue(trimestral);
		cotizacion.mensual = formatValue(mensual);
		
		cotizacion.cliente = {};
		cotizacion.cliente.nombre = $("#cliente").val();
		cotizacion.cliente.telefono = $("#telefono").val();
		cotizacion.cliente.gerente = $("#gerente").val();
		cotizacion.cliente.intermediario = $("#intermediario").val();
		cotizacion.cliente.email = $("#email").val();
		cotizacion.cliente.bcc = "";
		
		
		cotizacion.tabla = {};
		$(".campoTabla").each(function(){
			var campoTabla = $(this);
			cotizacion.tabla[campoTabla.attr("id")] = campoTabla.html();
		});
		
	}
	//sumar();	
}
function deleteRow(){
	var table = $("#detalles");
	table.children("tbody").children("tr:last").remove();
	limpiarTarifas();
}

function deleteRowDependiente(){
	var table = $("#dependientesTBL");
	table.children("tbody").children("tr:last").remove();
	limpiarTarifas();
}
function addRow(hijo){
	if (hijo == undefined){
		hijo = "";
	} else {
		hijo = "selected";
	}
	var row = $('<tr></tr>');
	columna = $('<td></td>');
	columna.append('<input type="text" id="codigo" size="11">');
	row.append(columna);

	
	columna = $('<td></td>');
	columna.append('<input type="text" id="nombre">');
	row.append(columna);
/*
	columna = $('<td></td>');
	var obj = $('<select id="parentesco"><option value="TC" >Titular/Conyugue</option><option value="H" ' + hijo + '>Hijo</option></select>');
	obj.change(limpiarTarifas);
	columna.append(obj);
	row.append(columna);
*/
	columna = $('<td></td>');
	obj= $('<select id="edad"><option value=""></option><option value="1">0-18</option><option value="2">19-24</option><option value="3">25-29</option><option value="4">30-34</option><option value="5">35-39</option><option value="6">40-44</option><option value="7">45-49</option><option value="8">50-54</option><option value="9">55-59</option><option value="10">60-64</option></select>');
	obj.change(limpiarTarifas);
	columna.append(obj);
	row.append(columna);

	columna = $('<td></td>');
	obj= $('<select id="sexo"><option value="F">F</option><option value="M">M</option></select>');
	obj.change(limpiarTarifas);
	columna.append(obj);
	row.append(columna);
	
	columna = $('<td></td>');
	obj= $('<select onchange="limpiarTarifas()" id="tipo"><option value="PR" selected="selected">Premium</option><option value="EX">Executive</option><option value="CL">Classic</option></select>');
	columna.append(obj);
	row.append(columna);
	
	columna = $('<td></td>');
	obj= $('<select onchange="limpiarTarifas()" id="international"><option value="1000">1,000</option><option value="2500">2,500</option><option value="5000">5,000</option><option value="10000">10,000</option></select>');
	columna.append(obj);
	row.append(columna);
	
	columna = $('<td></td>');
	obj= $('<select onchange="limpiarTarifas()" id="local"><option value="0/1000">0/1,000</option><option value="1000">1,000</option><option value="2500">2,500</option><option value="5000">5,000</option><option value="10000">10,000</option><option value="15000">15,000</option></select>');
	columna.append(obj);
	row.append(columna);
	
	columna = $('<td></td>');
	obj= $('<input type="text" id="cantidad" size="4"/>');
	
	columna.append(obj);
	row.append(columna);	

	columna = $('<td></td>');
	obj= $('<button id="btndependientes" dependientes="[]">Dependientes(0)</button>');
	obj.click(openDependiente);
	columna.append(obj);
	row.append(columna);

	
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="tarifa" class="tarifa"></span>');
	row.append(columna);

	var table = $("#detalles");
	table.append(row);
	limpiarTarifas();
}

function addRowDependiente(hijo){
	if (hijo == undefined){
		hijo = "";
	} else {
		hijo = "selected";
	}
	var row = $('<tr></tr>');
	columna = $('<td></td>');
	columna.append('<input type="text" id="codigo" size="11" class="control">');
	row.append(columna);

	
	columna = $('<td></td>');
	columna.append('<input type="text" id="nombre" class="control">');
	row.append(columna);

	columna = $('<td></td>');
	var obj = $('<select id="parentesco" class="control"><option value="TC">Conyugue</option><option value="H" ' + hijo + '>Hijo</option></select>');
	//obj.change(limpiarTarifas);
	columna.append(obj);
	row.append(columna);
	
	columna = $('<td></td>');
	obj= $('<select id="edad" class="control"><option value=""></option><option value="1">0-18</option><option value="2">19-24</option><option value="3">25-29</option><option value="4">30-34</option><option value="5">35-39</option><option value="6">40-44</option><option value="7">45-49</option><option value="8">50-54</option><option value="9">55-59</option><option value="10">60-64</option></select>');
	//obj.change(limpiarTarifas);
	columna.append(obj);
	row.append(columna);

	columna = $('<td></td>');
	obj= $('<select id="sexo" class="control"><option value="F">F</option><option value="M">M</option></select>');
	//obj.change(limpiarTarifas);
	columna.append(obj);
	row.append(columna);
	/*	
	columna = $('<td></td>');
	obj= $('<select onchange="limpiarTarifas()" id="tipo"><option value="PR" selected="selected">Premium</option><option value="EX">Executive</option><option value="CL">Classic</option></select>');
	columna.append(obj);
	row.append(columna);
	

	columna = $('<td></td>');
	obj= $('<select onchange="limpiarTarifas()" id="international"><option value="1000">1,000</option><option value="2500">2,500</option><option value="5000">5,000</option><option value="10000">10,000</option></select>');
	columna.append(obj);
	row.append(columna);
	
	columna = $('<td></td>');
	obj= $('<select onchange="limpiarTarifas()" id="local"><option value="0/1000">0/1,000</option><option value="1000">1,000</option><option value="2500">2,500</option><option value="5000">5,000</option><option value="10000">10,000</option><option value="15000">15,000</option></select>');
	columna.append(obj);
	row.append(columna);
	
	columna = $('<td></td>');
	obj= $('<input type="text" id="edad" size="4"/>');
	columna.append(obj);
	row.append(columna);	

	columna = $('<td></td>');
	obj= $('<button>Dependientes</button>');
	columna.append(obj);
	row.append(columna);

*/	
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="tarifa" class="tarifa controlspan"></span>');
	row.append(columna);

	var table = $("#dependientesTBL");
	table.append(row);
	//limpiarTarifas();
}

function openDependiente(){
	current = $(this);
	var dependiente = current.attr("dependientes");
	if (dependiente == undefined){
		dependiente = []; 
	} else {
		dependiente = eval(dependiente);
	}
	var table = $("#dependientesTBL");
	table.children("tbody").children().remove();
	
	for (var i = 0; i < dependiente.length;i++){
		addRowDependiente();
		var row = table.children("tbody").children("tr:last"); 
		
		row.find(".control").each(function(){
			$(this).val(dependiente[i][$(this).attr("id")]);
		});
		row.find(".controlspan").each(function(){
			$(this).html(dependiente[i][$(this).attr("id")]);
		});		
	}
	
	if (dependiente.length == 0){
		addRowDependiente();
	}
	
	$("#dependientesDiv").dialog("open");	
}

function saveDependiente(){
	//get data
	var data = [];
	
	var table = $("#dependientesTBL");
	var total = 0;
	
	table.children("tbody").children().each(function(){
		if ($(this).find("#edad").val() !== ""){
			var dep = {};
			$(this).find(".control").each(function(){
				dep[$(this).attr("id")] = $(this).val();
			});
			$(this).find(".controlspan").each(function(){
				dep[$(this).attr("id")] = $(this).html();
			});
			var tarifa = $(this).find("#tarifa").html();
			if (tarifa !== ""){
				total += parseFloat(tarifa);
			}
			data[data.length] = dep;
		}
	});
	current.attr("tarifa", total);
	current.attr("dependientes", JSON.stringify(data));
	current.html("Dependientes(" + data.length + ")");
	$("#dependientesDiv").dialog("close");
}
