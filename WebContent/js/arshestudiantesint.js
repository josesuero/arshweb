if (!String.prototype.startsWith) {
	String.prototype.startsWith = function (stringBuscada, posicion) {
		posicion = posicion || 0;
		return this.indexOf(stringBuscada, posicion) === posicion;
	};
}
if (!String.prototype.includes) {
	String.prototype.includes = function (search, start) {
		'use strict';
		if (typeof start !== 'number') start = 0;
		if (start + search.length > this.length) return false;
		else return this.indexOf(search, start) !== -1;
	};
}
/******************************************************************/
var proxy = true;
var dec = 0;
var deducibles = ["0"];
var cuotas = ["unico", "anual", "semestral", "trimestral"];
var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var tarifas = {};
var firstLocalData = true;
var dbRecords = [];
var planes;
var opciones;
var opciones_planes;
var pagos;
var maintable;
var data = {
	cotizador: "estudiantesint",
	deducibles: deducibles,
};
$(document).ready(function () {
	addRow();

	for (var i = 0; i < meses.length; i++) {
		var mes = meses[i];
		$("#desdeMes").append("<option value='" + (i + 1) + "'>" + mes + "</option>");
		$("#hastaMes").append("<option value='" + (i + 1) + "'>" + mes + "</option>");
		// $("#desdeAnio").attr("min", (new Date()).getFullYear());
		// $("#hastaAnio").attr("min", (new Date()).getFullYear());
	}
	var anio = (new Date()).getFullYear();
	for (var i = 0; i < 10; i++) {
		var option = anio + i;
		$("#desdeAnio").append("<option value='" + (option) + "'>" + option + "</option>");
		$("#hastaAnio").append("<option value='" + (option) + "'>" + option + "</option>");
	}
	var today = new Date();
	cambiarFechas(new Date(today.getFullYear(), today.getMonth() + 1, 1));
	fechasChanged();

	planes = deducibles.map(function (i) { return { "id": "0", "name": i, "tarifa": (i * 1), "mensual": (i * 1) / 12, "opciones": "0.00", "opcionesDep": "0.00", "sumMatern": "0.00", "descHijo": "0.00" }; });

	opciones = ajaxrequest("opciones_int", 'id:id,name:name,required:required,orden:orden', "&type=ei");
	opciones_planes = ajaxrequest("opciones_int_planes", 'id:id,id_option:id_option,name:name,tarifa:tarifa,tarifa_dep:tarifa_dep,orden:orden', "");
	// opciones = [{ id: '17', name: 'Enfermedades', required: '0', orden: 100 }];
	// opciones_planes = [/*{ id: '118', id_option: '18', name: 'US$7,000.00', tarifa: '112.86', tarifa_dep: '112.86', orden: 10 },*/{ id: '118', id_option: '18', name: 'US$7,000.00', tarifa: '1354.37', tarifa_dep: '1354.37', orden: 10 },];

	opciones = opciones.sort(function (a, b) { return (a.orden > b.orden) ? 1 : (a.orden < b.orden) ? -1 : a.name > b.name });
	opciones_planes = opciones_planes.sort(function (a, b) { return (a.orden > b.orden) ? 1 : (a.orden < b.orden) ? -1 : a.name > b.name });
	var tbody = $("tbody.opciones");
	var showRD = false;
	for (var x = 0; x < opciones.length; x++) {
		if (opciones[x].name[0] == "*") {
			showRD = true;
		}
		var row = $("<tr></tr>").addClass("calc2");
		// var cell = $("<td colspan='3'></td>").addClass("titulos");
		var cell = $("<td></td>").addClass("titulos");
		if (opciones[x].required == '1') {
			cell.append($("<input type='checkbox' id='" + opciones[x].name + "' checked disabled />").change(calcular));
		} else {
			cell.append($("<input type='checkbox' id='" + opciones[x].name + "' />").change(calcular));
		}
		cell.append($("<label for='" + opciones[x].name + "'></label>").html(opciones[x].name));
		row.append(cell);

		row.append($("<td>&nbsp;</td>"));
		row.append($("<td>&nbsp;</td>"));
		row.append($("<td>&nbsp;</td>"));

		// cell = $("<td colspan='2'></td>");
		cell = $("<td></td>");

		var planesObj = $("<select class='option' style='width:200px'></select>").change(function () {
			var planesObj = $(this);
			var x = $(this).attr("planID");
			$(this).parent().find(".subopcion").children().remove();
			for (var i = 0; i < opciones_planes.length; i++) {
				if (opciones_planes[i].id_option === opciones[x].id) {
					if (opciones_planes[i].name.indexOf("|") === -1) {
						// planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
					} else {
						if (opciones_planes[i].name.split("|")[0] === $($(this).attr("options")[$(this).attr("selectedIndex")]).text()) {
							var name = opciones_planes[i].name.split("|");
							if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
								planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
							}
							var subopcion = $(this).parent().find(".subopcion");
							if (subopcion.length === 0) {
								subopcion = $("<select class='subopcion'></select>").change(calcular);
								$(this).parent().append(subopcion);
							}
							if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
								subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
							}
						}
					}
				}
			}
			calcular();

		});
		planesObj.attr("planID", x);
		cell.append(planesObj);
		for (var i = 0; i < opciones_planes.length; i++) {
			if (opciones_planes[i].id_option === opciones[x].id) {
				if (opciones_planes[i].name.indexOf("|") === -1) {
					planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
				} else {
					var name = opciones_planes[i].name.split("|");
					if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
						planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
					}

					var subopcion = cell.find(".subopcion");
					if (subopcion.length === 0) {
						subopcion = $("<select class='subopcion'></select>").change(calcular);
						cell.append(subopcion);
					}
					if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
						subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
					}
				}
			}
		}
		row.append(cell);
		tbody.append(row);
	}
	if (showRD) {
		$("#NOSHOWRD").hide();
	}
	else {
		$("#SHOWRD").hide();
	}
	tbody = $(".calculable").children("tr");
	for (var i = 0; i < planes.length; i++) {
		var plan = planes[i];
		planName = plan.name.replace(/ /g, "");
		tbody.append($("<td class='tarifa numeric " + planName + "' align='right'></td>")
			.append(
				// $("<span type='text' class='calcular' id='" + plan.name + i + "'></span>").html(0)
				$("<input disabled type='text' class='tarifa calcular' id='" + plan.name + i + "' />").val(0)
			)
		);
	}

	var rowsCuotas = $("#cuotas").find("tr");
	for (var c = 0; c < cuotas.length; c++) {
		var cuota = cuotas[c];
		var tr = rowsCuotas[c];
		for (var i = 0; i < planes.length; i++) {
			var planName = planes[i].name.replace(/ /g, "");
			var td = $('<td style="width: 90px" class="numeric ' + planName + '"></td>')[0];
			var input = $('<input disabled class="tarifa" type="text" id="' + cuota + planName + '" />')[0];
			$(tr).append(td);
			$(td).append(input);
		};
	}

	calcular();
});
function fechasChanged() {
	var datesDiff = monthDiff();
	if (datesDiff.now.getMonth() == datesDiff.desde.getMonth()) {
		if (datesDiff.now.getDate() >= 16) {
			cambiarFechas(new Date(datesDiff.now.getFullYear(), datesDiff.now.getMonth() + 1, 1));
			alert("Este mes ya no aplica para cotizaci\xf3n.");
		}
	}
	else if (!(datesDiff.desde > datesDiff.now)) {
		cambiarFechas(new Date(datesDiff.now.getFullYear(), datesDiff.now.getMonth() + 1, 1));
		alert("La fecha Desde debe ser mayor a la actual.");
	}
	else if (datesDiff.months < 2) {
		cambiarFechas(datesDiff.desde);
		alert("Seleccione 3 meses como m\xednimo.");
	}
	calcular();
	var datesDiff = monthDiff();
	$("#desdeDia").html(datesDiff.desde.getDate());
	$("#hastaDia").html(datesDiff.hasta.getDate());
	data.daterange = dateToText(datesDiff.desde) + " - " + dateToText(datesDiff.hasta);
}
function dateToText(date) {
	return date.getDate() + " de " + $($("#desdeMes option")[date.getMonth()]).text() + " " + date.getFullYear();
}
function cambiarFechas(date) {
	var newDesde = new Date(date.setMonth(date.getMonth()));
	$("#desdeMes").val(newDesde.getMonth() + 1);
	$("#desdeAnio").val(newDesde.getFullYear());
	var datesDiff = monthDiff();
	if (datesDiff.months < 2) {
		var newHasta = new Date(date.setMonth(date.getMonth() + 2));
		$("#hastaMes").val(newHasta.getMonth() + 1);
		$("#hastaAnio").val(newHasta.getFullYear());
	}
}
function monthDiff() {
	var result = {
		now: new Date(),
		desde: new Date($("#desdeAnio").val(), $("#desdeMes").val() - 1, 1),
		hasta: new Date($("#hastaAnio").val(), $("#hastaMes").val(), 0),
		months: 0,
	};
	var months = (result.hasta.getFullYear() - result.desde.getFullYear()) * 12;
	months -= result.desde.getMonth(); // + 1;
	months += result.hasta.getMonth();
	result.months = months;
	return result;
}
function addCommas(nStr) {
	nStr += "";
	nStr = parseFloat(nStr.replace(/,/g, '')).toFixed(dec);
	var x = nStr.split(".");
	var x1 = x[0];
	var x2 = x.length > 1 ? "." + x[1] : "";
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, "$1" + "," + "$2");
	}
	return x1 + x2;
}

function formatValue(value) {
	return '$' + addCommas(value);
}

function report(type) {
	if ($("#detalles tr #edad").filter(function (p) { return $(this).val() != ""; }).length == 0) {
		alert("Debe especificar la edad de un prospecto.");
		$("#edad").focus();
		return false;
	}
	if ($("#cliente").val() === "") {
		alert("El campo Cliente no puede estar vac\xedo.");
		$("#cliente").focus();
		return false;
	}
	if ($("#telefono").val() === "") {
		alert("El campo Tel\xe9fono no puede estar vac\xedo.");
		$("#telefono").focus();
		return false;
	}
	if ($("#gerente").val() === "") {
		alert("El campo Gerente no puede estar vac\xedo.");
		$("#gerente").focus();
		return false;
	}
	if ($("#intermediario").val() === "") {
		alert("El campo Intermediario no puede estar vac\xedo.");
		$("#intermediario").focus();
		return false;
	}
	if ($("#email").val() === "") {
		alert("El campo E-Mail no puede estar vac\xedo.");
		$("#email").focus();
		return false;
	} else if (!emailRegex.test($("#email").val())) {
		alert("Debe ingresar un correo v\xe1lido.");
		$("#email").focus();
		return false;
	}
	if ($("#universidad").val() === "") {
		alert("El campo Universidad no puede estar vac\xedo.");
		$("#universidad").focus();
		return false;
	}

	var url = "";
	url += "?__report=arshestudiantesint.rptdesign&cliente=" + encodeURIComponent($("#cliente").val())
		+ "&intermediario=" + encodeURIComponent($("#intermediario").val())
		+ "&telefono=" + encodeURIComponent($("#telefono").val())
		+ "&plan=EI"
		+ "&international=" + encodeURIComponent($("#international option:selected").text())
		+ "&local=";

	var x = 0;
	var rows = $("#detalles").children("tbody").children("tr");
	for (var y = 0; y < rows.length; y++) {
		row = $(rows[y]);
		if (row.find("#edad").val() != "") {
			x += 1;
			url += "&codigo" + x + "=" + encodeURIComponent(row.find("#codigo").val())
				+ "&nombre" + x + "=" + encodeURIComponent(row.find("#nombre").val())
				+ "&parentesco" + x + "=" + encodeURIComponent(row.find("#parentesco option:selected").text())
				+ "&edad" + x + "=" + encodeURIComponent(row.find("#edad option:selected").text())
				+ "&sexo" + x + "=" + encodeURIComponent(row.find("#sexo").val())
			for (var i = 0; i < deducibles.length; i++) {
				var dedI = deducibles[i];
				url += "&tarifa" + x + dedI + "=" + row.find("#tarifa" + dedI).val();
			};
		}
	}

	url += "&email=" + encodeURIComponent($("#email").val());
	url += "&universidad=" + encodeURIComponent($("#universidad").val());
	url += "&bcc=";
	url += "&gerente=" + encodeURIComponent($("#gerente").val());
	for (var y = 0; y < cuotas.length; y++) {
		var cuota = cuotas[y];
		for (var i = 0; i < deducibles.length; i++) {
			var ded = deducibles[i];
			url += "&" + cuota + ded + "=" + ($("#" + cuota + ded).val() || "0");
		};
	};
	if (["report", "email"].indexOf(type) == -1) {
		url += "&data=" + encodeURIComponent(JSON.stringify(data));
	}
	url += "&__format=pdf";
	//store quote
	var urlproxy;
	if (proxy == true) {
		urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/store" + encodeURIComponent(url);
	} else {
		urlproxy = "store" + url;
	}
	$.ajax({
		type: "POST",
		url: urlproxy,
		async: false,
		// data: { data: data },
		data: "data=" + encodeURIComponent(JSON.stringify(data)),
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		success: function (result, funcname, resultobj) {
			//alert(resultobj.responseText);
		},
		error: function (error) {
			//alert(error);
		}
	});
	if (type === "report") {
		openReport("POST", "http://cotizador.humano.com.do:8080/birt/frameset" + url, { data: data }, "_blank");
	} else if (type === "email") {
		url = "/emailestudiantesint" + url;
		urlproxy = "";
		if (proxy == true) {
			url = encodeURIComponent(url);
			urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080" + url;
		} else {
			urlproxy = url;
		}
		$.ajax({
			type: "POST",
			url: urlproxy,
			async: false,
			data: "data=" + encodeURIComponent(JSON.stringify(data)),
			contentType: "application/x-www-form-urlencoded; charset=utf-8",
			success: function (result, funcname, resultobj) {
				console.log(arguments);
				alert("E-Mail enviado!.");
			},
			error: function (error) {
				console.log(arguments);
				alert(error);
			}
		});
	}
}

function calcular() {
	var totalafiliados = 0;
	var totalconyuge = 0;
	var totalhijos = 0;
	var rows = $("#detalles").children("tbody").children("tr");
	var meses = monthDiff().months + 1;
	data.showUnico = true;
	data.showAnual = meses != 12 && meses % 12 == 0;
	data.showSemestral = meses != 6 && meses % 6 == 0;
	data.showTrimestral = meses != 3 && meses % 3 == 0;
	if (data.showAnual) {
		$("#rowAnual").show();
	} else {
		$("#rowAnual").hide();
	}
	if (data.showSemestral) {
		$("#rowSemestral").show();
	} else {
		$("#rowSemestral").hide();
	}
	if (data.showTrimestral) {
		$("#rowTrimestral").show();
	} else {
		$("#rowTrimestral").hide();
	}

	// var total = deducibles.reduce(function (acc, cur, i) {
	// 	acc[cur] = 0;
	// 	return acc;
	// }, {});
	var totalAnualizado = deducibles.reduce(function (acc, cur, i) {
		acc[cur] = 0;
		return acc;
	}, {});
	var opcionalAnualizado = deducibles.reduce(function (acc, cur, i) {
		acc[cur] = 0;
		return acc;
	}, {});
	var selectedPlan = $("#modalidad").val();
	data.modalidad = selectedPlan;
	data.modalidadText = "Modalidad " + $("#modalidad option:selected").text();
	data.planes = [];
	for (var x = 0; x < rows.length; x++) {
		row = $(rows[x]);
		var calcularRow = false;
		var oPlan = {};
		oPlan.codigo = row.find("#codigo").val();
		oPlan.nombre = row.find("#nombre").val();
		oPlan.valparentesco = row.find("#parentesco option:selected").val()
		oPlan.parentesco = row.find("#parentesco option:selected").text()
		oPlan.valedad = row.find("#edad option:selected").val()
		oPlan.edad = row.find("#edad option:selected").text()
		oPlan.sexo = row.find("#sexo").val();

		var value = 0;
		var key = row.find("#edad").val();
		key += row.find("#sexo").val();
		key += "EI";
		key += "1"; //Región dominicana por defecto
		key += row.find("#parentesco").val();
		key += $("#modalidad").val();
		if (row.find("#edad").val() != "") {
			calcularRow = true;
			totalafiliados += 1;
			if (oPlan.parentesco == "Hijo")
				totalhijos += 1;
			if (!!firstLocalData && !!tarifas[key]) {
				value = tarifas[key];
			}
			if (!value.length) {
				value = dbRecords.filter(function (i) {
					return i.key.startsWith(key);
				});
				tarifas[key] = value;
			}
			if (!value.length) {
				var url = "dbserver?table=pricelist_details&field=key:plan_key,value:plan_value&id_list=17&plan_key=" + key;
				//proxy change
				var urlproxy;
				if (proxy === true) {
					url = encodeURIComponent(url);
					urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
				} else {
					urlproxy = url;
				}
				$.ajax({
					type: "GET",
					url: urlproxy,
					async: false,
					success: function (result, funcname, resultobj) {
						value = eval(resultobj.responseText);
					},
					error: function (error) {
						alert(error);
					}
				});
				tarifas[key] = value;
			}
		}
		if (calcularRow) {
			for (var y = 0; y < deducibles.length; y++) {
				var deducible = deducibles[y];
				var fieldValue = 0;
				var code = key + " - " + deducible;
				for (var i = 0; i < value.length; i++) {
					if (value[i].key == code) {
						// fieldvalue = Math.ceil(value[i].value * meses);
						var prima = value[i].value * 1;
						fieldValue = Math.ceil((prima / 12) * meses);
						// total[deducible] += fieldvalue;
						totalAnualizado[deducible] += Math.ceil(prima);
						break;
					}
				}
				oPlan['D' + deducible] = !!fieldValue ? formatValue(fieldValue) : "";
				row.find("#tarifa" + deducible).val(formatValue(fieldValue));
			}
			data.planes.push(oPlan);
		}
		else {
			row.find(".tarifa").val(formatValue(0));
		}
	}
	data["opciones"] = [];
	var id_option = 0;
	tbody = $("tbody.opciones").children("tr.calc2");
	tbody.each(function (x) {
		var row = $(this);
		var prima = 0;
		var tarifa = 0;
		var tarifa_dep = 0;
		var cOpcion = {};
		cOpcion.nombre = opciones[x].name;
		var calcularOpcion = row.find("input[type='checkbox']").attr("checked");
		var name = "";
		if (calcularOpcion === true) {
			id_option = opciones[x].id;
			name = row.find(".option").val();
			subopcion = row.find(".subopcion");
			if (subopcion.length > 0) {
				name += " " + subopcion.val();
			}
			cOpcion.plan = name;
			for (var i = 0; i < opciones_planes.length; i++) {
				if (opciones_planes[i].id_option === id_option && opciones_planes[i].name === name) {
					prima = parseFloat(opciones_planes[i].tarifa || 0);
					break;
				}
			}
		}
		row.find(".calcular").each(function (i) {
			var deducible = deducibles[i];
			var incluido = false;
			var notAvail = false;
			var tarifaO = 0;
			if (calcularOpcion === true) {
				for (var p = 0; p < data.planes.length; p++) {
					if (cOpcion.nombre.includes("Maternidad") && meses < 12) {
						notAvail = 1;
						continue;
					}
					tarifaO += Math.ceil((prima / 12) * meses);
				}

				if (calcularOpcion && notAvail) {
					tarifaO = "N/A";
				}
				else if (calcularOpcion && incluido) {
					tarifaO = "incluido";
				} else if (!incluido && !notAvail) {
					// total[deducible] += Math.ceil(tarifaO);
					opcionalAnualizado[deducible] += prima;
					tarifaO = formatValue(tarifaO);
				}
			}
			cOpcion['D' + deducible] = tarifaO;
			$(this).val(tarifaO);
		});
		if (calcularOpcion === true)
			data.opciones.push(cOpcion);
	});
	if (data.opciones.length == 0) {
		data.opciones.push({ "nombre": "Ninguno seleccionado." });
	} else if (data.opciones.filter(function (i) { return i.nombre[0] == "*"; }).length > 0) {
		data.opciones.push({ "nombre": "*S\xf3lo aplica en Rep\xfablica Dominicana." });
	}
	data.total = deducibles.reduce(function (acc, cur, i) {
		acc[cur] = totalAnualizado[cur] + opcionalAnualizado[cur];
		return acc;
	}, {});
	data.totalafiliados = totalafiliados;
	for (var y = 0; y < deducibles.length; y++) {
		var deducible = deducibles[y];

		var anual = 0;
		if (data.showAnual) {
			anual = totalAnualizado[deducible] + opcionalAnualizado[deducible];
		};

		var semestral = 0;
		if (data.showSemestral) {
			semestral = Math.ceil(totalAnualizado[deducible] * 0.53)
				+ Math.ceil(opcionalAnualizado[deducible] * 0.53);
		};

		var trimestral = 0;
		if (data.showTrimestral) {
			trimestral = Math.ceil(totalAnualizado[deducible] * 0.275)
				+ Math.ceil(opcionalAnualizado[deducible] * 0.275);
		};

		var unico = Math.ceil((totalAnualizado[deducible] / 12) * meses)
			+ Math.ceil((opcionalAnualizado[deducible] / 12) * meses);

		$("#unico" + deducible).val(formatValue(unico));
		$("#anual" + deducible).val(formatValue(anual));
		$("#semestral" + deducible).val(formatValue(semestral));
		$("#trimestral" + deducible).val(formatValue(trimestral));
	}
}
function deleteRow() {
	var rows = $("#detalles").children("tbody").children("tr");
	if (rows.length > 1) {
		$(rows[rows.length - 1]).remove();
		calcular();
	}
}
function addRow() {
	//if($("#detalles").children("tbody").children("tr").length > 9) return;
	var row = $('<tr></tr>');

	columna = $('<td></td>');
	columna.append('<input type="text" class="fldnombre" id="nombre" onchange="calcular();">');
	row.append(columna);

	columna = $('<td style="width:135px; background: #ecfafb;"></td>');
	var obj = $('<select id="parentesco"><option value="TC" selected>Titular</option></select>');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);

	columna = $('<td style="width:60px; background: #ecfafb;"></td>');
	obj = $(
		'<select id="edad">'
		+ '<option value=""></option>'
		// + '<option value="1">0-18</option>'
		+ '<option value="2">16-24</option>'
		+ '<option value="3">25-29</option>'
		+ '<option value="4">30-34</option>'
		+ '<option value="5">35-39</option>'
		+ '<option value="6">40-44</option>'
		+ '<option value="7">45-49</option>'
		// + '<option value="8">50-54</option>'
		// + '<option value="9">55-59</option>'
		// + '<option value="10">60-64</option>'
		+ '</select>');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);

	columna = $('<td style="width:60px; background: #ecfafb;"></td>');
	obj = $('<select id="sexo"><option value="F">F</option><option value="M">M</option></select></td');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);

	// columna = $('<td class="numeric"></td>');
	// columna.append('<span id="tarifa" class="tarifa"></span>');
	// row.append(columna);

	for (var i = 0; i < deducibles.length; i++) {
		columna = $('<td class="numeric ' + deducibles[i] + '" style="width: 90px"></td>');
		columna.append('<input type="text" disabled id="tarifa' + deducibles[i] + '" class="tarifa" />');
		row.append(columna);
	}
	var table = $("#detalles");
	table.append(row);
	calcular();
}

function ajaxrequest(table, fields, filter) {
	var url = "dbserver?table=" + table + "&field=" + fields + filter;
	var urlproxy;
	if (proxy === true) {
		url = encodeURIComponent(url);
		urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
	} else {
		urlproxy = url;
	}
	var value = false;
	$.ajax({
		type: "GET",
		url: urlproxy,
		async: false,
		success: function (result, funcname, resultobj) {
			value = eval(resultobj.responseText);
		},
		error: function (error) {
			alert(error);
			value = false;
		}
	});
	return value;
}

function openReport(verb, url, data, target) {
	var form = document.createElement("form");
	form.action = url;
	form.acceptCharset = "UTF-8";
	form.method = verb;
	form.target = target || "_self";
	if (data) {
		for (var key in data) {
			var input = document.createElement("textarea");
			input.name = key;
			input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
			form.appendChild(input);
		}
	}
	form.style.display = 'none';
	document.body.appendChild(form);
	form.submit();
};
