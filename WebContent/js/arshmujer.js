var hostserver = location.hostname;
if(hostserver=="localhost")
	var proxy = false;
else 
	var proxy = true;
var servidor = "http://" + hostserver + ":8080";
var cotizacion = {};
var dec = 0;
function ajaxrequest(table, fields, filter) {
    var url = "dbserver?table=" + table + "&field=" + fields + filter;

    var urlproxy;
    if (proxy === true) {
        url = encodeURIComponent(url);
        urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
    } else {
        urlproxy = url;
    }
    var value = false;
    $.ajax({ type: "GET",
        url: urlproxy,
        async: false,
        success: function (result, funcname, resultobj) {
            value = eval(resultobj.responseText);
        },
        error: function (error) {
            alert(error);
            value = false;
        }
    });
    return value;
}
function sendemail(Data) {
    var url = "emailmujer?data="+encodeURIComponent(Data);

    if (proxy === true) {
        url = encodeURIComponent(url);
        urlproxy = "/proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
    } else {
        urlproxy = url;
    }
    var value = false;
    $.ajax({ method: "POST",
        url: urlproxy,
        //data: {data: Data},
        dataType:"jsonp",
        async: false
    })
    .success( function (result, funcname, resultobj) {
        value = eval(resultobj.responseText);
    })
    .error( function (error) {
       value = error.responseText;
    });
    return value;
}
var planes;
var pagos;
var maintable;
$(document).ready(function () {
    planes = [{ id: '1', name: 'Protecci\xf3n Mujer', tarifa: '900.00', mensual: '75.00', opciones: '0.00', opcionesDep: '0.00' },];
    // pagos = ajaxrequest("pagos", 'id:id,name:name,descuento:descuento,recargo:recargo,divisor:divisor', "&showInCancer=1");
    pagos = [{ id: '1', name: 'Anual', descuento: '0.0000', recargo: '0.0000', divisor: '1' }, { id: '2', name: 'Semestral', descuento: '0.0000', recargo: '0.0000', divisor: '2' }];
    
    $("#titular").change(calcular);
    // $("#conyugue").change(calcular);
    $("#H").change(calcular);
    
    $("#showAll").click(function(){
    	for (var i = 0; i < planes.length; i++) {
    		planes[i].visible = true;
    		$("." + planes[i].name.replace(/ /g, "")).show();
    	}
    });

    maintable = $("#maintable");
    var thead = maintable.children("thead").children("#planes");
    for (var i = 0; i < planes.length; i++) {
        var plan = planes[i];
        planName = plan.name.replace(/ /g, "");
        planes[i].visible = true;
        var button = $("<button id='" + planName + "' disabled></button>").text(plan.name)
        thead.append($("<td class='titulo2 " + planName + "' style='width:200px'></td>").append(button));
    }
    
    tbody = $(".calculable").children("tr");
    for (var i = 0; i < planes.length; i++) {
        var plan = planes[i];
        planName = plan.name.replace(/ /g, "");
        tbody.append($("<td class='tarifa "+ planName +"' align='right'></td>").append($("<span type='text' class='calcular' id='" + plan.name + i + "'></span>").html(0)));
    }

    for (var i = 0; i < pagos.length; i++) {
        $("#opcionPagos").change(calcular).append("<option value='" + pagos[i].id + "'>" + pagos[i].name + "</option>");
    }
    
    $(".tituloSecion").each(function(){
    	var $this = $(this);
    	$this.find(".calcular").html("");
    });

    $("tr [id='Laboratorios']").find(".tarifa").each(function() { var $this = $(this); $this.find(".calcular").text("80% (2 por mes)"); });
    $("tr [id='Estudios Especiales']").find(".tarifa").each(function() { var $this = $(this); $this.find(".calcular").text("80% (3 por a\xf1o)"); });
    $("tr [id='Procedimientos']").find(".tarifa").each(function() { var $this = $(this); $this.find(".calcular").text("100% (ilimitado)"); });
    $("tr [id='Medicamentos Oncologicos']").find(".tarifa").each(function() { var $this = $(this); $this.find(".calcular").text("80% (ilimitado)"); });
    $("tr [id='Medicamentos Coadyuvantes']").find(".tarifa").each(function() { var $this = $(this); $this.find(".calcular").text("80% (ilimitado)"); });
    $("tr [id='Cirugia Reconstructiva']").find(".tarifa").each(function() { var $this = $(this); $this.find(".calcular").text("100% (ilimitado)"); });
    $("tr [id='Limite por caso']").find(".tarifa").each(function() { var $this = $(this); $this.find(".calcular").text("RD$500,000"); });
    
    calcular();
});

function round5(n){
	return Math.ceil(n/5)*5;
}

function calcular(e) {
    cotizacion = { planes: [], opciones: [], cliente: {}, totalAfiliados: 0, totalPlanes: {}, subTotal: {}, descuento: {}, total: {}, cuota: {}, cuotaText: {}, formaPago:"" , bcc:"",subject:"", planDef:[]};
    
    cotizacion.planDef = planes;
    var factores = { O: 0, T: 1, TM: 1, T65: 1, T75: 1, H: 1 };
    for (var i = 0; i < planes.length; i++) {
        planes[i].total = 0;
    }
    var totalafiliados = 0;
    totales = [];
    
    var tbody = maintable.children(".control").children("tr");    
    tbody.each(function () {
        var objectPlanes = {};
        var row = $(this);
        var control = row.find(".control");
        tipotarifa = "H";
        cantidad = 1;
        if (control.attr("id") !== "H"){
            tipotarifa = control.val();
        } else {
            cantidad = control.val();
            if (cantidad === "") {
                cantidad = 0;
            }
            cantidad = parseFloat(cantidad);
        }
        if (tipotarifa === "O") {
            cantidad = 0;
        }
        objectPlanes.nombre = tipotarifa;
        row.find(".calcular").each(function (i) {

            var plan = planes[i];
            var cargo = plan.tarifa;

            if ($("#opcionPagos").attr("selectedIndex") === 3) {
                cargo = (plan.tarifa * 1.00);
            }
            var tarifa = round5(cargo * factores[tipotarifa]) * cantidad;
            if (totales.length < (i + 1)) {
                totales[i] = 0;
            }

            tarifa = Math.ceil(tarifa);
            if (parseFloat(tarifa) !== 0){
	            if (control.attr("id")== 'titular' && control.val() != "T65" && control.val() != "T75"){
	            	tarifa += parseFloat(plan.opciones);
	            } else {
	            	tarifa += parseFloat(plan.opcionesDep);
	            }
            }
            totales[i] += tarifa;
            plan.total += tarifa;
            $(this).html(tarifa);
            // objectPlanes[plan.name] = tarifa;
            objectPlanes["mujer"] = tarifa;
        });
        cotizacion.planes[cotizacion.planes.length] = objectPlanes;    
        totalafiliados += cantidad;

    });
    
    tbody = $("tbody.totales").children("tr");
    tbody.each(function () {
        var row = $(this);       
        row.find(".calcular").each(function (i) {
            $(this).html(planes[i].total);
            // cotizacion.totalPlanes[planes[i].name] = planes[i].total;
            cotizacion.totalPlanes["mujer"] = planes[i].total;
        });
        
    });

    $("#totalAfiliados").html(totalafiliados);
    cotizacion.totalAfiliados = totalafiliados;

    var descuento = [];
    tbody = $("tbody.Descuento").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            var opcionPag = $("#opcionPagos").attr("selectedIndex");
            //var descuentoTarifa = parseFloat(planes[i].total) * ((parseFloat(pagos[opcionPag].descuento) / 100));
            var descuentoTarifa = parseFloat(totales[i]) * ((parseFloat(pagos[opcionPag].descuento) / 100));
            descuento[i] = Math.round(descuentoTarifa, 0);

            $("#descuento").html(Math.round(parseFloat(pagos[opcionPag].descuento), 0));
            $(this).html(descuento[i]);
            // cotizacion.descuento[planes[i].name] = descuento[i];
            cotizacion.descuento["mujer"] = descuento[i];
            var opcionPagoletra = "";
            switch (opcionPag) {
                case 0:
                    opcionPagoletra = "Anual";
                    break;
                case 1:
                    opcionPagoletra = "Semestral";
                    break;
                case 2:
                    opcionPagoletra = "Trimestral";
                    break;
                case 3:
                    opcionPagoletra = "Mensual";
                    break;
            }
            cotizacion.formaPago = opcionPagoletra;
        });
    });

    tbody = $("tbody.total").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            $(this).html(totales[i] - descuento[i]);
            cotizacion.total["mujer"] = totales[i] - descuento[i] ;
            // cotizacion.total[planes[i].name] = totales[i] - descuento[i] ;
        });
    });

    tbody = $("tbody.Cuota").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            //var total = Math.round(planes[i].total * ((parseFloat(pagos[$("#opcionPagos").attr("selectedIndex")].descuento) / 100)));
            var cuota = Math.round((totales[i] - descuento[i]) / parseFloat(pagos[$("#opcionPagos").attr("selectedIndex")].divisor),0);
            //var cuota = Math.round((planes[i].total - total) / parseFloat(pagos[$("#opcionPagos").attr("selectedIndex")].divisor),0);
            $(this).html(cuota);
            cotizacion.cuota["mujer"] = cuota;
            cotizacion.planDef[i].primaSeleccionada = addCommas(cuota);
            // cotizacion.cuota[planes[i].name] = cuota;
        });
    });
    
    tbody = $("#tabla2").find(".cliente");
    tbody.each(function(){
        cotizacion.cliente[this.id] = $(this).val();
    });
    $(".calcular").prettynumber({
        delimiter: ','
    });
}
function addCommas(nStr){
	nStr += "";
	nStr = parseFloat(nStr.replace(/,/g,'')).toFixed(dec);
	var x = nStr.split(".");
	var x1 = x[0];
	var x2 = x.length > 1 ? "." + x[1]: "";
	var rgx= /(\d+)(\d{3})/;
	while(rgx.test(x1)){
		x1 = x1.replace(rgx, "$1" + "," + "$2");
	}
	return x1+x2;
}
function sendMail(){
	calcular();
	$("#printData").val(JSON.stringify(cotizacion));
    $("#printForm").attr("target","emailFrame");
    //$("#printForm").submit();
    //setTimeout('alert($("#emailFrame").contents())',500)
    ///Modificacion hecha por CManzu..
	cotizacion.bcc = $('#txtMailR').val();
    if($('#txtMailC').val()!==''){
       //$.post(servidor + "/emaillocal",{data: JSON.stringify(cotizacion)},function(e){
    	var e = sendemail(JSON.stringify(cotizacion));
    	/*if(e=="E-Mail Enviado!")
    	   alert(e);
    	else
    		alert("No se envi\xf3 el correo!.\n Ha ocurrido un problema. Compruebe que los e-mails indicados son v\xe1lidos");
    		$("#txtMailC").focus();*/
		setTimeout('alert("Email enviado")',500);
    }
    else
    	alert("El campo Mail para cliente es requerido para el env\xedo.");
} 
function print(){	
	calcular();
	$("#printForm").attr("action", servidor + "/birt/frameset");
	$("#printForm").attr("target","_new");
	$("#printData").val((JSON.stringify(cotizacion)));
	$("#printForm").submit();	
}
