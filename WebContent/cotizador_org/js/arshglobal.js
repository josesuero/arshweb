var proxy = true;
var dec = 0;
$(document).ready(function(){
		addRow();
		addRow();
	for(var i = 0; i<4;i++){
		addRow(1);
	}
});
function addCommas(nStr){
	nStr += "";
	nStr = parseFloat(nStr.replace(/,/g,'')).toFixed(dec);
	var x = nStr.split(".");
	var x1 = x[0];
	var x2 = x.length > 1 ? "." + x[1]: "";
	var rgx= /(\d+)(\d{3})/;
	while(rgx.test(x1)){
		x1 = x1.replace(rgx, "$1" + "," + "$2");
	}
	return x1+x2;
}
function report(type){
	if ($("#email").val() === ""){
		alert("El campo E-Mail no puede estar vacio");
		$("#email").focus();
		return false;
	}
	
	if ($("#cliente").val() === ""){
		alert("El campo Cliente no puede estar vacio");
		$("#cliente").focus();
		return false;
	}
	
	if ($("#telefono").val() === ""){
		alert("El campo Tel�fono no puede estar vacio");
		$("#telefono").focus();
		return false;
	}
	
	if ($("#gerente").val() === ""){
		alert("El campo Gerente no puede estar vacio");
		$("#gerente").focus();
		return false;
	}
	if ($("#intermediario").val() === ""){
		alert("El campo Intermediario no puede estar vacio");
		$("#intermediario").focus();
		return false;
	}
	
	var url = "";

	url += "?__report=arshint.rptdesign&cliente=" + $("#cliente").val() + "&intermediario=" + $("#intermediario").val()
				+ "&telefono=" + $("#telefono").val() + "&plan=" + $("#tipo").val() + "&international=" + $("#international option:selected").text()+
				"&local=" + $("#local option:selected").text();
				
	var rows = $("#detalles").children("tbody").children("tr");
	for(var y = 0; y < rows.length; y++){
		var fieldvalue = 0;
		row =  $(rows[y]);
		var x = y + 1;
		if (row.find("#edad").val() != ""){
			url += "&codigo" + x + "=" + row.find("#codigo").val() +  "&nombre" + x +"=" + row.find("#nombre").val() + "&parentesco" + x + "=" + row.find("#parentesco option:selected").text() + "&edad" + x +  "=" + row.find("#edad option:selected").text() + "&sexo" + x +  "=" + row.find("#sexo").val() + "&tarifa" + x + "=" + row.find("#tarifa").html();
		}
	}	
	
/*
	url += "&nombre1=" + $("#nombre1").val() + "&nombre2=" + $("#nombre2").val()+ "&nombre3=" + 
				$("#nombre3").val() + "&nombre4=" + $("#nombre4").val() + "&nombre5=" + $("#nombre5").val()+ "&nombre6=" + $("#nombre6").val()
				+ "&parentesco1=" + $("#parentesco1").val() + "&parentesco2=" + $("#parentesco2").val() + "&parentesco3=" + 
				$("#parentesco3").val() + "&parentesco4=" + $("#parentesco4").val() + "&parentesco5=" + 
				$("#parentesco5").val() + "&parentesco6=" + $("#parentesco6").val() + "&edad1=" +$("#edad1").val() + "&edad2=" +$("#edad2").val()+ "&edad3=" +
				$("#edad3").val() + "&edad4=" +$("#edad4").val()+ "&edad5=" +$("#edad5").val()+ "&edad6=" +$("#edad6").val() + "&sexo1=" +$("#sexo1").val()+ "&sexo2=" +
				$("#sexo2").val()+ "&sexo3=" +$("#sexo3").val() + "&sexo4=" +$("#sexo4").val()+ "&sexo5=" +$("#sexo5").val()+ "&sexo6=" +$("#sexo6").val()+ 
				"&tarifa1=" +$("#tarifa1").html()+ "&tarifa2=" +$("#tarifa2").html()+ "&tarifa3=" +$("#tarifa3").html() + "&tarifa4=" +
				$("#tarifa4").html()+ "&tarifa5=" +$("#tarifa5").html()+"&tarifa6=" +$("#tarifa6").html()
*/				
	url += "&email=" + $("#email").val();
	url += "&bcc=";
	url += "&gerente=" + $("#gerente").val(); 
	url += "&total=" + $("#total").html() +	"&semestral=" + $("#semestral").html() + "&trimestral=" + $("#trimestral").html()+ "&010001000=" + $("#010001000").html()+
				"&10001000=" + $("#10001000").html() + "&25001000=" + $("#25001000").html()+ "&50001000=" + $("#50001000").html()
				+ "&100001000=" + $("#100001000").html() + "&150001000=" + $("#150001000").html() + "&010002500=" + $("#010002500").html()+
				"&10002500=" + $("#10002500").html() + "&25002500=" + $("#25002500").html()+ "&50002500=" + $("#50002500").html()
				+ "&100002500=" + $("#100002500").html() + "&150002500=" + $("#150002500").html() + "&010005000=" + $("#010005000").html()+
				"&10005000=" + $("#10005000").html() + "&25005000=" + $("#25005000").html()+ "&50005000=" + $("#50005000").html()
				+ "&100005000=" + $("#100005000").html() + "&150005000=" + $("#150005000").html()+ "&0100010000=" + $("#0100010000").html()+
				"&100010000=" + $("#100010000").html() + "&250010000=" + $("#250010000").html()+ "&500010000=" + $("#500010000").html()
				+ "&1000010000=" + $("#1000010000").html() + "&1500010000=" + $("#1500010000").html();
	url += "&__format=pdf";
	

	//store quote
	var urlproxy;
	if (proxy == true){
		urlproxy = "proxy.php?mode=native&url=http://172.24.206.227:8080/store"+encodeURIComponent(url);
	} else {
		urlproxy = "store" + url;
	}
	$.ajax({type: "GET",
		url:urlproxy,
		async:false,
		success:function(result,funcname, resultobj){
			//alert(resultobj.responseText);
		},
		error:function(error){
			//alert(error);
		}
	});	
	
	if (type === "report"){
		url = "/birt/frameset" + url;
	} else if (type === "email"){
		url = "/email" + url;
	}
	if (type === "report"){
		window.open("http://cotizador.arshumano.com:8080" + url);	
	} else if (type === "email"){
		urlproxy = "";
		if (proxy == true){
			url = encodeURIComponent(url);
			urlproxy = "proxy.php?mode=native&url=http://172.24.206.227:8080/"+url;
		} else {
			urlproxy = url;
		}
		$.ajax({type: "GET",
			url:urlproxy,
			async:false,
			success:function(result,funcname, resultobj){
				alert(resultobj.responseText);
			},
			error:function(error){
				alert(error);
			}
		});
	}
	
}
function cleanTable(){
	$("#010001000").html(0).parent().css("background-color","white");
	$("#10001000").html(0).parent().css("background-color","white");
	$("#25001000").html(0).parent().css("background-color","white");
	$("#50001000").html(0).parent().css("background-color","white");
	$("#100001000").html(0).parent().css("background-color","white");
	$("#150001000").html(0).parent().css("background-color","white");
	$("#010002500").html(0).parent().css("background-color","white");
	$("#10002500").html(0).parent().css("background-color","white");
	$("#25002500").html(0).parent().css("background-color","white");
	$("#50002500").html(0).parent().css("background-color","white");
	$("#100002500").html(0).parent().css("background-color","white");
	$("#150002500").html(0).parent().css("background-color","white");
	$("#010005000").html(0).parent().css("background-color","white");
	$("#10005000").html(0).parent().css("background-color","white");
	$("#25005000").html(0).parent().css("background-color","white");
	$("#50005000").html(0).parent().css("background-color","white");
	$("#100005000").html(0).parent().css("background-color","white");
	$("#150005000").html(0).parent().css("background-color","white");
	$("#0100010000").html(0).parent().css("background-color","white");
	$("#100010000").html(0).parent().css("background-color","white");
	$("#250010000").html(0).parent().css("background-color","white");
	$("#500010000").html(0).parent().css("background-color","white");
	$("#1000010000").html(0).parent().css("background-color","white");
	$("#1500010000").html(0).parent().css("background-color","white");
}

function addValue(key, result){
	for(var y = 0; y < result.length;y++){
		var fieldkey = result[y].key.replace(key,"").replace(" - ","").replace("/","");
		var value = parseFloat($("#" + fieldkey).html().replace(/,/g,""));
		$("#" + fieldkey).html(formatValue(value + Math.ceil(parseFloat(result[y].value))));
	}
}
function formatValue(value){
	return addCommas(value);
}
/*function sumar(){
	var total = 0;
	$(".tarifa").each(function(i, obj){
		var value = $(obj).html();
		if(value != ""){
			total += parseFloat(value);
		};
	})
	
	//.53
	//.2750
}*/

function calcular(){
	var value;
	var rows = $("#detalles").children("tbody").children("tr");
	cleanTable();
	var total = 0;
	for(var x = 0; x < rows.length; x++){
		var fieldvalue = 0;

		row =  $(rows[x]);
		//var row = $(this).parent().parent();
		if(row.find("#edad").val()!=""){
			var key = row.find("#edad").val();
			//key+= row.find("#sexo").val();
			//key+= $("#tipo").val();
			//key+= row.find("#parentesco").val();
			var url = "dbserver?table=pricelist_details&field=key:plan_key,value:plan_value&id_list=15&plan_key=" + key;
			//proxy change
			var urlproxy;
			if (proxy === true){
				url = encodeURIComponent(url);
				urlproxy = "proxy.php?mode=native&url=http://172.24.206.227:8080/"+url;
			}  else {
				urlproxy = url;
			}
			
			$.ajax({type: "GET",
				url:urlproxy,
				async:false,
				success:function(result,funcname, resultobj){
					value = eval(resultobj.responseText);
				},
				error:function(error){
					alert(error);
				}
			});
			fieldvalue = Math.ceil(value[0].value) * 12; 
			total += fieldvalue;
			/*
			addValue(key, value);
			key+= $("#local").val();
			key+= " - ";
			key+= $("#international").val();
			
			for (var i =0; i < value.length;i++){
				if(value[i].key == key){
					fieldvalue = Math.ceil(value[i].value);
					total+=fieldvalue;
				}
			}
			*/
		}
		var semestral = total / 2;
		var trimestral = total /4;
		$("#total").html(formatValue(total));
		$("#anual").html(formatValue(total));
		$("#semestral").html(formatValue(semestral));
		$("#trimestral").html(formatValue(trimestral));
		row.find("#tarifa").html(formatValue(fieldvalue));
		$("#" + $("#local").val().replace("/","") + $("#international").val()).parent().css("background-color","lightgrey");
	}
	//sumar();	
}
function deleteRow(){
	var table = $("#detalles");
	table.children("tbody").children("tr:last").remove();
	calcular();
}
function addRow(hijo){
	if (hijo == undefined){
		hijo = "";
	} else {
		hijo = "selected";
	}
	var row = $('<tr></tr>');
	columna = $('<td></td>');
	columna.append('<input type="text" id="codigo" size="11">');
	row.append(columna);

	
	columna = $('<td></td>');
	columna.append('<input type="text" class="fldnombre" id="nombre">');
	row.append(columna);

	columna = $('<td></td>');
	var obj = $('<select id="parentesco"><option value="TC" >Titular/Conyugue</option><option value="H" ' + hijo + '>Hijo</option></select>');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);

	columna = $('<td></td>');
	obj= $('<select id="edad"><option value=""></option><option value="1">0-19</option><option value="2">20-24</option><option value="3">25-29</option><option value="4">30-34</option><option value="5">35-39</option><option value="6">40-44</option><option value="7">45-49</option><option value="8">50-54</option><option value="9">55-59</option><option value="10">60-64</option><option value="11">65</option></select>');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);

	columna = $('<td></td>');
	obj= $('<select id="sexo"><option value="F">F</option><option value="M">M</option></select></td');
	obj.change(calcular);
	columna.append(obj);
	row.append(columna);	
	
	columna = $('<td class="numeric"></td>');
	columna.append('<span id="tarifa" class="tarifa"></span>');
	row.append(columna);

	var table = $("#detalles");
	table.append(row);
	calcular();
}

$(document).ready(function(){
	calcular();
});
