﻿var to;
var timeout = 600000;
var editingControl = false;
$(window).bind('beforeunload', function () {
    if (editingControl) {
        return 'Si sale de esta pagina perdera la informacion que haya modificado';
    }
});

function TimedOut() {
  $.post( "refresh_session.aspx", null,
    function(data) { 
      if(data == "success") {
          to = setTimeout("TimedOut()", timeout);
      }
      else { $("#timeout").slideDown('fast'); }
    }
  );
}

function ajaxrequest(command, rqdata,rqdataType){
    var rtn;
    var dataType = "json";
    if (rqdataType){
        dataType = rqdataType;
    }
    if (typeof rqdata === "object"){
        rqdata = JSON.stringify(rqdata);
    }

    var data = "requesttype=" + command + "&data=" + encodeURIComponent(rqdata) + "&rnd=" + Math.floor(Math.random() * 1000);
    $.ajax({
        type: "POST",
        url: "ajaxserver",
        dataType:dataType,
        async: false,
        data: data,
        success: function(msg){
            rtn = msg;
        },
        error:function(error){
            alert(error.responseText);
            rtn = false;
        }
    });
    return rtn;
}

function fillSelect(selectObj, collection, id, name) {
    selectObj.children("*").remove();
    for (var i = 0; i < collection.length; i++) {
        var opcion = $("<option></option>").attr("value", collection[i][id]).text(collection[i][name]);
        selectObj.append(opcion);
    }
    return selectObj;
}

function status(tab, val) {
    var tabControl = $(document);
    if (tab) {
        tabControl = $("#" + tab);
    }
    tabControl.find("#buttonbox").find(".control").attr("disabled", !val);
    tabControl.find("#processLog").find(".control").attr("disabled", !val);
    tabControl.find("#okcancel").find(".control").attr("disabled", val);
    tabControl.find("#maintable").find(".control").attr("disabled", val);
    tabControl.find(".collection").find(".control").attr("disabled", val);
    tabControl.find(".enabled").attr("disabled", false);
}

function enable(tab) {
    status(tab, false);
}

function disable(tab) {
    status(tab, true);
}

function required(tab) {
    var tabControl = $(document);
    if (tab) {
        tabControl = $("#" + tab);
    }    
    var rtn = true;
    tabControl.find(".required").each(function () {
        $this = $(this);
        var val;
        if ($(this).attr("nodeName").toLowerCase() == "span") {
            val = $(this).html();
        } else {
            val = $(this).val();
        }
        if (val === "" || val === null || val === "NaN") {
            alert("El campo " + $this.attr("fieldName") + " no puede estar vacio");
            $this.focus();
            rtn = false;
            return false;
        }
    });
    return rtn;
}

function value(tab) {
    if (!required(tab)) {
        return false;
    }
    var tabControl = $(document);
    if (tab) {
        tabControl = $("#" + tab);
    }
    var obj = {};
    tabControl.find("#maintable").find(".control").each(function () {
        var value;
        if ($(this).attr("nodeName").toLowerCase() == "span") {
            value = $(this).html();
        } else {
            value = $(this).val();
        }
        if ($(this).hasClass("numeric")) {
            value = value.replace(/,/g, "");
        }
        obj[this.id] = value;
    });
    tabControl.find(".collection").each(function () {
        var rows = [];
        $(this).children("tbody").children("tr").each(function () {
            var row = {};
            $(this).find(".control").each(function () {

                var value;
                if ($(this).attr("nodeName").toLowerCase() == "span") {
                    value = $(this).html();
                } else {
                    value = $(this).val();
                }
                if ($(this).hasClass("numeric")) {
                    value = value.replace(/,/g, "");
                }
                row[this.id] = value
            });
            rows[rows.length] = row;
        });
        obj[this.id] = rows;
    });
    return obj;
    //return JSON.stringify(obj);
}

function clean(tab) {
    var tabControl = $(document);
    if (tab) {
        tabControl = $("#" + tab);
    }
    tabControl.find("#maintable").find(".control").val("");
    tabControl.find(".collection").children("tbody").children("tr").remove();
}
