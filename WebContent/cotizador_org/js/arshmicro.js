var proxy = true;
var cotizacion = {};
function ajaxrequest(table, fields, filter) {
    var url = "dbserver?table=" + table + "&field=" + fields + filter;

    var urlproxy;
    if (proxy === true) {
        url = encodeURIComponent(url);
        urlproxy = "proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
    } else {
        urlproxy = url;
    }
    var value = false;
    $.ajax({ type: "GET",
        url: urlproxy,
        async: false,
        success: function (result, funcname, resultobj) {
            value = eval(resultobj.responseText);
        },
        error: function (error) {
            alert(error);
            value = false;
        }
    });
    return value;
}
var planes;
var opciones;
var opciones_planes;
var pagos;
var maintable;
$(document).ready(function () {   
	
	/*

	planes = ajaxrequest("planes", 'id:id,name:name,tarifa:tarifa,mensual:mensual,opciones:opciones,opcionesDep:opcionesDep', "");
    opciones = ajaxrequest("opciones", 'id:id,name:name,required:required', "");
    opciones_planes = ajaxrequest("opciones_planes", 'id:id,id_option:id_option,name:name,tarifa:tarifa,tarifa_dep:tarifa_dep', "");
    pagos = ajaxrequest("pagos", 'id:id,name:name,descuento:descuento,recargo:recargo,divisor:divisor', "");
    */    

    planes = [{id:'1',name:'Micro I',tarifa:'3360.00',mensual:'280.00',opciones:'0.00',opcionesDep:'0.00'},{id:'2',name:'Micro II',tarifa:'6540.00',mensual:'545.00',opciones:'0.00',opcionesDep:'0.00'}];
    opciones = [];
    opciones_planes = [{id:'29',id_option:'1',name:'Superior|Amerident',tarifa:'0.00',tarifa_dep:'0.00'},{id:'28',id_option:'1',name:'Superior|Alfadental',tarifa:'0.00',tarifa_dep:'0.00'},{id:'5',id_option:'2',name:'50,000',tarifa:'180.00',tarifa_dep:'180.00'},{id:'6',id_option:'2',name:'100,000',tarifa:'360.00',tarifa_dep:'360.00'},{id:'7',id_option:'2',name:'150,000',tarifa:'540.00',tarifa_dep:'540.00'},{id:'8',id_option:'2',name:'ILIMITADO',tarifa:'840.00',tarifa_dep:'840.00'},{id:'9',id_option:'3',name:'2,000|Coaseguro 20%',tarifa:'1050.00',tarifa_dep:'1050.00'},{id:'10',id_option:'3',name:'3,000|Coaseguro 20%',tarifa:'1460.00',tarifa_dep:'1460.00'},{id:'11',id_option:'3',name:'4,000|Coaseguro 20%',tarifa:'1820.00',tarifa_dep:'1820.00'},{id:'12',id_option:'3',name:'5,000|Coaseguro 20%',tarifa:'2150.00',tarifa_dep:'2150.00'},{id:'13',id_option:'3',name:'7,000|Coaseguro 20%',tarifa:'2880.00',tarifa_dep:'2880.00'},{id:'14',id_option:'3',name:'10,000|Coaseguro 20%',tarifa:'3630.00',tarifa_dep:'3630.00'},{id:'15',id_option:'3',name:'15,000|Coaseguro 20%',tarifa:'5220.00',tarifa_dep:'5220.00'},{id:'16',id_option:'3',name:'20,000|Coaseguro 20%',tarifa:'6430.00',tarifa_dep:'6430.00'},{id:'17',id_option:'3',name:'30,000|Coaseguro 20%',tarifa:'7770.00',tarifa_dep:'7770.00'},{id:'18',id_option:'3',name:'2,000|Coaseguro 30%',tarifa:'920.00',tarifa_dep:'920.00'},{id:'19',id_option:'3',name:'3,000|Coaseguro 30%',tarifa:'1275.00',tarifa_dep:'1275.00'},{id:'20',id_option:'3',name:'4,000|Coaseguro 30%',tarifa:'1580.00',tarifa_dep:'1580.00'},{id:'21',id_option:'3',name:'5,000|Coaseguro 30%',tarifa:'1870.00',tarifa_dep:'1870.00'},{id:'22',id_option:'3',name:'7,000|Coaseguro 30%',tarifa:'2500.00',tarifa_dep:'2500.00'},{id:'23',id_option:'3',name:'10,000|Coaseguro 30%',tarifa:'3150.00',tarifa_dep:'3150.00'},{id:'24',id_option:'3',name:'15,000|Coaseguro 30%',tarifa:'4540.00',tarifa_dep:'4540.00'},{id:'25',id_option:'3',name:'20,000|Coaseguro 30%',tarifa:'5595.00',tarifa_dep:'5595.00'},{id:'26',id_option:'3',name:'30,000|Coaseguro 30%',tarifa:'6770.00',tarifa_dep:'6770.00'},{id:'30',id_option:'1',name:'Superior|Amos',tarifa:'0.00',tarifa_dep:'0.00'},{id:'31',id_option:'1',name:'Superior|Cosmeticdental Center',tarifa:'0.00',tarifa_dep:'0.00'},{id:'32',id_option:'1',name:'Superior|Dentasana',tarifa:'0.00',tarifa_dep:'0.00'},{id:'33',id_option:'1',name:'Superior|Odontodom',tarifa:'0.00',tarifa_dep:'0.00'},{id:'34',id_option:'1',name:'Superior|Pladent',tarifa:'0.00',tarifa_dep:'0.00'},{id:'35',id_option:'1',name:'Superior|Profidental',tarifa:'0.00',tarifa_dep:'0.00'},{id:'36',id_option:'1',name:'Superior|Salud Bucal',tarifa:'0.00',tarifa_dep:'0.00'},{id:'37',id_option:'1',name:'Superior|SDD',tarifa:'0.00',tarifa_dep:'0.00'},{id:'38',id_option:'1',name:'Superior|Servidens',tarifa:'0.00',tarifa_dep:'0.00'},{id:'39',id_option:'1',name:'Superior|Sonrisas Brillantes',tarifa:'0.00',tarifa_dep:'0.00'},{id:'40',id_option:'1',name:'Royal|Alfadental',tarifa:'528.00',tarifa_dep:'528.00'},{id:'41',id_option:'1',name:'Royal|Amerident',tarifa:'528.00',tarifa_dep:'528.00'},{id:'42',id_option:'1',name:'Royal|Amos',tarifa:'528.00',tarifa_dep:'528.00'},{id:'43',id_option:'1',name:'Royal|Cosmeticdental Center',tarifa:'528.00',tarifa_dep:'528.00'},{id:'44',id_option:'1',name:'Royal|Dentasana',tarifa:'528.00',tarifa_dep:'528.00'},{id:'45',id_option:'1',name:'Royal|Odontodom',tarifa:'528.00',tarifa_dep:'528.00'},{id:'46',id_option:'1',name:'Royal|Pladent',tarifa:'528.00',tarifa_dep:'528.00'},{id:'47',id_option:'1',name:'Royal|Profidental',tarifa:'528.00',tarifa_dep:'528.00'},{id:'48',id_option:'1',name:'Royal|Salud Bucal',tarifa:'528.00',tarifa_dep:'528.00'},{id:'49',id_option:'1',name:'Royal|SDD',tarifa:'528.00',tarifa_dep:'528.00'},{id:'50',id_option:'1',name:'Royal|Servidens',tarifa:'528.00',tarifa_dep:'528.00'},{id:'51',id_option:'1',name:'Royal|Sonrisas Brillantes',tarifa:'528.00',tarifa_dep:'528.00'},{id:'52',id_option:'1',name:'Max|Alfadental',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'53',id_option:'1',name:'Max|Amerident',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'54',id_option:'1',name:'Max|Amos',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'55',id_option:'1',name:'Max|Cosmeticdental Center',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'56',id_option:'1',name:'Max|Dentasana',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'57',id_option:'1',name:'Max|Odontodom',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'58',id_option:'1',name:'Max|Pladent',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'59',id_option:'1',name:'Max|Profidental',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'60',id_option:'1',name:'Max|Salud Bucal',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'61',id_option:'1',name:'Max|SDD',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'62',id_option:'1',name:'Max|Servidens',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'63',id_option:'1',name:'Max|Sonrisas Brillantes',tarifa:'1188.00',tarifa_dep:'1188.00'},{id:'81',id_option:'5',name:'USD$10,000',tarifa:'1800.00',tarifa_dep:'1800.00'},{id:'80',id_option:'1',name:'Platinum|Red Dental Platinum',tarifa:'6000.00',tarifa_dep:'6000.00'},{id:'76',id_option:'4',name:'Alert Plus',tarifa:'1320.00',tarifa_dep:'1320.00'},{id:'77',id_option:'4',name:'Movi Alert',tarifa:'480.00',tarifa_dep:'480.00'},{id:'82',id_option:'6',name:'RD$100,000',tarifa:'0.00',tarifa_dep:'0.00'},{id:'83',id_option:'6',name:'RD$300,000',tarifa:'720.00',tarifa_dep:'0.00'},{id:'84',id_option:'6',name:'RD$500,000',tarifa:'1440.00',tarifa_dep:'0.00'}];
    pagos = [{id:'1',name:'Anual',descuento:'0.0000',recargo:'0.0000',divisor:'1'},{id:'2',name:'Semestral',descuento:'0.0000',recargo:'0.0000',divisor:'2'},{id:'3',name:'Trimestral',descuento:'0.0000',recargo:'0.0000',divisor:'4'},{id:'4',name:'Mensual',descuento:'0.0000',recargo:'0.0000',divisor:'12'}];
    
    $("#titular").change(calcular);
    $("#conyugue").change(calcular);
    $("#H").keyup(calcular);
    
    $("#showAll").click(function(){
    	for (var i = 0; i < planes.length; i++) {
    		planes[i].visible = true;
    		$("." + planes[i].name.replace(/ /g, "")).show();
    	}
    });

    maintable = $("#maintable");
    var thead = maintable.children("thead").children("#planes");
    for (var i = 0; i < planes.length; i++) {
        var plan = planes[i];
        planName = plan.name.replace(/ /g, "");
        planes[i].visible = true;
        var button = $("<button id='" + planName + "'></button>").text(plan.name).click(function(){
        	for (var i = 0; i < planes.length; i++) {
        		if (planes[i].name.replace(/ /g,"") == this.id){
        			planes[i].visible = false;
        			break;
        		}
        	}
        	$("." + this.id).hide();
        });
        
        thead.append($("<td class='titulo2 " + planName + "' style='width:150px'></td>").append(button));
    }
    
   
    var tbody = $("tbody.opciones");
    for (var x = 0; x < opciones.length; x++) {
        var row = $("<tr></tr>");
        var cell = $("<td></td>");
        if (opciones[x].required == '1'){
        	cell.append($("<input type='checkbox' id='" + opciones[x].name + "' checked disabled />").change(calcular));
        } else {
        	cell.append($("<input type='checkbox' id='" + opciones[x].name + "' />").change(calcular));
        }
        cell.append($("<label for='" + opciones[x].name + "'></label>").html(opciones[x].name));
        /*
        for (var i = 0; i < planes.length; i++) {
            var plan = planes[i];
        }
        */
        row.append(cell);

        cell = $("<td></td>");

        var planesObj = $("<select class='option' style='width:200px'></select>").change(function(){
            var planesObj = $(this);
        	var x = $(this).attr("planID");
            $(this).parent().find(".subopcion").children().remove();
            for (var i = 0; i < opciones_planes.length; i++) {
                if (opciones_planes[i].id_option === opciones[x].id) {
                    if (opciones_planes[i].name.indexOf("|") === -1) {
                       // planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
                    } else {
                    	if (opciones_planes[i].name.split("|")[0] === $($(this).attr("options")[$(this).attr("selectedIndex")]).text()){
	                        var name = opciones_planes[i].name.split("|");
	                        if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
	                            planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
	                        }
	
	                        var subopcion = $(this).parent().find(".subopcion");
	                        if (subopcion.length === 0) {
	                            subopcion = $("<select class='subopcion'></select>").change(calcular);
	                            $(this).parent().append(subopcion);
	                        }
	                        if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
	                            subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
	                        }
                    	}
                    }
                }
            }
        	calcular();
        	
        });
        planesObj.attr("planID",x);
        cell.append(planesObj);
        for (var i = 0; i < opciones_planes.length; i++) {
            if (opciones_planes[i].id_option === opciones[x].id) {
                if (opciones_planes[i].name.indexOf("|") === -1) {
                    planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
                } else {
                    var name = opciones_planes[i].name.split("|");
                    if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
                        planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
                    }

                    var subopcion = cell.find(".subopcion");
                    if (subopcion.length === 0) {
                        subopcion = $("<select class='subopcion'></select>").change(calcular);
                        cell.append(subopcion);
                    }
                    if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
                        subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
                    }
                }
            }
        }

        row.append(cell);
        tbody.append(row);
    }

    tbody = $(".calculable").children("tr");
    for (var i = 0; i < planes.length; i++) {
        var plan = planes[i];
        planName = plan.name.replace(/ /g, "");
        tbody.append($("<td class='tarifa "+ planName +"' align='right'></td>").append($("<span type='text' class='calcular' id='" + plan.name + i + "'></span>").html(0)));
    }

    for (var i = 0; i < pagos.length; i++) {
        $("#opcionPagos").change(calcular).append("<option value='" + pagos[i].id + "'>" + pagos[i].name + "</option>");
    }
       
    $("tr [id='Emergencias']").find("[id='Micro I0']").text("100%");
    $("tr [id='Emergencias']").find("[id='Micro II1']").text("100%");
    
    $("tr [id='Consultas']").find("[id='Micro I0']").text("N/D");
    $("tr [id='Consultas']").find("[id='Micro II1']").text("Ilimitadas");
    
    $("tr [id='Rayos X/Laboratorios1']").find("[id='Micro I0']").text("N/D");
    $("tr [id='Rayos X/Laboratorios1']").find("[id='Micro II1']").text("60% (ilimitado)");

    $("tr [id='Estudios especiales1']").find("[id='Micro I0']").text("N/D");
    $("tr [id='Estudios especiales1']").find("[id='Micro II1']").text("60% (ilimitado)");
    
    $("tr [id='Habitacion']").find("[id='Micro I0']").html("100%");
    $("tr [id='Habitacion']").find("[id='Micro II1']").html("100%");
    
    $("tr [id='Maximo de dias por caso']").find("[id='Micro I0']").html("RD$100,000");
    $("tr [id='Maximo de dias por caso']").find("[id='Micro II1']").html("RD$100,000");
    
    $("tr [id='Medicinas durante el internamiento']").find("[id='Micro I0']").html("100%");
    $("tr [id='Medicinas durante el internamiento']").find("[id='Micro II1']").html("100%");
    

    $("tr [id='Sala de cirugia']").find("[id='Micro I0']").text("70%");
    $("tr [id='Sala de cirugia']").find("[id='Micro II1']").text("70%");
    
    $("tr [id='Rayos X/Laboratorios']").find("[id='Micro I0']").text("70%");
    $("tr [id='Rayos X/Laboratorios']").find("[id='Micro II1']").text("70%");

    $("tr [id='Material gastable']").find("[id='Micro I0']").text("100%");
    $("tr [id='Material gastable']").find("[id='Micro II1']").text("100%");
    
    $("tr [id='Estudios especiales']").find("[id='Micro I0']").text("70%");
    $("tr [id='Estudios especiales']").find("[id='Micro II1']").text("70%");
    
    $("tr [id='Honorarios por cirugia']").find("[id='Micro I0']").text("100%");
    $("tr [id='Honorarios por cirugia']").find("[id='Micro II1']").text("100%");
    
    $("tr [id='Honorarios por anestesia']").find("[id='Micro I0']").text("100%");
    $("tr [id='Honorarios por anestesia']").find("[id='Micro II1']").text("100%");
    
    $("tr [id='Honorarios por visitas durante internamiento no quirurgico']").find("[id='Micro I0']").text("100%");
    $("tr [id='Honorarios por visitas durante internamiento no quirurgico']").find("[id='Micro II1']").text("100%");
    
    $("tr [id='Plan de enfermedades catastroficas']").find("[id='Micro I0']").html("RD$350,000");
    $("tr [id='Plan de enfermedades catastroficas']").find("[id='Micro II1']").html("$ 500,000");
    
    $("tr [id='Maternidad']").find("[id='Micro I0']").html("N/D");
    $("tr [id='Maternidad']").find("[id='Micro II1']").html("70%");
    
    $("tr [id='Odontologia']").find("[id='Micro I0']").html("Superior");
    $("tr [id='Odontologia']").find("[id='Micro II1']").html("Superior");
    
    $(".tituloSecion").each(function(){
    	var $this = $(this);
    	$this.find("[id='Micro I0']").html("");
    	$this.find("[id='Micro II1']").html("");
    });
    
    $(".totalOpciones").find(".calcular").html("");
    
    calcular();
});

function round5(n){
	return Math.ceil(n/5)*5;
}

function calcular(e) {
    cotizacion = { planes: [], opciones: [], cliente: {}, totalAfiliados: 0, totalPlanes: {}, subTotal: {}, descuento: {}, total: {}, cuota: {}, formaPago:"" , bcc:"",subject:"", planDef:[]};
    if ($("#titular").val() === "TM" && $("#conyugue").val() !== "O") {
        alert("No aplica conyugue con maternidad ya que el plan incluye maternidad");
        $(e.target).attr("selectedIndex", 0);
    }
    if ($("#titular").val() === "T" && $("#conyugue").val() === "TM") {
        alert("No Aplica titular y conyugue con maternidad");
        $(e.target).attr("selectedIndex", 0);
    }
    var factores = { O: 0, T: 1, TM: 1, T65: 1, T75: 1, H: 1 };
    for (var i = 0; i < planes.length; i++) {
        planes[i].total = 0;
    }
    var totalafiliados = 0;
    totales = [];
    
    var tbody = maintable.children(".control").children("tr");    
    tbody.each(function () {
        var objectPlanes = {};
        var row = $(this);
        var control = row.find(".control");
        tipotarifa = "H";
        cantidad = 1;
        if (control.attr("id") !== "H"){
            tipotarifa = control.val();
        } else {
            cantidad = control.val();
            if (cantidad === "") {
                cantidad = 0;
            }
            cantidad = parseFloat(cantidad);
        }
        if (tipotarifa === "O") {
            cantidad = 0;
        }
        objectPlanes.nombre = tipotarifa;
        row.find(".calcular").each(function (i) {

            var plan = planes[i];
            var cargo = plan.tarifa;

            if ($("#opcionPagos").attr("selectedIndex") === 3) {
                cargo = (plan.tarifa * 1.00);
            }
            var tarifa = round5(cargo * factores[tipotarifa]) * cantidad;
            if (totales.length < (i + 1)) {
                totales[i] = 0;
            }

            tarifa = Math.ceil(tarifa);
            if (parseFloat(tarifa) !== 0){
	            if (control.attr("id")== 'titular' && control.val() != "T65" && control.val() != "T75"){
	            	tarifa += parseFloat(plan.opciones);
	            } else {
	            	tarifa += parseFloat(plan.opcionesDep);
	            }
            }
            totales[i] += tarifa;
            plan.total += tarifa;
            $(this).html(tarifa);
            objectPlanes[plan.name] = tarifa;
        });
        cotizacion.planes[cotizacion.planes.length] = objectPlanes;    
        totalafiliados += cantidad;

    });


    tbody = $("tbody.totales").children("tr");
    tbody.each(function () {
        var row = $(this);       
        row.find(".calcular").each(function (i) {
            $(this).html(planes[i].total);
            cotizacion.totalPlanes[planes[i].name] = planes[i].total;
        });
        
    });

    var id_option = 0;
    tbody = $("tbody.opciones").children("tr.calc2");
    tbody.each(function (x) {
        var row = $(this);
        tarifa = 0;
        var objectTarifa = {};
        objectTarifa.nombre = opciones[x].name;
        var calcularOpcion = row.find("input[type='checkbox']").attr("checked");
        var name = "";
        if (calcularOpcion === true) {
            id_option = opciones[x].id;
            name = row.find(".option").val();
            subopcion = row.find(".subopcion");
            if (subopcion.length > 0) {
                name += "|" + subopcion.val();
            }
            objectTarifa.plan = name;
            for (var i = 0; i < opciones_planes.length; i++) {
                if (opciones_planes[i].id_option === id_option && opciones_planes[i].name === name) {
                    tarifa = parseFloat(opciones_planes[i].tarifa);
                    tarifa += parseFloat(opciones_planes[i].tarifa_dep * (totalafiliados - 1));
                }
            }
        }
        var temptarifa = tarifa;
        row.find(".calcular").each(function (i) {
        var notAvail = 0;
        tarifa = Math.ceil(temptarifa);
        if ((id_option == 5 || id_option == 4) && $(this).attr("id").indexOf("Platinum") !== -1){
        	tarifa = 0;
        }

        if ((id_option == 1 ) && $(this).attr("id").indexOf("Platinum") !== -1){
        	//notAvail = 1;
        } else {
        	//notAvail = 0;
        }


	     if (name && id_option == 1 && name.indexOf('Platinum') !== -1 && $(this).attr("id").indexOf("Platinum") == -1){
	    	 notAvail = 1;
	    	 tarifa = 0;
         }

	     if (name && id_option == 1 && name.indexOf('Platinum') == -1 && $(this).attr("id").indexOf("Platinum") !== -1){
	    	 notAvail = 1;
	    	 tarifa = 0;
         }
	     
	     if (($("#titular").val() == "T75" || $("#titular").val() == "T65") && id_option == 6){
	    	 notAvail = 1;
	    	 tarifa = 0;
	    	 objectTarifa.plan = "N/D";
	     }

            if (tarifa === 0 && calcularOpcion === true) {
                $(this).html("incluido");
            } else {
           		$(this).html(tarifa);
            }
	     if (notAvail == 1){
           		$(this).html("N/D");
	     }
            totales[i] += tarifa;
            objectTarifa[planes[i].name] = tarifa;
        });
        cotizacion.opciones[cotizacion.opciones.length] = objectTarifa;
    });

    $("#totalAfiliados").html(totalafiliados);
    cotizacion.totalAfiliados = totalafiliados;

    tbody = $("tbody.totalOpciones2").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            $(this).html(totales[i] - planes[i].total);
        });
    });
    tbody = $("tbody.totalGeneral").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            $(this).html(totales[i]);
            cotizacion.subTotal[planes[i].name] = totales[i];
        });
    });

    var descuento = [];
    tbody = $("tbody.Descuento").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            var opcionPag = $("#opcionPagos").attr("selectedIndex");
            //var descuentoTarifa = parseFloat(planes[i].total) * ((parseFloat(pagos[opcionPag].descuento) / 100));
            var descuentoTarifa = parseFloat(totales[i]) * ((parseFloat(pagos[opcionPag].descuento) / 100));
            descuento[i] = Math.round(descuentoTarifa, 0);

            $("#descuento").html(Math.round(parseFloat(pagos[opcionPag].descuento), 0));
            $(this).html(descuento[i]);
            cotizacion.descuento[planes[i].name] = descuento[i];
            var opcionPagoletra = "";
            switch (opcionPag) {
                case 0:
                    opcionPagoletra = "Anual";
                    break;
                case 1:
                    opcionPagoletra = "Semestral";
                    break;
                case 2:
                    opcionPagoletra = "Trimestral";
                    break;
                case 3:
                    opcionPagoletra = "Mensual";
                    break;
            }
            cotizacion.formaPago = opcionPagoletra;
        });
    });

    tbody = $("tbody.total").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            $(this).html(totales[i] - descuento[i]);
            cotizacion.total[planes[i].name] = totales[i] - descuento[i] ;
        });
        
    });

    tbody = $("tbody.Cuota").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            //var total = Math.round(planes[i].total * ((parseFloat(pagos[$("#opcionPagos").attr("selectedIndex")].descuento) / 100)));
            var cuota = Math.round((totales[i] - descuento[i]) / parseFloat(pagos[$("#opcionPagos").attr("selectedIndex")].divisor),0);
            //var cuota = Math.round((planes[i].total - total) / parseFloat(pagos[$("#opcionPagos").attr("selectedIndex")].divisor),0);
            $(this).html(cuota);
            cotizacion.cuota[planes[i].name] = cuota;
        });
    });
    
    cotizacion.planDef = planes;
    
    tbody = $("#tabla2").find(".cliente");
    tbody.each(function(){
        //var row = $(this);
        cotizacion.cliente[this.id] = $(this).val();
    });

    $(".calcular").prettynumber({
        delimiter: ','
    });

    //$("#json").val(JSON.stringify(cotizacion));
}





function sendMail(){
	calcular();
	//var url = "emaillocal";
	//var urlproxy;
	//var data;
	/*
	if (proxy == true){
		//url = encodeURIComponent(url + "?data=" + JSON.stringify(cotizacion));
		url = url + "?data=" + JSON.stringify(cotizacion);
		urlproxy = "proxy.php";
		data = {url:"http://172.24.206.227:8080/" + url, mode:'native', REQUEST_METHOD:"POST"};
		//mode=native&url=http://172.24.206.227:8080/"+url
	} else {
		urlproxy = url;
		data = {data:encodeURIComponent(JSON.stringify(cotizacion))};
	}

	urlproxy = "http://cotizador.arshumano.com.do:8080/emaillocal";
	data = {data:JSON.stringify(cotizacion)};
	$.ajax({type: "POST",
		url:urlproxy,
		async:false,
		data:data,
		success:function(result,funcname, resultobj){
			alert(resultobj.responseText);
		},
		error:function(error){
			alert(error.responseText);
		}
	});		
	*/	
	$("#printForm").attr("action","http://cotizador.arshumano.com:8080/emailmicro");
	$("#printData").val(JSON.stringify(cotizacion));
	$("#printForm").attr("target","emailFrame");
	$("#printForm").submit();
	//setTimeout('alert($("#emailFrame").contents())',500)
	///Modificacion hecha por CManzu..
	setTimeout('alert("Mensaje enviado correctamente.")',500);
} 
function print(){	
	calcular();	
	/*var url = "";
	url += "/birt/frameset";
	url += "?__report=arshlocal.rptdesign&data="+JSON.stringify(cotizacion); 
	url += "&__format=pdf";
	window.open("http://cotizador.arshumano.com:8080" + url);
	*/
	$("#printForm").attr("action","http://cotizador.arshumano.com:8080/birt/frameset");
	$("#printForm").attr("target","_new");
	$("#printData").val(JSON.stringify(cotizacion));
	$("#printForm").submit();	
}

