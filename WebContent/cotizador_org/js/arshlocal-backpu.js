var proxy = true;
var cotizacion = {};
function ajaxrequest(table, fields, filter) {
    var url = "dbserver?table=" + table + "&field=" + fields + filter;

    var urlproxy;
    if (proxy === true) {
        url = encodeURIComponent(url);
        urlproxy = "proxy.php?mode=native&url=http://172.24.206.227:8080/" + url;
    } else {
        urlproxy = url;
    }
    var value;
    $.ajax({ type: "GET",
        url: urlproxy,
        async: false,
        success: function (result, funcname, resultobj) {
            value = eval(resultobj.responseText);
        },
        error: function (error) {
            alert(error);
            value = false;
        }
    });
    return value;
}
var planes;
var opciones;
var opciones_planes;
var pagos;
var maintable;
$(document).ready(function () {
    //$("#logo").hide();


	planes = ajaxrequest("planes", 'id:id,name:name,tarifa:tarifa,mensual:mensual', "");
    opciones = ajaxrequest("opciones", 'id:id,name:name', "");
    opciones_planes = ajaxrequest("opciones_planes", 'id:id,id_option:id_option,name:name,tarifa:tarifa', "");
    pagos = ajaxrequest("pagos", 'id:id,name:name,descuento:descuento,recargo:recargo,divisor:divisor', "");
    /*
    planes = [{ id: '1', name: 'Superior', tarifa: '9800.00', mensual: '860' }, { id: '2', name: 'Royal', tarifa: '11332.00', mensual: '994' }, { id: '3', name: 'Max', tarifa: '16732.00', mensual: '1468' }, { id: '4', name: 'Platinum', tarifa: '38800.00', tarifa: '38800', mensual: '3405'}];
    opciones = [{ id: '1', name: 'Odontologia' }, { id: '2', name: 'Ultimos Gastos' }, { id: '3', name: 'Medicina Ambulatoria'}];
    opciones_planes = [{ id: '1', id_option: '1', name: 'Superior|Dentasa', tarifa: '0.00' }, { id: '2', id_option: '1', name: 'Royal|Dentasa', tarifa: '528.00' }, { id: '3', id_option: '1', name: 'Max|Dentasa', tarifa: '1188.00' }, { id: '4', id_option: '1', name: 'Platinum|Dentasa', tarifa: '4188.00' }, { id: '5', id_option: '2', name: '50,000', tarifa: '180.00' }, { id: '6', id_option: '2', name: '100,000', tarifa: '360.00' }, { id: '7', id_option: '2', name: '150,000', tarifa: '540.00' }, { id: '8', id_option: '2', name: 'ILIMITADO', tarifa: '840.00' }, { id: '9', id_option: '3', name: '2000|20', tarifa: '1050.00' }, { id: '10', id_option: '3', name: '3000|20', tarifa: '1460.00' }, { id: '11', id_option: '3', name: '4000|20', tarifa: '1820.00' }, { id: '12', id_option: '3', name: '5000|20', tarifa: '2150.00' }, { id: '13', id_option: '3', name: '7000|20', tarifa: '2880.00' }, { id: '14', id_option: '3', name: '10000|20', tarifa: '3630.00' }, { id: '15', id_option: '3', name: '15000|20', tarifa: '5220.00' }, { id: '16', id_option: '3', name: '20000|20', tarifa: '6430.00' }, { id: '17', id_option: '3', name: '30000|20', tarifa: '7770.00' }, { id: '18', id_option: '3', name: '2000|30', tarifa: '920.00' }, { id: '19', id_option: '3', name: '3000|30', tarifa: '1275.00' }, { id: '20', id_option: '3', name: '4000|30', tarifa: '1580.00' }, { id: '21', id_option: '3', name: '5000|30', tarifa: '1870.00' }, { id: '22', id_option: '3', name: '7000|30', tarifa: '2500.00' }, { id: '23', id_option: '3', name: '10000|30', tarifa: '3150.00' }, { id: '24', id_option: '3', name: '15000|30', tarifa: '4540.00' }, { id: '25', id_option: '3', name: '20000|30', tarifa: '5595.00' }, { id: '26', id_option: '3', name: '30000|30', tarifa: '6770.00'}];
    pagos = [{ id: '1', name: 'Anual', descuento: '10.000', recargo: '0.0000', divisor: '1' }, { id: '2', name: 'Semestral', descuento: '5.000', recargo: '0.0000', divisor: '2' }, { id: '3', name: 'Trimestral', descuento: '0.0000', recargo: '0.0000', divisor: '4' }, { id: '4', name: 'Mensual', descuento: '0.0000', recargo: '5.3061', divisor: '12'}];
     */      
    $("#titular").change(calcular);
    $("#conyugue").change(calcular);
    $("#H").keyup(calcular);
    
    $("#showAll").click(function(){
    	for (var i = 0; i < planes.length; i++) {
    		planes[i].visible = true;
    		$("." + planes[i].name).show();
    	}
    });

    maintable = $("#maintable");
    var thead = maintable.children("thead").children("#planes");
    for (var i = 0; i < planes.length; i++) {
        var plan = planes[i];
        planes[i].visible = true;
        var button = $("<button id='" + plan.name + "'></button>").text(plan.name).click(function(){
        	for (var i = 0; i < planes.length; i++) {
        		if (planes[i].name == this.id){
        			planes[i].visible = false;
        			break;
        		}
        	}
        	$("." + this.id).hide();
        });
        
        thead.append($("<td class='titulo2 " + plan.name + "'></td>").append(button));
    }
    
   
    var tbody = $("tbody.opciones");
    for (var x = 0; x < opciones.length; x++) {
        var row = $("<tr></tr>");
        var cell = $("<td></td>");
        cell.append($("<input type='checkbox' id='" + opciones[x].name + "' />").change(calcular));
        cell.append($("<label for='" + opciones[x].name + "'></label>").html(opciones[x].name));

        for (var i = 0; i < planes.length; i++) {
            var plan = planes[i];
        }
        row.append(cell);

        cell = $("<td></td>");

        var planesObj = $("<select class='option' style='width:200px'></select>").change(calcular);
        cell.append(planesObj);
        for (var i = 0; i < opciones_planes.length; i++) {
            if (opciones_planes[i].id_option === opciones[x].id) {
                if (opciones_planes[i].name.indexOf("|") === -1) {
                    planesObj.append("<option value='" + opciones_planes[i].name + "'>" + opciones_planes[i].name + "</option>");
                } else {
                    var name = opciones_planes[i].name.split("|");
                    if (planesObj.find("option[value='" + name[0] + "']").length === 0) {
                        planesObj.append("<option value='" + name[0] + "'>" + name[0] + "</option>");
                    }

                    var subopcion = cell.find(".subopcion");
                    if (subopcion.length === 0) {
                        subopcion = $("<select class='subopcion' style='width:135px'></select>").change(calcular);
                        cell.append(subopcion);
                    }
                    if (subopcion.find("option[value='" + name[1] + "']").length === 0) {
                        subopcion.append("<option value='" + name[1] + "'>" + name[1] + "</option>");
                    }
                }
            }
        }

        row.append(cell);
        tbody.append(row);
    }

    tbody = $(".calculable").children("tr");
    for (var i = 0; i < planes.length; i++) {
        var plan = planes[i];
        tbody.append($("<td class='tarifa "+ plan.name +"' align='right'></td>").append($("<span type='text' class='calcular' id='" + plan.name + i + "'></span>").html(0)));
    }

    for (var i = 0; i < pagos.length; i++) {
        $("#opcionPagos").change(calcular).append("<option value='" + pagos[i].id + "'>" + pagos[i].name + "</option>");
    }
    calcular();
});


function calcular(e) {
    cotizacion = { planes: [], opciones: [], cliente: {}, totalAfiliados: 0, totalPlanes: {}, subTotal: {}, descuento: {}, total: {}, cuota: {}, formaPago:"" , bcc:"",subject:"", planDef:[]};
    if ($("#titular").val() === "TM" && $("#conyugue").val() !== "O") {
        alert("No aplica conyugue con maternidad ya que el plan incluye maternidad");
        $(e.target).attr("selectedIndex", 0);
    }
    if ($("#titular").val() === "T" && $("#conyugue").val() === "TM") {
        alert("No Aplica titular y conyugue con maternidad");
        $(e.target).attr("selectedIndex", 0);
    }
    var factores = { O: 0, T: 1, TM: 1.4, T65: 2, T75: 3, H: 0.8 };
    for (var i = 0; i < planes.length; i++) {
        planes[i].total = 0;
    }
    var totalafiliados = 0;
    totales = [];
    
    var tbody = maintable.children(".control").children("tr");    
    tbody.each(function () {
        var objectPlanes = {};
        var row = $(this);
        var control = row.find(".control");
        tipotarifa = "H";
        cantidad = 1;
        if (control.attr("id") !== "H") {
            tipotarifa = control.val();
        } else {
            cantidad = control.val();
            if (cantidad === "") {
                cantidad = 0;
            }
            cantidad = parseFloat(cantidad);
        }
        if (tipotarifa === "O") {
            cantidad = 0;
        }
        objectPlanes.nombre = tipotarifa;
        row.find(".calcular").each(function (i) {

            var plan = planes[i];
            var cargo = plan.tarifa;

            if ($("#opcionPagos").attr("selectedIndex") === 3) {
                cargo = plan.mensual * 12;
            }
            var tarifa = Math.ceil(cargo * factores[tipotarifa]) * cantidad;
            if (totales.length < (i + 1)) {
                totales[i] = 0;
            }

            tarifa = Math.ceil(tarifa);
            totales[i] += tarifa;
            plan.total += tarifa;
            $(this).html(tarifa);
            objectPlanes[plan.name] = tarifa;
        });
        cotizacion.planes[cotizacion.planes.length] = objectPlanes;    
        totalafiliados += cantidad;

    });


    tbody = $("tbody.totales").children("tr");
    tbody.each(function () {
        var row = $(this);       
        row.find(".calcular").each(function (i) {
            $(this).html(planes[i].total);
            cotizacion.totalPlanes[planes[i].name] = planes[i].total;
        });
        
    });


    tbody = $("tbody.opciones").children("tr");
    tbody.each(function (x) {
        var row = $(this);
        tarifa = 0;
        var objectTarifa = {};
        objectTarifa.nombre = opciones[x].name;
        var calcularOpcion = row.find("input[type='checkbox']").attr("checked");
        if (calcularOpcion === true) {
            var id_option = opciones[x].id;
            var name = row.find(".option").val();
            subopcion = row.find(".subopcion");
            if (subopcion.length > 0) {
                name += "|" + subopcion.val();
            }
            objectTarifa.plan = name;
            for (var i = 0; i < opciones_planes.length; i++) {
                if (opciones_planes[i].id_option === id_option && opciones_planes[i].name === name) {
                    tarifa = opciones_planes[i].tarifa * totalafiliados;
                }
            }
        }

        row.find(".calcular").each(function (i) {
            tarifa = Math.ceil(tarifa);
            totales[i] += tarifa;
            if (tarifa === 0 && calcularOpcion === true) {
                $(this).html("incluido");
            } else {
                $(this).html(tarifa);
            }
            objectTarifa[planes[i].name] = tarifa;
        });
        cotizacion.opciones[cotizacion.opciones.length] = objectTarifa;
    });

    $("#totalAfiliados").html(totalafiliados);
    cotizacion.totalAfiliados = totalafiliados;

    tbody = $("tbody.totalOpciones").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            $(this).html(totales[i] - planes[i].total);
        });
    });
    tbody = $("tbody.totalGeneral").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            $(this).html(totales[i]);
            cotizacion.subTotal[planes[i].name] = totales[i];
        });
    });

    var descuento = [];
    tbody = $("tbody.Descuento").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            var opcionPag = $("#opcionPagos").attr("selectedIndex");
            var descuentoTarifa = parseFloat(totales[i]) * ((parseFloat(pagos[opcionPag].descuento) / 100))
            descuento[i] = Math.round(descuentoTarifa, 0);

            $("#descuento").html(Math.round(parseFloat(pagos[opcionPag].descuento), 0))
            $(this).html(descuento[i]);
            cotizacion.descuento[planes[i].name] = descuento[i];
            var opcionPagoletra = "";
            switch (opcionPag) {
                case 0:
                    opcionPagoletra = "Anual";
                    break;
                case 1:
                    opcionPagoletra = "Semestral";
                    break;
                case 2:
                    opcionPagoletra = "Trimestral";
                    break;
                case 3:
                    opcionPagoletra = "Mensual";
                    break;
            }
            cotizacion.formaPago = opcionPagoletra;
        });
    });

    tbody = $("tbody.total").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            $(this).html(totales[i] - descuento[i]);
            cotizacion.total[planes[i].name] = totales[i] - descuento[i] ;
        });
        
    });

    tbody = $("tbody.Cuota").children("tr");
    tbody.each(function () {
        var row = $(this);
        row.find(".calcular").each(function (i) {
            var total = totales[i] * ((parseFloat(pagos[$("#opcionPagos").attr("selectedIndex")].descuento) / 100));
            var cuota = Math.round((totales[i] - total) / parseFloat(pagos[$("#opcionPagos").attr("selectedIndex")].divisor),0);
            $(this).html(cuota);
            cotizacion.cuota[planes[i].name] = cuota;
        });
    });
    
    cotizacion.planDef = planes;
    
    tbody = $("#tabla2").find(".cliente");
    tbody.each(function(){
        var row = $(this);
        cotizacion.cliente[this.id] = $(this).val();
    });

    $(".calcular").prettynumber({
        delimiter: ','
    });

    //$("#json").val(JSON.stringify(cotizacion));
}

function sendMail(){
	calcular();
	var url = "emaillocal";
	var urlproxy;
	var data;
	if (proxy == true){
		//url = encodeURIComponent(url + "?data=" + JSON.stringify(cotizacion));
		url = url + "?data=" + JSON.stringify(cotizacion);
		urlproxy = "proxy.php";
		data = {url:"http://172.24.206.227:8080/" + url, mode:'native', REQUEST_METHOD:"POST"};
		//mode=native&url=http://172.24.206.227:8080/"+url
	} else {
		urlproxy = url;
		data = {data:encodeURIComponent(JSON.stringify(cotizacion))};
	}
	$.ajax({type: "POST",
		url:urlproxy,
		async:false,
		data:data,
		success:function(result,funcname, resultobj){
			alert(resultobj.responseText);
		},
		error:function(error){
			alert(error.responseText);
		}
	});		
	
	
} 
function print(){
	calcular();
	var url = "";
	url += "/birt/frameset";
	url += "?__report=arshlocal.rptdesign&data="+JSON.stringify(cotizacion); 
	url += "&__format=pdf";
	window.open("http://cotizador.arshumano.com:8080" + url);
	
}